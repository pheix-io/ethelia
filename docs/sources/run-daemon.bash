#!/bin/bash

BOT=/home/kostas/git/pheix-io/ethelia/sources/tg-bot/trivial-bot.raku
LOG=/var/log/tg-bot
APP=/home/kostas/tg-bot/app

if [ ! -d "${LOG}" ]; then
    echo "no ${LOG} found";

    exit 1;
fi;

if [ ! -d "${APP}" ]; then
    echo "no ${APP} found";

    exit 2;
fi;

if [ ! -f "${BOT}" ]; then
    echo "no ${BOT} installed";

    exit 2;
fi;

# run from privileged user
daemonize -a -e ${LOG}/error.log -o ${LOG}/bot.log -u kostas -p /tmp/tg-bot.pid -c ${APP} -v ${BOT}

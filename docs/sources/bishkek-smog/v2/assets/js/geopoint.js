window.PM25globals = { "sidebar" : false };

let mode = Cookies.get('_pm25_online_mode');

if (mode === 'day' || mode === 'night') {
    jQuery('#body-container').removeClass('daymode');
    jQuery('#body-container').removeClass('nightmode');
    jQuery('#body-container').addClass(mode + 'mode');

    if (mode === 'day') {
        jQuery("#toggle-trigger").prop('checked', true);
    }
    else {
        jQuery("#toggle-trigger").prop('checked', false);
    }
}

ol.proj.useGeographic();

const mapstyle = {
    datavizdark: 'dataviz-dark',
    dataviz: 'dataviz',
    streetsv2: 'streets-v2',
    openstreetmap: 'openstreetmap',
    winterv2: 'winter-v2',
    backdrop: 'backdrop',
    basicv2: 'basic-v2',
    brightv2: 'bright-v2',
    brightv2dark: 'bright-v2-dark',
    ocean: 'ocean',
    outdoorv2: 'outdoor-v2',
    satellite: 'satellite',
    tonerv2: 'toner-v2',
    topov2: 'topo-v2'
};

const key       = 'EX1rrmQhlhgA5T6p5I3z';
//const styleJson   = `https://api.maptiler.com/maps/dataviz/style.json?key=${key}`;
const refresh   = 30000;
const maxage    = 9000;
const endpoint  = "http://pheix.webtech-omen/api";
const bishkek   = [ 74.607386, 42.832880 ];
const bishkek_m = [ 74.601875, 42.823562 ];
const zoom      = 13;
const zoom_m    = 12.43;
const point     = new ol.geom.Point(bishkek);
//const filter    = new ol.filter.Colorize();
const reiqair   = /^iqair-[\d]+/i;
const reaqicn   = /^aqicn-[\d]+/i;

var source          = undefined;
var layers          = [];
var layer           = undefined;
var advanceMarkers  = [];
var markerInstances = [];
var timer_id        = undefined;
var map             = undefined;

function initEnv() {
    let mode = Cookies.get('_pm25_online_mode');

    if (mode === 'day' || mode === 'night') {
        jQuery('#body-container').removeClass('daymode');
        jQuery('#body-container').removeClass('nightmode');
        jQuery('#body-container').addClass(mode + 'mode');
    }

    let mapmode = (mode === 'night' ? mapstyle.brightv2dark : mapstyle.brightv2);

    source = new ol.source.TileJSON({
      url: `https://api.maptiler.com/maps/${mapmode}/tiles.json?key=${key}`,
      tileSize: 512,
      crossOrigin: 'anonymous'
    });

    layers = [ new ol.layer.Tile({ source: source }) ];
    layer  = layers[0];
    advanceMarkers  = [];
    markerInstances = [];
    timer_id = undefined;
    map      = undefined;

    //layer.addFilter(filter);

    return true;
}

function adjustCSS(value, valid, nonlocal) {
    var default_classes;
    var air_quality_class;

    if (value > 0 && value <= 20) {
        air_quality_class = "excellent-air";
    } else if (value > 20 && value <= 50) {
        air_quality_class = "good-air";
    } else if (value > 50 && value <= 100) {
        air_quality_class = "moderate-air";
    } else if (value > 100 && value <= 150) {
        air_quality_class = "unhealthy-sensitive-air";
    } else if (value > 150 && value <= 200) {
        air_quality_class = "unhealthy-air";
    } else if (value > 200 && value <= 300) {
        air_quality_class = "very-unhealthy-air";
    } else {
        air_quality_class = "hazardous-air";
    }

    if (nonlocal) {
        default_classes = 'iqair-geopoint ' + (valid ? air_quality_class : 'outdated') + '-outline ';
    }
    else {
        default_classes = 'iqair-geopoint ' + (valid ? air_quality_class : 'outdated') + '-outline-animate ';
    }

    if (!valid) {
        return  default_classes + ' outdated';
    }

    return default_classes + air_quality_class;
}

async function initMap() {
    if (map === undefined) {
        const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
        const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

        map = new ol.Map({
          target: 'map',
          layers: layers,
          view: new ol.View({
            //constrainResolution: true,
            center: vw > 800 ? bishkek : bishkek_m,
            zoom: vw > 800 ? zoom : zoom_m,
          }),
          //controls: [],
        });

        map.on('loadend', function () {
            if (!window.PM25globals.sidebar) {
                jQuery('.ol-zoom').css("display","block");
            }
        });

        //olms.apply(map, styleJson);

        for (let i = 0; i < advanceMarkers.length; i++) {
            let value = advanceMarkers[i].sensorvalue;
            let valid = advanceMarkers[i].valid;
            let code  = advanceMarkers[i].code;

            if (value == 0) {
                next;
            }

            advanceMarkers[i].marker.title       = advanceMarkers[i].title;
            advanceMarkers[i].marker.className   = adjustCSS(value, valid, code.match(reiqair) || code.match(reaqicn));
            advanceMarkers[i].marker.textContent = value;

            if (advanceMarkers[i].transactionHash) {
                advanceMarkers[i].marker.setAttribute("href", "https://sepolia.etherscan.io/tx/" + advanceMarkers[i].transactionHash);
                advanceMarkers[i].marker.setAttribute("target", "_blank");
            }

            markerInstances.push(
                new ol.Overlay({
                    position: [ advanceMarkers[i].position.lng, advanceMarkers[i].position.lat ],
                    positioning: 'center-center',
                    element: advanceMarkers[i].marker,
                    stopEvent: false,
                })
            );

            map.addOverlay(markerInstances[markerInstances.length - 1]);
        }

        jQuery('.ol-overlaycontainer-stopevent').css("z-index","2");
    }

    toggleSpinner(false);
}

function toggleSpinner(show) {
    if (show) {
        if (!jQuery('#img-sandwich').hasClass('is-active')) {
            jQuery('#header-container').removeClass('header-bg');
            jQuery('#header-container').addClass('header-bg-loading');
        }

        jQuery('#map-container').css('display', 'none');
        jQuery('#spinner-container').css('display', 'block');
    }
    else {
        if (!jQuery('#img-sandwich').hasClass('is-active')) {
            jQuery('#header-container').removeClass('header-bg-loading');
            jQuery('#header-container').addClass('header-bg');
        }

        jQuery('#map-container').css('display', 'block');
        jQuery('#spinner-container').css('display', 'none');
    }

    if (navigator.userAgent.includes("17169cb4-bda6-4661-8ce6-f848bd8a4ac7")) {
        jQuery('.ol-overlaycontainer-stopevent').css("display","none");
        jQuery('.header').css("display","none");
    }

    return true;
}

function b64DecodeUnicode(str) {
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

function requestProviders(runfromtimer) {
    const requestProviders = {
        "credentials": {
            "token": "0xd78776d2eec8eb2835e86d7c8bb070abf001543171ffd31f676e6997e6f54f33"
        },
        "method": "GET",
        "route": "/api/ethelia/event/search",
        "payload": {
            "search": {
                "target": "1",
                "value": "*"
            }
        }
    };

    if (!runfromtimer) {
        advanceMarkers = [];
    }

    var jqxhr = jQuery.post(endpoint, JSON.stringify(requestProviders, null, 2), function(response) {
        var response_object = JSON.parse(response);

        if (response_object.content && response_object.content.tparams) {
            var tparams = response_object.content.tparams;

            if (tparams.events && tparams.events.length) {
                // console.log('got ' + trequestProvidersparams.events.length + ' event from endpoint');

                var d = new Date();

                console.log(d.toString() + ': got ' + tparams.events.length + ' event from endpoint');

                for (let i = 0; i < tparams.events.length; i++) {
                    let event = tparams.events[i];
                    var payload;

                    try {
                        payload = JSON.parse(b64DecodeUnicode(event[3]));
                    } catch (e) {
                        console.error('event payload decoding failure: ' + event[3])
                    }

                    const nowts   = Date.parse(new Date().toUTCString())/1000;
                    let event_age = nowts - Date.parse(event[2])/1000;

                    if (event_age < 0) {
                        console.log(event[4] + ' : ' + event[2] + ' - found negative age=' + event_age + ' (now=' + nowts + ')');

                        event_age = maxage;
                    }

                    if (payload && event[0] && payload.enrichment && payload.enrichment.position) {
                        if (runfromtimer && map !== undefined) {
                            for (let i = 0; i < advanceMarkers.length; i++) {
                                if (advanceMarkers[i].address === event[0]) {
                                    let updated_value = payload.sensorvalue;

                                    advanceMarkers[i].code = event[1];
                                    advanceMarkers[i].valid = (event_age < maxage ? true : false);
                                    advanceMarkers[i].sensorvalue = updated_value;
                                    advanceMarkers[i].marker.textContent = updated_value;
                                    advanceMarkers[i].marker.className = adjustCSS(updated_value, advanceMarkers[i].valid, (advanceMarkers[i].code.match(reiqair) || advanceMarkers[i].code.match(reaqicn)));

                                    if (payload.enrichment.transactionHash) {
                                        advanceMarkers[i].transactionHash = payload.enrichment.transactionHash;
                                        advanceMarkers[i].marker.setAttribute("href", "https://sepolia.etherscan.io/tx/" + payload.enrichment.transactionHash);
                                        advanceMarkers[i].marker.setAttribute("target", "_blank");
                                    }

                                    // console.log('regular update for ' + advanceMarkers[i].address + ' value ' + updated_value);
                                }
                            }
                        } else {
                            advanceMarkers.push({
                                address: event[0],
                                code: event[1],
                                valid: (event_age < maxage ? true : false),
                                sensorvalue: payload.sensorvalue || 0,
                                title: (event[1].match(reiqair) || event[1].match(reaqicn) ? '' : '🏆  ') + payload.enrichment.title || 'sample title',
                                position: payload.enrichment.position,
                                transactionHash: (payload.enrichment.transactionHash ? payload.enrichment.transactionHash : undefined),
                                marker: document.createElement('a'),
                            });
                        }
                    }
                }

                if (!runfromtimer || jQuery('#map-container').css('display') === 'none') {
                    initMap();
                }
            }
        }
    })
    .fail(function() {
        toggleSpinner(true);
        console.error('request is failed');
    })
    .always(function() {
        // console.log('request is finished');
    });
}

initEnv();
requestProviders(false);

timer_id = setInterval(requestProviders, refresh, true);

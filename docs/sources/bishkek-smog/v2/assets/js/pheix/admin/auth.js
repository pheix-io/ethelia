import {loadAPI_v2, b64DecodeUnicode} from '../api.js';

function resetContent() {
    var spinner = '<i class="fa fa-cog fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>';

    riot.unmount('pheix-content', true);

    jQuery('.pheix-spinner-page').empty();
    jQuery('.pheix-spinner-page').append(spinner);

    console.log('try to reset content to default state');
}

export function getEmojiFlags(ca_codes) {
    let flags = [];

    if (ca_codes.length) {
        ca_codes.forEach((ca_code) => {
            if (!ca_code || ca_code === 'undef' || !ca_code.match(/^[A-Z]{2}$/)) {
                flags.push('🏴‍☠️');

                return;
            }

            flags.push(
                String.fromCodePoint(0x1F1E6 - 0x41 + ca_code.toUpperCase().charCodeAt(0)) +
                String.fromCodePoint(0x1F1E6 - 0x41 + ca_code.toUpperCase().charCodeAt(1)) +
                //'→' + ca_code
                ' ' + ca_code
            );
        });
    }

    return flags;
}

export function showDialog(message) {
    var content;
    var decoded = b64DecodeUnicode(message);

    try {
        content = JSON.stringify(JSON.parse(decoded), undefined, 2);
        console.log(content);
    }
    catch(e) {
        content = decoded;
        console.log(e);
    }

    jQuery('#admin_message_modal_body').empty();
    jQuery('#admin_message_modal_body').append('<pre>'+content+'</pre>');
    jQuery('#admin_message_modal').modal('show');
}

export function showNotification(details) {
    var defaults = {header: 'Pheix', date: '', text: 'Try to close session in foreground'};

    if (details) {
        defaults.header = details.header ? details.header : defaults.header;
        defaults.text   = details.text ? details.text : defaults.text;
    }

    jQuery('#pheix-notify-header').text(defaults.header);
    jQuery('#pheix-notify-date').text(defaults.date);
    jQuery('#pheix-notify-text').text(defaults.text);

    var domToast = document.getElementById('liveToast');
    var bsNotify = new bootstrap.Toast(domToast);

    bsNotify.show();
}

export function login(t) {
    var sesstoken = t || '0x0';

    var login = jQuery('#ethereumAddress').val() || '';
    var passw = jQuery('#userPassword').val() || '';

    loadAPI_v2('page', {sesstoken: sesstoken, login: login, password: passw}, 'GET', '/api/admin/auth', '200', '', null, false, null);

    resetContent();

    var loginEssentials = document.getElementById("login-essentials");

    if(loginEssentials) {
        jQuery('#login-essentials').hide();

        console.log('hide login essentials container');
    }

    console.log('try to login for admin panel with sesstoken=' + sesstoken);
}

export function refresh(tab) {
    if (window.PheixAPI.isLoading) {
        showNotification({text: 'Skip refresh due to request API in background'});
    }
    else {
        var active_tab = jQuery("ul#adminNav li a.active").attr('id').replace('select','');

        console.log("nav active tab: " + active_tab);

        loadAPI_v2('page', {token: '0x0'}, 'GET', '/api/admin/session/refresh', '200', '', {targettab: active_tab}, false, null);

        resetContent();

        console.log('try to refresh data for authenticated session in foreground');
    }
}

export function exit() {
    if (window.PheixAPI.isLoading) {
        showNotification({text: 'Skip exit due to request API in background'});
    }
    else {
        loadAPI_v2('page', {token: '0x0'}, 'GET', '/api/admin/session/close', '200', '', null, false, null);

        resetContent();
        showNotification();

        console.log('try to close session in foreground');
    }
}

export function validate_in_background(riotobj) {
    if (!window.PheixAPI.isLoading) {
        loadAPI_v2('page', {token: '0x0'}, 'GET', '/api/admin/session/validate', '200', '', null, true, riotobj);

        console.log('try to validate session in background');

        /* showNotification({header: 'Pheix', date: '', text: 'Try to validate session in background'}); */
    }
}

export function extend_in_background(riotobj) {
    if (!window.PheixAPI.isLoading) {
        loadAPI_v2('page', {token: '0x0'}, 'GET', '/api/admin/session/extend', '200', '', null, true, riotobj);

        console.log('try to extend session in background');

        showNotification({text: 'Try to extend session in background'});
    }
}

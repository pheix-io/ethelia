import * as PheixAuth from './admin/auth.js';

const endpoint = 'http://pheix.webtech-omen';

window.PheixAPI  = { "isLoading" : false };
window.PheixAuth = PheixAuth;

function logAgent(entry) {
    if (entry) {
        var log_data = {
            //log: b64EncodeUnicode(entry)
            log: entry
        };

        var jqxhr = jQuery.post(endpoint + '/logger', JSON.stringify(log_data, null, 2), function(response) {
            console.log('Logger request is ok');
        })
        .fail(function() {
            console.log('Logger request is failed');
        })
        .always(function() {
            console.log('Logger request is finished');
        });
    }
}

function riotRender(component, tparams, jspayload) {
    var riot_code;

    var c_default = '<div><div class="alert alert-danger" role="alert">Request to API backend failed — <a href="javascript:top.location.reload()">try</a> once more?!</div></div>';

    /* try to compile the component */
    try {
        riot_code = riot.compileFromString(component || c_default).code;
    }
    catch(e) {
        riot_code = riot.compileFromString(c_default).code;

        logAgent('riotRender() compile exception <' + e.message + '>, component: ' + component);
    }

    /* try to inject the component */
    try {
        tparams.jspayload = jspayload;

        riot.inject(riot_code, 'pheix-content', './pheix-content.riot');
        const components = riot.mount('pheix-content', tparams || {});

        // throw new ReferenceError('Synthetic exception', 'api.js', 41);

        if (components.length) {
            jQuery('.pheix-spinner-page').empty();
        }
        else {
            throw new ReferenceError('Components are empty after riot.mount()', 'api.js', 52);
        }
    }
    catch(e) {
        var exception_msg = '<div><div class="alert alert-danger" role="alert">Caught exception while requesting backend</div></div>';

        jQuery('.pheix-spinner-page').empty();
        jQuery('.pheix-spinner-page').append(exception_msg);

        logAgent('riotRender() inject exception <' + e.message + '>, component: ' + component);
    }
}

export function loadAPI_v2(handler, credentials, method, route, httpstat, msg, jspayload, skiprender, riotobj) {
    var request_data = {
        credentials: credentials,
        method: method,
        route:  encodeURIComponent(route),
        httpstat: httpstat,
        message: msg
    };

    if (window.PheixAPI.isLoading) {
        /*console.warn('found another working instance of loadAPI_v2()');

        jQuery('#liveToast').toast('show');*/

        return false;
    }

    window.PheixAPI = { "isLoading" : true };

    var ret;
    var content = null;
    var json_text = JSON.stringify(request_data, null, 2);

    console.log('Request:');
    console.log(json_text);

    if (json_text) {
        jQuery("#top-navigation,#right-announcements").each(function(){
            jQuery(this).addClass('pheix-disable-container');
        });

        var jqxhr = jQuery.post(endpoint + '/api', json_text, function(response) {
            var response_obj = JSON.parse(response);
            var component_render;

            if (response_obj.content) {
                component_render = response_obj.content.component_render ?
                    ' (' + response_obj.content.component_render + ')' :
                        '';
            }

            console.log(
                response_obj.msg + ', render time: ' +
                response_obj.render + component_render
            );

            if (response_obj.status == 1) {
                if (handler === 'captcha' && !skiprender) {
                    if (response_obj.content) {
                        var isImage = new RegExp('^data:image/png;base64,');

                        if (isImage.test(response_obj.content)) {
                            jQuery('#pheix-captcha').attr('src', response_obj.content);
                        }
                    }
                    else {
                        logAgent('null captcha, possible currupted token: ' + credentials.token);
                    }
                }
                else if (!skiprender) {
                    try {
                        ret = b64DecodeUnicode(response_obj.content.component);

                        content = response_obj.content;
                    }
                    catch(e) {
                        ret = response_obj.content.component;
                        logAgent('b64DecodeUnicode() exception <' + e.message + '>, component: ' + response_obj.content.component);
                    }

                    riotRender(ret, response_obj.content.tparams, jspayload);

                    if (jQuery('#pheix-exception-msg').text()) {
                        jQuery('#pheix-exception-msg').removeClass('d-none');
                    }
                }
                else {
                    console.log('try to update the session for next ' + response_obj.content.tparams.session + ' seconds');

                    if (riotobj) {
                        if (response_obj.content.sesstatus) {
                            riotobj.update({
                               timeout: 0,
                               addr: response_obj.content.tparams.addr,
                               pkey: response_obj.content.tparams.pkey,
                               tx: response_obj.content.tparams.tx,
                               pheixauth: response_obj.content.tparams.pheixauth ? response_obj.content.tparams.pheixauth : "😵",
                               sesstime: response_obj.content.tparams.session,
                               tryextend: response_obj.content.tryextend ? 'True' : 'False',
                            });
                        }
                        else {
                            PheixAuth.showNotification({text: 'The authenticated session is closed'});

                            console.error('lost session is found');
                        }
                    }
                    else {
                        console.error('riot object is missed');
                    }
                }
            }
            else {
                if (handler !== 'captcha' && !skiprender) {
                    ret =
                        '<div><div class="alert alert-warning" role="alert">' +
                        response_obj.content + '</div></div>'
                    ;
                    riotRender(ret);
                }

                logAgent(response_obj.content);
            }
        })
        .fail(function() {
            if (handler !== 'captcha' && !skiprender) {
                riotRender();
            }

            logAgent('API request is failed: ' + json_text);
            console.log('API request is failed');
        })
        .always(function() {
            console.log('API request is finished');

            window.PheixAPI = { "isLoading" : false };

            jQuery("#top-navigation,#right-announcements").each(function(){
                jQuery(this).removeClass('pheix-disable-container');
            });
        });
    }
    else {
        if (!skiprender) {
            riotRender();
        }

        logAgent('empty JSON request: ' + json_text);
        console.log('empty JSON request');
    }

    return true;
}

/*
 * https://stackoverflow.com/questions/30106476/using-javascripts-atob-to-decode-base64-doesnt-properly-decode-utf-8-strings
 */

export function b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

function b64EncodeUnicode(str) {
    // first we use encodeURIComponent to get percent-encoded UTF-8,
    // then we convert the percent encodings into raw bytes which
    // can be fed into btoa.
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
    }));
}

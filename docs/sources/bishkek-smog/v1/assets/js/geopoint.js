/*
 * @license
 * Copyright 2019 Google LLC. All Rights Reserved.
 * SPDX-License-Identifier: Apache-2.0
 */
const maxage   = 7200;
const endpoint = "http://pheix.webtech-omen/api";
var advanceMarkers = [];
var markerInstances = [];

function adjustCSS(value, valid) {
    if (!valid) {
        return 'pm25-geopoint shadow outdated';
    }

    var air_quality_class;

    if (value > 0 && value <= 20) {
        air_quality_class = "excellent-air";
    } else if (value > 20 && value <= 50) {
        air_quality_class = "good-air";
    } else if (value > 50 && value <= 100) {
        air_quality_class = "moderate-air";
    } else if (value > 100 && value <= 150) {
        air_quality_class = "unhealthy-sensitive-air";
    } else if (value > 150 && value <= 200) {
        air_quality_class = "unhealthy-air";
    } else if (value > 200 && value <= 300) {
        air_quality_class = "very-unhealthy-air";
    } else {
        air_quality_class = "hazardous-air";
    }

    return 'pm25-geopoint shadow ' + air_quality_class;
}

async function initMap() {
    const {
        Map
    } = await google.maps.importLibrary("maps");
    const {
        AdvancedMarkerElement
    } = await google.maps.importLibrary("marker");

    const map = new Map(document.getElementById("map"), {
        center: {
            lat: 42.856880,
            lng: 74.607386
        },
        zoom: 13,
        mapId: "6387db97109c0f4",
    });

    for (let i = 0; i < advanceMarkers.length; i++) {
        let value = advanceMarkers[i].sensorvalue;
        let valid = advanceMarkers[i].valid;

        if (value == 0) {
            next;
        }

        advanceMarkers[i].marker.className = adjustCSS(value, valid);
        advanceMarkers[i].marker.textContent = value;

        markerInstances.push(
            new AdvancedMarkerElement({
                map,
                position: advanceMarkers[i].position,
                title: advanceMarkers[i].title,
                content: advanceMarkers[i].marker,
            })
        );
    }
}

function b64DecodeUnicode(str) {
    return decodeURIComponent(atob(str).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

function requestProviders(runfromtimer) {
    var requestProviders = {
        "credentials": {
            "token": "0xd78776d2eec8eb2835e86d7c8bb070abf001543171ffd31f676e6997e6f54f33"
        },
        "method": "GET",
        "route": "/api/ethelia/event/search",
        "payload": {
            "search": {
                "target": "1",
                "value": "*"
            }
        }
    };

    var jqxhr = jQuery.post(endpoint, JSON.stringify(requestProviders, null, 2), function(response) {
            var response_object = JSON.parse(response);

            if (response_object.content && response_object.content.tparams) {
                var tparams = response_object.content.tparams;

                if (tparams.events && tparams.events.length) {
                    // console.log('got ' + tparams.events.length + ' event from endpoint');

                    for (let i = 0; i < tparams.events.length; i++) {
                        let event = tparams.events[i];
                        var payload;

                        try {
                            payload = JSON.parse(b64DecodeUnicode(event[3]));
                        } catch (e) {
                            console.error('event payload decoding failure: ' + event[3])
                        }

                        let event_age = Date.parse(new Date().toUTCString())/1000 - Date.parse(event[2])/1000;

                        if (payload && event[0] && payload.enrichment && payload.enrichment.position) {
                            if (runfromtimer) {
                                for (let i = 0; i < advanceMarkers.length; i++) {
                                    if (advanceMarkers[i].address === event[0]) {
                                        let updated_value = payload.sensorvalue;

                                        advanceMarkers[i].valid = (event_age < maxage ? true : false);
                                        advanceMarkers[i].sensorvalue = updated_value;
                                        advanceMarkers[i].marker.textContent = updated_value;
                                        advanceMarkers[i].marker.className = adjustCSS(updated_value, advanceMarkers[i].valid);

                                        //console.log('regular update for ' + advanceMarkers[i].address);
                                    }
                                }
                            } else {
                                advanceMarkers = [];
                                advanceMarkers.push({
                                    address: event[0],
                                    valid: (event_age < maxage ? true : false),
                                    sensorvalue: payload.sensorvalue || 0,
                                    title: payload.enrichment.title || 'sample title',
                                    position: payload.enrichment.position,
                                    marker: document.createElement('div'),
                                });

                                //console.log('initial update for ' + event[0]);
                            }
                        }
                    }

                    if (!runfromtimer) {
                        initMap();
                    }
                }
            }
        })
        .fail(function() {
            console.error('request is failed');
        })
        .always(function() {
            // console.log('request is finished');
        });
}

requestProviders(0);

setInterval(requestProviders, 30000, 1);

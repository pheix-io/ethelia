#!/bin/env raku

use Pheix::Model::Database::Access;
use Node::Ethereum::Keccak256::Native;
use Pheix::Model::Database::Blockchain::SendTx;
use Node::Ethereum::KeyStore::V3;
use Ethelia::Explorer;
use JSON::Fast;

constant markdownfile = sprintf("%s-markdown.txt", DateTime.now(formatter => { sprintf("%d-%02d-%02d_%s", .year, .month, .day, .hh-mm-ss) }));
constant explorerurlf = "https://sepolia.etherscan.io/tx/%s";

my $jsonobj;
my $remotetab;
my $datatab    = 'ExtensionEthelia';

sub MAIN (
    Str :$table = 'ExtensionEthelia',
    Str :$storageconfig = sprintf("%s/git/pheix-pool/core-perl6/conf", $*HOME),
    Str :$remotetableprefix = 'remote',
    Str :$abi = sprintf("%s/git/pheix-pool/core-perl6/conf/system/eth/PheixDatabase.abi", $*HOME),
    Str :$logfile!,
    Str :$additionallogfile,
) {
    die sprintf("logfile %s is not found", $logfile) unless $logfile.IO.f;

    my $trxdecoder = Ethelia::Explorer.new(:$abi);

    $datatab = sprintf("%s/%s", $remotetableprefix, $table);
    $jsonobj = Pheix::Model::JSON.new(:path($storageconfig));

    my %tabsets = $jsonobj.get_all_settings_for_group_member('Pheix', 'storage', $datatab);

    die sprintf("no table %s is found", $datatab) unless %tabsets && %tabsets.keys;

    sprintf("found table %s in Pheix settings (type=%d, hash=%s)", $table, %tabsets<type>, %tabsets<hash>).say;

    my $dbobj = Pheix::Model::Database::Access.new(
        :table($datatab),
        :fields(List.new),
        :jsonobj($jsonobj)
    );

    die 'blockchain database object failure' unless $dbobj && $dbobj.dbswitch == 1;

    my $ethobj = $dbobj.chainobj.ethobj;

    $ethobj.keepalive = False;
    $ethobj.abi       = $trxdecoder.abi.IO.slurp;

    my @log = $logfile.IO.lines.map({ $_.split(q{|}, :skip-empty).map({ $_.trim }) });

    die 'no log records' unless @log && @log.elems;

    my $naive_database = process_logrecords(:$trxdecoder, :$ethobj, :@log);

    die 'no records on native blockchain database' unless $naive_database && $naive_database.keys;

    if $additionallogfile && $additionallogfile.IO.f {
        sprintf("Additional logfile %s is found", $additionallogfile).say;

        my @log = $additionallogfile.IO.lines.map({ $_.split(q{|}, :skip-empty).map({ $_.trim }) });

        die 'no additional log records' unless @log && @log.elems;

        my $extended_database = process_logrecords(:$trxdecoder, :$ethobj, :@log);

        die 'no records on additional blockchain database' unless $naive_database && $naive_database.keys;

        $naive_database.keys.map({
            if $extended_database{$_} {
                $naive_database{$_}<additional_db> = $extended_database{$_}:delete;
            }
        });

        $naive_database<additional_db> = $extended_database;
    }

    my $data;

    $naive_database.keys.sort.map({
        $data ~= sprintf("| %s | [%d](%s) | %s |\n",
            $_,
            $naive_database{$_}<sensorvalue>,
            $naive_database{$_}<link>,
            sprintf($naive_database{$_}<additional_db> && $naive_database{$_}<additional_db>.keys ?? "[%d](%s)" !! "—%s%s",
                $naive_database{$_}<additional_db><sensorvalue> // q{},
                $naive_database{$_}<additional_db><link> // q{}
            )
        ) if $_ ne 'additional_db';
    });

    if $naive_database<additional_db> && $naive_database<additional_db>.keys {
        $naive_database<additional_db>.keys.sort.map({
            $data ~= sprintf("| %s | — | [%d](%s) |\n",
                $_,
                $naive_database<additional_db>{$_}<sensorvalue>,
                $naive_database<additional_db>{$_}<link>
            );
        });
    }

    $data.say;

    markdownfile.IO.spurt($data);

    return 0;
}

sub process_logrecords(:$trxdecoder!, :$ethobj!, :@log!) returns Hash {
    my $naive_database;

    for @log.kv -> $num, $record {
        my $trxhash  = $record[4];
        my $nonce    = $record[1].UInt;
        my $function = $record[5];

        next unless $trxhash ~~ m:i/^ 0x<xdigit>**64 $/;

        my $trx = $ethobj.eth_getTransactionByHash($trxhash);

        next unless $trx.keys && $trx<input> && $trx<input> ~~ m:i/^ 0x<xdigit>+ $/;

        my $input = $trxdecoder.decode(:$function, :inputdata($trx<input>));

        next unless $input && $input.keys && $input<rowdata><payload>;

        $naive_database{$input<rowdata><payload><enrichment><title>} = {
            sensorvalue => $input<rowdata><payload><sensorvalue>,
            link => sprintf(explorerurlf, $trxhash)
        };

        sprintf("transaction %s is processed", $trxhash).say;
    }

    return $naive_database // {};
}

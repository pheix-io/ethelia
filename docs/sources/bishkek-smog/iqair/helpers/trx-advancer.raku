#!/bin/env raku

use Pheix::Model::Database::Access;
use Node::Ethereum::Keccak256::Native;
use Pheix::Model::Database::Blockchain::SendTx;
use Node::Ethereum::KeyStore::V3;
use Ethelia::Explorer;

constant blocksearchiters = 100;
constant blockshift       = 500;
constant explorerurlf     = "https://sepolia.etherscan.io/tx/%s";

my $jsonobj;
my $remotetab;
my $datatab = 'ExtensionEthelia';

sub MAIN (
    Str :$trxlog!,
    Str :$table = 'ExtensionEthelia',
    Str :$storageconfig = sprintf("%s/git/pheix-pool/core-perl6/conf", $*HOME),
    Str :$remotetableprefix = 'remote',
    Str :$abi = sprintf("%s/git/pheix-pool/core-perl6/conf/system/eth/PheixDatabase.abi", $*HOME),
) {
    die sprintf("input log file %s is not found", $trxlog) unless $trxlog.IO.f;

    die sprintf("abi file %s is not found", $abi) unless $abi.IO.f;

    my $start      = now;
    my $trxdecoder = Ethelia::Explorer.new(:$abi);

    $datatab = sprintf("%s/%s", $remotetableprefix, $table);
    $jsonobj = Pheix::Model::JSON.new(:path($storageconfig));

    my %tabsets = $jsonobj.get_all_settings_for_group_member('Pheix', 'storage', $datatab);

    die sprintf("no table %s is found", $datatab) unless %tabsets && %tabsets.keys;

    sprintf("found table %s in Pheix settings (type=%d, hash=%s)", $table, %tabsets<type>, %tabsets<hash>).say;

    my $dbobj = Pheix::Model::Database::Access.new(
        :table($datatab),
        :fields(List.new),
        :jsonobj($jsonobj)
    );

    die 'blockchain database object failure' unless $dbobj && $dbobj.dbswitch == 1;

    my $ethobj = $dbobj.chainobj.ethobj;

    $ethobj.keepalive = False;
    $ethobj.abi       = $trxdecoder.abi.IO.slurp;

    my $actualnonce = $ethobj.eth_getTransactionCount(
        :data($dbobj.chainobj.ethacc),
        :tag('latest')
    );

    sprintf("logfile %s, actual nonce %d", $trxlog, +$actualnonce).say;

    my @log = $trxlog.IO.lines.map({ $_.split(q{|}, :skip-empty).map({ $_.trim }) });

    my $signer = Pheix::Model::Database::Blockchain::SendTx
        .new(
            :signerobj($dbobj.chainobj.sgnobj),
            :targetobj($dbobj.chainobj),
            :debug(True),
            :diag($dbobj.chainobj.diag)
        );

    die 'no keystore found' unless $signer.signerobj.config<keystore> && $signer.signerobj.config<keystore>.IO.f;

    die 'no keystore password' unless $signer.signerobj.ethobj.unlockpwd;

    my $privatekey = Node::Ethereum::KeyStore::V3
        .new(:keystorepath($signer.signerobj.config<keystore>))
        .decrypt_key(:password($signer.signerobj.ethobj.unlockpwd));

    for @log.kv -> $num, $record {
        my $trxhash  = $record[4];
        my $nonce    = $record[1].UInt;
        my $function = $record[5];

        next unless $nonce >= $actualnonce;

        next unless $trxhash ~~ m:i/^ 0x<xdigit>**64 $/ && $nonce > 0;

        my $trx = $ethobj.eth_getTransactionByHash($trxhash);

        if !$trx.keys {
            my $receipt = $ethobj.eth_getTransactionReceipt($trxhash);

            if !$receipt || !$receipt.keys || !$receipt<status> {
                my %trx =
                    from  => $dbobj.chainobj.ethacc,
                    to    => $dbobj.chainobj.ethacc,
                    nonce => $nonce,
                    value => 0;

                my $esimated_gas = $ethobj.eth_estimateGas(%trx);
                my $fees         = $ethobj.get_fee_data;

                %trx<gas>                  = ($esimated_gas * $signer.gasprmult).Int;
                %trx<maxfeepergas>         = $fees<maxFeePerGas>;
                %trx<maxpriorityfeepergas> = $fees<maxPriorityFeePerGas>;

                # %trx.gist.say;

                my %sign = $signer.sign_transaction(
                    :trx(%trx),
                    :privatekey($privatekey)
                );

                die 'sign transaction failure' unless %sign<raw> ~~ m:i/^ 0x<xdigit>+ $/;

                my $updatedtrx = $ethobj.eth_sendRawTransaction(:data(%sign<raw>));

                sprintf("%03d - processed initial trx %s with nonce=%d: %s", $num, $trxhash, $nonce, $updatedtrx).say;

                die 'transaction is not mined' unless $dbobj.chainobj.wait_for_transactions(:hashes([$updatedtrx]));
            }
        }
        else {
            my $input = $trxdecoder.decode(:$function, :inputdata($trx<input>));

            my %data =
                tabname => $datatab,
                rowid   => $input<rowid><reference>,
                rowdata => $ethobj.hex2buf($input<rowdata><hexdata>),
                comp      => $input<comp><reference>,
                signature => $dbobj.chainobj.signaturefactory.sign(:message($input<rowdata><filechain>));

            my $mdata = $ethobj.marshal($function, %data);

            my %trx =
                from  => $dbobj.chainobj.ethacc,
                to    => $dbobj.chainobj.scaddr,
                nonce => $nonce,
                data  => $mdata;

            my $esimated_gas = $ethobj.eth_estimateGas(%trx);
            my $fees         = $ethobj.get_fee_data;

            %trx<gas>                  = ($esimated_gas * $signer.gasprmult).Int;
            %trx<maxfeepergas>         = $fees<maxFeePerGas>;
            %trx<maxpriorityfeepergas> = $fees<maxPriorityFeePerGas>;

            my $updatedtrx;

            while (!$updatedtrx) {
                my %sign = $signer.sign_transaction(
                    :trx(%trx),
                    :privatekey($privatekey)
                );

                die 'sign transaction failure' unless %sign<raw> ~~ m:i/^ 0x<xdigit>+ $/;

                try {
                    $updatedtrx = $ethobj.eth_sendRawTransaction(:data(%sign<raw>));

                    CATCH {
                        default {
                            my $error = .message;

                            if $error ~~ /'replacement transaction underpriced'/ {
                                %trx<maxfeepergas> = %trx<maxfeepergas> * 2;
                                %trx<maxpriorityfeepergas> = %trx<maxpriorityfeepergas> * 2;

                                sprintf("replacement transaction underpriced: fees * 2 -> %d/%d", %trx<maxfeepergas>, %trx<maxpriorityfeepergas>).say;
                            }
                            else {
                                .throw;
                            }
                        }
                    }
                }
            }

            sprintf("%03d - processed initial trx %s with existed data nonce=%d, function=%s: %s", $num, $trxhash, $nonce, $function, $updatedtrx).say;

            die 'transaction is not mined' unless $dbobj.chainobj.wait_for_transactions(:hashes([$updatedtrx]));
        }
    }

    sprintf("advancing is done in %d seconds", (now - $start).Int).say;
}

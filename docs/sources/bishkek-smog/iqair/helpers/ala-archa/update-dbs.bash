#!/bin/bash

ME=`basename "$0"`
STARTDELAY=$1
LOGDIR="$2"
TESTNUMREGEXPR='^[0-9]+$'
CONTAINER=ethelia-tg-bot
WORKDIR=/home/kostas/git/pheix-io/ethelia/sources/bishkek-smog/iqair
CUSTOMLIB=$HOME/nvme/home/apache_root/pheix.org/git/pheix-pool/core-perl6/lib/
ETHELIALIB=$HOME/git/raku-ethelia/lib/
BACKUPDIR=$HOME/webtech/logs/ethelia
LOGFILE=$BACKUPDIR/update-db/${ME}_`date +"%Y%m%d_%H-%M-%S"`.log

if [ -z "${AQICNTOKEN}" ] ; then
	echo "No AQICNTOKEN found"

	exit 1;
fi

if ! [[ $STARTDELAY =~ $TESTNUMREGEXPR ]] ; then
	STARTDELAY=0
else
	echo "set up start delay for ${STARTDELAY} seconds" | \
		xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> ${LOGFILE}
fi

if [ ! -d "${LOGDIR}" ]; then
	LOGDIR=$HOME
fi

echo "fetching logs from ${LOGDIR}" | \
	xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> ${LOGFILE}

RAKULIB="${RAKULIB},${CUSTOMLIB}" ${WORKDIR}/station-parser.raku \
    --token=${AQICNTOKEN} \
	--path=${WORKDIR}/config.json \
	--fetchpos \
	--output=${HOME}/nvme/home/apache_root/pheix.org/www/conf/extensions \
	--storageconfig=${HOME}/nvme/home/apache_root/pheix.org/www/conf/addons/ExtensionEthelia \
	--pheixpath=${HOME}/nvme/home/apache_root/pheix.org/www \
	--remotetableprefix="alchemy" \
	--table="alchemy/ExtensionEthelia" 2>&1 | \
		xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> ${LOGFILE}

for FILE in ${LOGDIR}/station-parser.raku-sgn-*; do
	if [ -f ${FILE} ]; then
		docker cp ${FILE} ${CONTAINER}:/tg-bot/app/`basename "${FILE}"` 2>&1 | \
			xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> ${LOGFILE}
		cp -rf ${FILE} ${BACKUPDIR} && rm -f ${FILE}
	fi;
done

sleep $STARTDELAY

RAKULIB="${RAKULIB},${CUSTOMLIB}" ${WORKDIR}/station-parser.raku \
    --token=${AQICNTOKEN} \
	--local \
	--path=${WORKDIR}/config.json \
	--fetchpos \
	--output=${HOME}/nvme/home/apache_root/pheix.org/www/conf/extensions \
	--storageconfig=${HOME}/nvme/home/apache_root/pheix.org/www/conf/addons/ExtensionEthelia \
	--pheixpath=${HOME}/nvme/home/apache_root/pheix.org/www \
	--remotetableprefix=alchemy \
	--table=ExtensionEthelia 2>&1 | \
		xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> ${LOGFILE}

RAKULIB="${RAKULIB},${CUSTOMLIB},${ETHELIALIB}" ${WORKDIR}/helpers/trx-advancer.raku \
	--trxlog=${BACKUPDIR}/`ls -tp ${BACKUPDIR} | grep -v /$ | head -1` \
	--storageconfig=${HOME}/nvme/home/apache_root/pheix.org/www/conf/addons/ExtensionEthelia \
	--abi=${HOME}/nvme/home/apache_root/pheix.org/git/pheix-pool/core-perl6/conf/system/eth/PheixDatabase.abi \
	--remotetableprefix=alchemy 2>&1 | \
		xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> ${LOGFILE}

docker cp ${LOGFILE} ${CONTAINER}:/tg-bot/app/`basename "${LOGFILE}"` 2>&1 | \
	xargs -I{} logger --no-act -s "{}" 2>&1 | sed 's/^<[0-9]\+>//' >> ${LOGFILE}

exit 0

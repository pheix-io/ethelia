#!/bin/bash

DATEW="$1"

if [ -z "${DATEW}" ]; then
	DATEW=`docker exec ethelia-tg-bot /bin/bash -c "date '+%Y%m%d'"`
else
	echo "Use date from arg ${DATEW}"
fi

mkdir -p /tmp/${DATEW} && rm -f /tmp/${DATEW}/*

docker exec ethelia-tg-bot /bin/bash -c "cd /tg-bot/app/staticmaps && ls | grep \"${DATEW}\" | tar -T - -czf ${DATEW}.maps.tgz"
docker cp ethelia-tg-bot:/tg-bot/app/staticmaps/${DATEW}.maps.tgz /tmp/${DATEW}

cd /tmp/${DATEW} && tar -xzf ${DATEW}.maps.tgz

for FILE in ./*.png; do
        TIME=`echo $FILE | perl -lne 'print "$1:$2:$3" if /(\d\d)(\d\d)(\d\d)\.png$/'`
        convert ${FILE} -fill white -font DejaVu-Sans-Mono-Bold -gravity NorthWest -pointsize 30 -annotate +10+10 "${TIME}" ${FILE}
        # convert -quality 80% ${FILE} ${FILE}.jpg && rm ${FILE};
done

ffmpeg -framerate 1 -pattern_type glob -i '*.png' ${DATEW}-animatedmap.mp4 && rm -f *.jpg

docker exec ethelia-tg-bot /bin/bash -c "mkdir -p /tg-bot/app/staticmaps/animated"
docker cp ${DATEW}-animatedmap.mp4 ethelia-tg-bot:/tg-bot/app/staticmaps/animated/${DATEW}-animatedmap.mp4

rm -rf /tmp/${DATEW}

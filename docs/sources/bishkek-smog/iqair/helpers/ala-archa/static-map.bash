#!/bin/bash

FILENAME="map_`date '+%Y%m%d_%H%M%S'`.png"
CONTAINER=ethelia-tg-bot
EXPORTPATH=/tg-bot/app/staticmaps/${FILENAME}
GLOBALTIMEOUT=60
TIMEBUDGET=60000
WINDOWSIZE="640,1024"
USERAGENT="Chrome-Headless-Shell/17169cb4-bda6-4661-8ce6-f848bd8a4ac7"
URL=https://pm25.online
SCREENSHOTPATH=/tmp/${FILENAME}
HEADERS="Set-Cookie: _pm25_online_mode=night"

# v1
# timeout ${GLOBALTIMEOUT} google-chrome --no-sandbox --headless=old --screenshot=${SCREENSHOTPATH} --run-all-compositor-stages-before-draw --window-size=${WINDOWSIZE} --virtual-time-budget=${TIMEBUDGET} --disable-gpu --hide-scrollbars --user-agent=${USERAGENT} ${URL} 2>&1 | logger -t "headlesschrome"

# v2
#timeout ${GLOBALTIMEOUT} google-chrome --no-sandbox --headless=new --screenshot=${SCREENSHOTPATH} --run-all-compositor-stages-before-draw --virtual-time-budget=${TIMEBUDGET} --disable-gpu --hide-scrollbars --user-agent=${USERAGENT} ${URL} 2>&1 | logger -t "headlesschrome"

# v3: https://googlechromelabs.github.io/chrome-for-testing/
timeout ${GLOBALTIMEOUT} ${HOME}/webtech/distrib/chrome-headless/chrome-headless-shell-linux64/chrome-headless-shell --no-sandbox --disable-cookie-encryption --user-data-dir=${HOME}/utils/ethelia/iqair/chrome-data --window-size=${WINDOWSIZE} --screenshot=${SCREENSHOTPATH} --run-all-compositor-stages-before-draw --virtual-time-budget=${TIMEBUDGET} --disable-gpu --hide-scrollbars --user-agent=${USERAGENT} ${URL} 2>&1 | logger -t "headlesschromeshell"
docker cp ${SCREENSHOTPATH} ${CONTAINER}:${EXPORTPATH} 2>&1 | logger -t "dockerscreenshotcopy"

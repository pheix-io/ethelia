#!/bin/env raku

use Pheix::Model::Database::Access;
use Node::Ethereum::Keccak256::Native;
use Pheix::Model::Database::Blockchain::SendTx;
use Node::Ethereum::KeyStore::V3;
use Ethelia::Explorer;

constant blocksearchiters = 25;
constant blockshift       = 650;
constant explorerurlf     = "https://sepolia.etherscan.io/tx/%s";

my $jsonobj;
my $remotetab;
my $datatab    = 'ExtensionEthelia';
my $trxdecoder = Ethelia::Explorer.new;

sub MAIN (
    Str  :$table = 'ExtensionEthelia',
    Str  :$storageconfig = sprintf("%s/git/pheix-pool/core-perl6/conf", $*HOME),
    Str  :$remotetableprefix = 'remote',
    UInt :$block = 0
) {
    $datatab = sprintf("%s/%s", $remotetableprefix, $table);
    $jsonobj = Pheix::Model::JSON.new(:path($storageconfig));

    my %tabsets = $jsonobj.get_all_settings_for_group_member('Pheix', 'storage', $datatab);

    die sprintf("no table %s is found", $datatab) unless %tabsets && %tabsets.keys;

    sprintf("found table %s in Pheix settings (type=%d, hash=%s)", $table, %tabsets<type>, %tabsets<hash>).say;

    my $dbobj = Pheix::Model::Database::Access.new(
        :table($datatab),
        :fields(List.new),
        :jsonobj($jsonobj)
    );

    die 'blockchain database object failure' unless $dbobj && $dbobj.dbswitch == 1;

    my $ethobj = $dbobj.chainobj.ethobj;

    $ethobj.keepalive = False;
    $ethobj.abi       = $trxdecoder.abi.IO.slurp;

    my $actualnonce = $ethobj.eth_getTransactionCount(
        :data($dbobj.chainobj.ethacc),
        :tag('latest')
    );

    my @log = _latest_mined_trx(:$dbobj, :$block);

    my $signer = Pheix::Model::Database::Blockchain::SendTx
        .new(
            :signerobj($dbobj.chainobj.sgnobj),
            :targetobj($dbobj.chainobj),
            :debug(True),
            :diag($dbobj.chainobj.diag)
        );

    die 'no keystore found' unless $signer.signerobj.config<keystore> && $signer.signerobj.config<keystore>.IO.f;

    die 'no keystore password' unless $signer.signerobj.ethobj.unlockpwd;

    my $privatekey = Node::Ethereum::KeyStore::V3
        .new(:keystorepath($signer.signerobj.config<keystore>))
        .decrypt_key(:password($signer.signerobj.ethobj.unlockpwd));

    for @log.kv -> $num, $record {
        my $trxhash  = $record[4];
        my $nonce    = $record[1].UInt;
        my $function = 'set';

        next unless $nonce < $actualnonce;

        next unless $trxhash ~~ m:i/^ 0x<xdigit>**64 $/ && $nonce > 0;

        my $trx = $ethobj.eth_getTransactionByHash($trxhash);

        if !$trx.keys {
            my $receipt = $ethobj.eth_getTransactionReceipt($trxhash);

            if !$receipt || !$receipt.keys || !$receipt<status> {
                die sprintf("transaction %s with nonce=%d is not mined'", $trxhash, $nonce);
            }
        }
        else {
            my $input;
            my $idkey = 'rowid';

            try {
                $input = $trxdecoder.decode(:$function, :inputdata($trx<input>));

                CATCH {
                    default {
                        $function = 'insert';
                        $idkey    = 'id';
                        $input    = $trxdecoder.decode(:$function, :inputdata($trx<input>));

                        $input.gist.say;
                    }
                }
            }

            my $created = $input<rowdata><filechain>.split(q{|}, :skip-empty)[3];

            my %data =
                tabname => $datatab,
                rowid   => $input{$idkey}<reference>,
                rowdata => $ethobj.hex2buf($input<rowdata><hexdata>),
                comp      => $input<comp><reference>,
                signature => $dbobj.chainobj.signaturefactory.sign(:message($input<rowdata><filechain>));

            sprintf("%03d - trx %s with nonce=%d, function=%s: id=%09d, created=%s, title=%s, sensorvalue=%d", $num, $trxhash, $nonce, $function, $input{$idkey}<reference>, $created, $input<rowdata><payload><enrichment><title>, $input<rowdata><payload><sensorvalue>).say;
        }
    }
}

sub _latest_mined_trx(:$dbobj!, UInt :$block) returns List {
    my @trx;
    my $ethobj = $dbobj.chainobj.ethobj;

    my $nonce = $ethobj.eth_getTransactionCount(
        :data($dbobj.chainobj.ethacc),
        :tag('latest')
    );

    my $evsignhex      = $ethobj.string2hex($dbobj.chainobj.evsign);
    my $event_sig      = Node::Ethereum::Keccak256::Native.new.keccak256(:msg($ethobj.hex2buf($evsignhex)));
    my $tabbuf         = $ethobj.hex2buf($ethobj.string2hex($dbobj.table));
    my buf8 $tablepad  = buf8.new(0 xx (32 - $tabbuf.bytes)).unshift($tabbuf);
    my $latest_block   = $block // $ethobj.eth_blockNumber;
    my $earliest_block = $block // ($latest_block - (blockshift * blocksearchiters));

    sprintf("found the latest nonce=%d, smart contract addr %s, latest block=%d, earliest block=%d", $nonce, $dbobj.chainobj.scaddr, $latest_block, $earliest_block).say;

    my $index = 0;

    while ($index < blocksearchiters) {
        sprintf("search topics on range [%d..%d]", $earliest_block, $earliest_block + blockshift).say;

        my %filter =
            fromBlock => sprintf('0x%x', $earliest_block),
            toBlock   => sprintf('0x%x', $earliest_block + blockshift),
            address   => $dbobj.chainobj.scaddr,
            topics    => [
                $ethobj.buf2hex($event_sig).lc,
                $ethobj.buf2hex($tablepad).lc
            ];

        my $ret = $ethobj.eth_getLogs(%filter);

        if $ret && $ret.elems {
            for $ret.values -> $transaction {

                next unless ($transaction<transactionHash>:exists) && $transaction<transactionHash> ~~ m:i/^ 0x<xdigit>**64 $/;

                my $trxdetails = $ethobj.eth_getTransactionByHash($transaction<transactionHash>);

                my $logrec = [DateTime.now, $trxdetails<nonce>.UInt, $ethobj.hex2buf($trxdetails<input>).bytes, $trxdetails<gas>.UInt, $transaction<transactionHash>, 'set', 'signed'];

                if $trxdetails<nonce> == $nonce - 1 {
                    @trx.push($logrec);

                    # sprintf("%08d: %s", $trxdetails<nonce>, @trx.tail.gist).say;

                    return @trx;
                }
                else {
                    @trx.push($logrec);

                    # sprintf("%08d: %s", $trxdetails<nonce>, @trx.tail.gist).say;
                }
            }
        }

        if $block {
            return @trx;
        }
        else {
            $earliest_block = $earliest_block + blockshift;

            $index++;
        }
    }

    return List.new;
}

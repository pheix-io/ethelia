#!/bin/env raku

use HTTP::Request;
use HTTP::UserAgent;
use JSON::Fast;
use MIME::Base64;
use Net::Ethereum;
use Node::Ethereum::Keccak256::Native;
use XML;

use Pheix::Datepack;
use Pheix::Model::Database::Access;
use Pheix::Model::JSON;
use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;

constant iqairtimeout  = 6;
constant iqairiters    = 10;
constant searchlimit   = 25;
constant blockshift    = 650;
constant endpoint      = 'https://www.iqair.com:443/kyrgyzstan/bishkek';
constant aqicnendpoint = 'https://api.waqi.info/v2/map/bounds?latlng=42.682435,74.359589,43.032259,74.825821&networks=all';
constant containertag  = 'app\-location\-list';
constant useragent     = HTTP::UserAgent.new;
constant netethereum   = Net::Ethereum.new;
constant keccak256     = Node::Ethereum::Keccak256::Native.new;
constant base64        = MIME::Base64.new;
constant explorerurlf  = "https://sepolia.etherscan.io/tx/%s";

my $jsonobj;
my $remotetab;
my $datatab = 'events';

sub MAIN (
    Str  :$token!,
    Bool :$useiqair = False,
    Bool :$dry = False,
    Bool :$debug = False,
    Bool :$local = False,
    Str  :$path = './config.json',
    Bool :$fetchpos = False,
    Str  :$output = $*CWD.Str,
    Str  :$table = 'events',
    Str  :$pheixpath = sprintf("%s/git/pheix-pool/core-perl6", $*HOME),
    Str  :$storageconfig = sprintf("%s/git/pheix-pool/core-perl6/conf", $*HOME),
    Str  :$remotetableprefix = 'remote'
) {
    $*ERR.out-buffer = False;
    $*OUT.out-buffer = False;

    my $config  = $path.IO.f ?? from-json($path.IO.slurp) !! {};
    useragent.timeout = 10;

    $datatab = $table;
    $jsonobj = Pheix::Model::JSON.new(:path($storageconfig));

    my $temp_config = {
        module => {
            configuration => {
                settings => {
                    storage => {
                        group => {
                            $datatab => {
                                type => '0',
                                path => $output,
                            }
                        }
                    }
                }
            }
        }
    };

    my %tabsets = $jsonobj.get_all_settings_for_group_member('Pheix', 'storage', $table);

    if !%tabsets || !%tabsets.keys {
        $jsonobj = Pheix::Model::JSON.new.set_entire_config(:setup($temp_config));
    }
    else {
        sprintf("found table %s in Pheix settings (type=%d, hash=%s)", $table, %tabsets<type>, %tabsets<hash>).say;
    }

    die 'can not override JSON object' unless $jsonobj ~~ Pheix::Model::JSON;

    $remotetab = $local ?? sprintf("%s/%s", $remotetableprefix, $datatab) !! $datatab;
    my $dbobj  = Pheix::Model::Database::Access.new(
        :table($remotetab),
        :fields(List.new),
        :jsonobj($jsonobj)
    );

    die 'database object failure' unless $dbobj;

    if $local {
        die 'can not update local storage' unless _update_local_storage(:$dbobj, :$temp_config, :$pheixpath, :$dry);

        'successfully saved data from remote database to local'.say;

        return 0;
    }

    my @data;

    if $useiqair {
        my @blankitems;
        my @li = _get_iqair_data;

        sprintf("found %s stations at IQair", @li.elems).say;

        for @li -> $li {
            my ($a) = $li.elements(:TAG<a>);

            my $title       = $a[0].string;
            my $address     = netethereum.buf2hex(keccak256.keccak256(:msg($title))).lc.substr(0,42);
            my $sensorvalue = +($a[1][0].string);
            my $profilehref = ~($a.attribs<href>);

            next unless $sensorvalue && $sensorvalue > 0;

            my $position = _get_position_from_config(:$config, :addr($address));

            sprintf("position failure for %s", $title).say if $position<lat> == 0.0 && $position<lng> == 0.0;

            @data.push({
                title => $title,
                sensorvalue => $sensorvalue,
                profile => $profilehref,
                addr => $address,
                position => $position,
            });

            if $position<lat> == 0.0 && $position<lng> == 0.0 && $fetchpos {
                $position = _get_position(:href($profilehref));

                @data.tail<position> = $position;

                @blankitems.push(@data.tail);
            }
        }

        die 'no air quality data' unless @data && @data.elems;

        if @blankitems && @blankitems.elems {
            my $logfilename = sprintf("%s.blank-position-items.json",
                DateTime.now(formatter => { sprintf("%04d-%02d-%02d %02d:%02d:%02u.%04u", .year, .month, .day, .hour, .minute, .second.Int, (.second % (.second.Int || .second)) * 10_000) }));

            @blankitems = @blankitems.map({
                %(
                    title => $_<title>,
                    profile => $_<profile>,
                    addr => $_<addr>,
                    position => $_<position>,
                )
            });

            $logfilename.IO.spurt(to-json(@blankitems, :sorted-keys));

            if $path.IO.f && $config && $config.keys && ($config<geopoints>:exists) && $config<geopoints>.elems {
                $config<geopoints>.push(@blankitems.Slip);

                $path.IO.spurt(to-json($config, :sorted-keys, :pretty));

                sprintf("geopoints configuration file <%s> is updated: %d records are added", $path, @blankitems.elems).say;
            }
        }
    }
    else {
        @data = _get_aqicn_data(:$token).map({
            %(
                title => translit(:text($_<station><name>)),
                sensorvalue => $_<aqi>,
                profile => sprintf("https://aqicn.org/station/@%d/", $_<uid>.abs),
                addr => netethereum.buf2hex(keccak256.keccak256(:msg($_<station><name>))).lc.substr(0,42),
                position => %(
                    lat => $_<lat>,
                    lng => $_<lon>,
                ),
            )
        });

        sprintf("found %s stations at AQICN", @data.elems).say;
    }

    # to-json(@data).say;
    # sprintf('found %d active geopoints at IQair', @data.elems).say;

    @data = _salt_from_local(:@data, :$remotetableprefix, :$datatab, :localdbpath($output)) if !$local;

    my $offset = time;

    _tweak_chainobj(:$dbobj, :$pheixpath, :$debug);

    my @remotetable = $dbobj.get_all(:fast(True), :withcomp(True));

    sprintf("table %s at remote database has %d records", $dbobj.table, @remotetable.elems).say;

    for @data.kv -> $index, $data {
        my $id      = $data<id> // $offset + $index;
        (my $topic  = $data<topic> // $data<title>) ~~ s:g/<:!alpha>//;
        my $payload = {
            notification => sprintf("%s notifications", $useiqair ?? 'IQair' !! 'AQICN'),
            details => 'automated',
            enrichment => {
                position => $data<position>,
                title => $data<title>,
            },
            device => 'Ala-Archa Linux',
            application => 'crontab',
            sensorvalue => $data<sensorvalue>,
            sensordevice => $useiqair ?? 'IQair' !! 'AQICN'
        };

        my $record = {
            id => $id,
            address => $data<addr>,
            created => Pheix::Datepack.new(:date(DateTime.new(now)), :unixtime(time)).get_http_response_date,
            payload => to-json($payload),
            topic => $topic,
            code => $data<code> ?? $data<code> !! sprintf("%s-%s", $useiqair ?? 'iqair' !! 'aqicn', $id),
        };

        die 'can not store event' unless _store_event(:$dbobj, :$record, :$pheixpath, :$debug, :$dry, :@remotetable);
    }

    sprintf("successfully saved data from %d geopoints", @data.elems).say;

    if $dbobj.dbswitch == 1 && $dbobj.chainobj.sgnlog.elems {
        Pheix::Test::BlockchainComp::Helpers
            .new(:tstobj(Pheix::Test::Blockchain.new))
            .flush_signing_session(
                :sgnlog($dbobj.chainobj.sgnlog),
                :filepostfix(sprintf("-%s-update", $useiqair ?? 'iqair' !! 'aqicn'))
            );
    }
}

sub _prepare_request(Str :$endpoint!, Bool :$useiqair = False) returns HTTP::Request {
    my $request = HTTP::Request.new(GET => $endpoint);

    $request.header.field(
        User-Agent => 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/112.0',
        Accept => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
        Accept-Language => 'en-US,en;q=0.5',
        Accept-Encoding => 'gzip, deflate, br',
        Alt-Used => 'www.iqair.com',
        Content-Type => 'text/html',
        Connection   => 'keep-alive',
        Upgrade-Insecure-Requests => 1,
    ) if $useiqair;

    return $request;
}

sub _get_position(Str :$href!) returns Hash {
    my $ret = {
        lat => 0.0,
        lng => 0.0
    };

    my $endpoint    = sprintf("https://www.iqair.com:443%s", $href);
    my $detais_resp = useragent.request(_prepare_request(:$endpoint, :useiqair(True)));

    if $detais_resp.is-success {
        # /air-quality-map?lat=42.786622&lng=74.658695&placeId=84ce78ef3501d7925b7e

        if $detais_resp.content ~~ /'lat=42.'(<[\d]>+)'&amp;lng=74.'(<[\d]>+)/ {
            $ret<lat> = sprintf("42.%d", $0);
            $ret<lng> = sprintf("74.%d", $1);

            sprintf("found position for %s: %s", $href, $ret.gist).say;
        }
    }
    else {
        die $detais_resp.status-line;
    }

    return $ret;
}

sub _get_position_from_config(Hash :$config = {}, Str :$addr!) returns Hash {
    my $ret = {
        lat => 0.0,
        lng => 0.0
    };

    return $ret unless
        $config ~~ Hash &&
        $config.keys &&
        ($config<geopoints>:exists) &&
        $config<geopoints>.elems;

    for $config<geopoints>.values -> $geopoint {
        next unless ($geopoint<addr>:exists) && $geopoint<addr> ~~ m:i/^ 0x<xdigit>**40 $/;

        if $geopoint<addr> eq $addr {
            if ($geopoint<position>:exists) && $geopoint<position>.keys {
                return $geopoint<position>;
            }
        }
    }

    return $ret;
}

sub _store_event(
         :$dbobj!,
    Hash :$record!,
    Str  :$pheixpath,
    Bool :$debug = False,
    Bool :$dry = False,
    Bool :$force = False,
    Bool :$skiptrxhash = True,
         :@remotetable,
) returns Bool {
    my @fields   = <address code created payload topic>;
    my Bool $ret = False;

    return $ret unless $record.keys && ($record<address>:exists) && $jsonobj && $jsonobj ~~ Pheix::Model::JSON;

    my @rows = @remotetable.clone;
    my $existed_rec = {};

    for @rows.values -> $row {
        next unless $row ~~ Hash && $row<data>;

        my $data = $dbobj.dbswitch == 1 ?? $row<data> !!
            base64.decode-str($row<data>);
        next unless $data && $data ~~ /<[\|]>+/;

        my @records = $data.split(q{|}, :skip-empty);

        if $dbobj.dbswitch == 1 {
            $row<id>          = @records.shift;
            $row<compression> = @records.pop;
        }

        if @records[0] eq $record<address> {
            $existed_rec = {
                id          => $row<id>,
                row         => @records,
                compression => $row<compression>,
            }

            last;
        }
    }

    if $existed_rec.keys && ($existed_rec<id>:exists) {
        if ($existed_rec<row>[3]) {
            my $payload        = from-json(base64.decode-str($existed_rec<row>[3]));
            my $update_payload = from-json($record<payload>);

            $update_payload<enrichment><transactionHash>:delete if ($update_payload<enrichment><transactionHash>:exists) && $skiptrxhash;

            # $update_payload<sensorvalue> = $update_payload<sensorvalue> + 1;

            if (!$force && ($payload<sensorvalue>:exists) && $payload<sensorvalue>.Int == $update_payload<sensorvalue>.Int) {
                sprintf("skip update for %d (%s): sensorvalue <%d> is not changed", $existed_rec<id>, $existed_rec<row>.tail, $payload<sensorvalue>.Int).say;

                return True;
            }
            else {
                $existed_rec<row>[1] = $record<code>;
                $existed_rec<row>[2] = $record<created>;
                $existed_rec<row>[3] = base64.encode-str(to-json($update_payload), :oneline);

                # die sprintf("update for record %d is not implemented", $existed_rec<id>);
                if $dry {
                    sprintf('skip set in dry run mode').say;

                    $ret = True;
                }
                else {
                    my $set_clause = { id => $existed_rec<id>.UInt };
                    my $set_data   = {
                        data        => $dbobj.dbswitch == 1 ?? $existed_rec<row>.join(q{|}) !! base64.encode-str($existed_rec<row>.join(q{|}), :oneline),
                        $dbobj.dbswitch == 1 ?? %(
                            id          => $existed_rec<id>.UInt,
                            compression => $existed_rec<compression>.UInt,
                        ) !! %()
                    };

                    $ret = $dbobj.set($set_data, $set_clause, :waittx(False),).Bool;
                }
            }
        }
    }
    else {
        $record<payload> = base64.encode-str($record<payload>, :oneline);

        if $dbobj.dbswitch == 1 && !$dbobj.exists {
            my $rc_unlock = $dbobj.chainobj.unlock_account;

            die sprintf("unlock account %s on blockchain failure", $dbobj.chainobj.ethacc) unless $rc_unlock;

            my $rc_create = $dbobj.chainobj.table_create(:waittx(True));

            die sprintf("create table %s on blockchain failure: %s", $dbobj.chainobj.table, $rc_create.gist) unless $rc_create && $rc_create<status>;
        }

        my $insert_data = {
            id          => $record<id>,
            data        => $dbobj.dbswitch == 1 ?? @fields.map({$record{$_}}).join(q{|}) !! base64.encode-str(@fields.map({$record{$_}}).join(q{|}), :oneline),
            compression => q{1},
            waittx      => False,
        };

        if $dry {
            sprintf('skip insert in dry run mode').say;

            $ret = True;
        }
        else {
            $ret = $dbobj.insert($insert_data);
        }
    }

    return $ret;
}

sub _update_local_storage(:$dbobj!, Hash :$temp_config!, Str :$pheixpath!, Bool :$debug = False, Bool :$dry = False) returns Bool {
    die 'database on blockchain is unavailable' unless $dbobj ~~ Pheix::Model::Database::Access && $dbobj.dbswitch == 1;
    die 'tweak database on blockchain object failure' unless _tweak_chainobj(:$dbobj, :$pheixpath, :$debug);

    my $lastblockchainmod = $dbobj.chainobj.get_modify_time(:t($dbobj.table));
    my $lastmoddeltamins  = ((now - $lastblockchainmod)/60).UInt;

    sprintf("blockchain table was modified on %s: %d mins ago", DateTime.new($lastblockchainmod, formatter => {sprintf("%04d/%02d/%02d %s", .year, .month, .day, .hh-mm-ss)}), $lastmoddeltamins).say;

    if $lastmoddeltamins > 150 {
        'database on blockchain looks ancient, skipping sync'.say;

        return True;
    }

    my @database;
    my @rows = $dbobj.get_all(:fast(True), :withcomp(True));

    for @rows.values -> $row {
        next unless $row ~~ Hash && $row<data>;

        my @records = $row<data>.split(q{|}, :skip-empty);

        next unless @records && @records.elems == 7;

        my $payload;

        try {
            $payload = from-json(base64.decode-str(@records[4]));

            CATCH {
                default {
                    my $e = .massage;

                    sprintf("JSON payload parsing failure %s", $e);
                }
            }
        }

        if $payload && $payload.keys {
            if (my $trxhash = _get_transaction_from_logs(:$dbobj, :rowid(@records[0].UInt))) {
                $payload<enrichment><transactionHash> = $trxhash;

                @records[4] = base64.encode-str(to-json($payload), :oneline);
            }
        }

        @database.push({
            id      => @records[0],
            address => @records[1],
            code    => @records[2],
            created => @records[3],
            payload => base64.decode-str(@records[4]),
            topic   => @records[5]
        })
    }

    die 'database on blockchain is empty' unless @database && @database.elems;

    my $localdbobj = Pheix::Model::Database::Access.new(
        :table($datatab),
        :fields(List.new),
        :jsonobj(Pheix::Model::JSON.new.set_entire_config(:setup($temp_config)))
    );

    for @database.values -> $record {
        die "can not store event locally" unless _store_event(:dbobj($localdbobj), :$record, :$debug, :$dry, :force(True), :skiptrxhash(False));
    }

    return True;
}

sub _tweak_chainobj(:$dbobj!, Str :$pheixpath!, Bool :$debug = False, ) returns Bool {
    if $pheixpath && $pheixpath.IO.d {
        if $dbobj.dbswitch == 1 {
            my %tabsets = $jsonobj.get_all_settings_for_group_member('Pheix', 'storage', $remotetab);

            if (
                (%tabsets<path>:exists) &&
                (%tabsets<strg>:exists) &&
                (%tabsets<extn>:exists)
            ) {
                my Str $fpath = sprintf("%s/%s/%s.%s",
                    $pheixpath,
                    %tabsets<path>,
                    %tabsets<strg>,
                    %tabsets<extn>
                );

                if ($fpath.IO.e) {
                    $dbobj.chainobj.comp                 = True;
                    $dbobj.chainobj.debug                = $debug;
                    $dbobj.chainobj.ethobj.abi           = $fpath.IO.slurp;
                    $dbobj.chainobj.ethobj.tx_wait_iters = 200;
                    $dbobj.chainobj.ethobj.tx_wait_sec   = 3;
                }
            }
        }
    }

    return True;
}

sub _get_transaction_from_logs(:$dbobj!, UInt :$rowid!) returns Str {
    die 'can not get ethereum logs from non blockchain database object' unless $dbobj.dbswitch == 1;
    die sprintf("invalid rowid=%d is given", $rowid) unless $rowid > 0;

    my $ethobj    = $dbobj.chainobj.ethobj;
    my $evsignhex = $ethobj.string2hex($dbobj.chainobj.evsign);
    my $event_sig = Node::Ethereum::Keccak256::Native.new.keccak256(:msg($ethobj.hex2buf($evsignhex)));

    # sprintf("Smart contract %s v%s", $dbobj.chainobj.scaddr, $dbobj.chainobj.get_smartcontract_ver).say;

    my $tabbuf        = $ethobj.hex2buf($ethobj.string2hex($dbobj.table));
    my buf8 $tablepad = buf8.new(0 xx (32 - $tabbuf.bytes)).unshift($tabbuf);

    my $latest_block = $ethobj.eth_blockNumber;

    my $topics = [
        $ethobj.buf2hex($event_sig).lc,
        $ethobj.buf2hex($tablepad).lc,
        [
            sprintf('0x%064x', 4),
            sprintf('0x%064x', 5)
        ],
        sprintf('0x%064x', $rowid)
    ];

    my $ret = [];
    my $inx = 0;

    while ($inx < searchlimit) {
        # sprintf("search topics on range [%d..%d]", $latest_block - blockshift, $latest_block).say;

        my %filter =
            fromBlock => sprintf('0x%x', $latest_block - blockshift),
            toBlock   => sprintf('0x%x', $latest_block),
            address   => $dbobj.chainobj.scaddr,
            topics    => $topics;

        $ret = $ethobj.eth_getLogs(%filter);

        last if $ret.elems;

        $latest_block = $latest_block - blockshift;

        $inx++;
    }

    return Str unless
        $ret.elems && ($ret.tail<transactionHash>:exists) && $ret.tail<transactionHash> ~~ m:i/^ 0x<xdigit>**64 $/;

#   $ret.map({ sprintf("https://sepolia.etherscan.io/tx/%s", $_<transactionHash>).say });

    sprintf("found transaction %s for rowid=%d at block %d", $ret.tail<transactionHash>, $rowid, $ret.tail<blockNumber>).say;

    return $ret.tail<transactionHash>;
}

sub _salt_from_local(:@data!, Str :$datatab!, Str :$remotetableprefix, Str :$localdbpath!) returns List {
    # die sprintf("invalid tableprefix=%s for table %s", $remotetableprefix, $datatab) unless $datatab ~~ m:i/^<{$remotetableprefix}>'/'/;

    (my Str $localtab = $datatab) ~~ s:i/<{$remotetableprefix}>'/'//;

    my $temp_config = {
        module => {
            configuration => {
                settings => {
                    storage => {
                        group => {
                            $localtab => {
                                type => '0',
                                path => $localdbpath,
                            }
                        }
                    }
                }
            }
        }
    };

    my $localdbobj = Pheix::Model::Database::Access.new(
        :table($localtab),
        :fields(List.new),
        :jsonobj(Pheix::Model::JSON.new.set_entire_config(:setup($temp_config)))
    );

    if !$localdbobj.exists {
        sprintf("local table %s is not existed", $localtab).say;

        return @data;
    }

    my @salteddata = @data.clone;
    my @rows       = $localdbobj.get_all(:fast(True), :withcomp(True));

    for @rows.values -> $row {
        next unless $row ~~ Hash && $row<data>;

        my $rowdata = base64.decode-str($row<data>);

        next unless $rowdata && $rowdata ~~ /<[\|]>+/;

        my @records = $rowdata.split(q{|}, :skip-empty);

        next unless @records.elems == 5;

        if @salteddata.grep({$_<addr> eq @records.head}).elems == 0 {
            my $payload;

            try {
                $payload = from-json(base64.decode-str(@records[3]));

                CATCH {
                    default {
                        my $e = .message;

                        sprintf("parse payload failure while salting from id=%d: %s", $row<id>, $e).say;
                    }
                }
            }

            next unless $payload && $payload.keys;

            @salteddata.push({
                title => $payload<enrichment><title>,
                sensorvalue => $payload<sensorvalue>,
                code => @records[1],
                addr => @records.head,
                position => $payload<enrichment><position>,
                id => $row<id>,
                topic => @records[4]
            });

            sprintf("salted %s data with id=%d (sensorvalue=%d) from local table %s", $payload<enrichment><title>, $row<id>, $payload<sensorvalue>, $localdbobj.table).say;
        }
    }

    return @salteddata;
}

sub _get_iqair_data returns List {
    my $ul;

    for ^iqairiters -> $iter {
        sprintf("try to fetch IQair data on iteration %d", $iter).say;

        my $res = useragent.request(_prepare_request(:endpoint(endpoint), :useiqair(True)));

        if $res.is-success {
            my @lines = $res.content.split(/<[\r\n]>+/, :skip-empty);

            die 'can not parse response' unless @lines.elems;

            my $range = False;
            my @stations;

            for @lines -> $line {
                $range = True if $line ~~ /'<'<{containertag}>/;

                next unless $range;

                @stations.push($line);

                last if $line ~~ /'</'<{containertag}>'>'/;
            }

            my $html = @stations.join;

            (my $tag = containertag) ~~ s:g/\\//;

            my $s = $html.rindex(sprintf("<%s", $tag));
            my $e = $html.rindex(sprintf("</%s>", $tag)) + (sprintf("</%s>", $tag).chars - 1);

            next unless $s && $e && $e > $s;

            my $xml = from-xml(substr($html, $s..$e));

            next unless $xml && $xml[0];

            $ul = $xml[0].elements(:TAG<ul>).head;

            last if $ul;
        }
        else {
            die $res.status-line;
        }

        sleep(iqairtimeout);
    }

    die 'empty station list from IQair' unless $ul;

    my @li = $ul.elements(:TAG<li>);

    die 'no stations station in list from IQair' unless @li && @li.elems;

    return @li;
}

sub _get_aqicn_data(Str :$token!) returns List {
    my $endpoint = sprintf("%s&token=%s", aqicnendpoint, $token);

    for ^iqairiters -> $iter {
        sprintf("try to fetch AQICN data from %s on iteration %d", $endpoint, $iter).say;


        my $res = useragent.request(_prepare_request(:$endpoint), :useiqair(False));

        if $res.is-success {
            my $response;

            try {
                $response = from-json($res.content);

                CATCH {
                    default {
                        my $e = .message;

                        sprintf("***ERR: faulure while parsing data from AQICN: %s", $e).say;
                    }
                }
            }

            next unless $response<status> && $response<status> ~~ /ok/;

            if $response<data> && $response<data>.elems {
                return $response<data>.grep({ $_<aqi> ne '-' }).List;
            }
        }
        else {
            die $res.status-line;
        }

        sleep(iqairtimeout);
    }

    return List.new;
}

sub translit(Str :$text!) returns Str {
    my $transliterated = q{};
	my $converter = {
        'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
		'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
		'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
		'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
		'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
		'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
		'э' => 'e',    'ю' => 'yu',   'я' => 'ya',

		'А' => 'A',    'Б' => 'B',    'В' => 'V',    'Г' => 'G',    'Д' => 'D',
		'Е' => 'E',    'Ё' => 'E',    'Ж' => 'Zh',   'З' => 'Z',    'И' => 'I',
		'Й' => 'Y',    'К' => 'K',    'Л' => 'L',    'М' => 'M',    'Н' => 'N',
		'О' => 'O',    'П' => 'P',    'Р' => 'R',    'С' => 'S',    'Т' => 'T',
		'У' => 'U',    'Ф' => 'F',    'Х' => 'H',    'Ц' => 'C',    'Ч' => 'Ch',
		'Ш' => 'Sh',   'Щ' => 'Sch',  'Ь' => '',     'Ы' => 'Y',    'Ъ' => '',
		'Э' => 'E',    'Ю' => 'Yu',   'Я' => 'Ya'
	};

    my @chararray = $text.split(q{}, :skip-empty);

    for @chararray.values -> $char {
        next unless $char && $char.ord;

        if ($converter{$char}) {
            $transliterated ~= $converter{$char};
        }
        else {
            $transliterated ~= $char;
        }
    }

    return $transliterated;
}

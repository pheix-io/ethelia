# Open points

> reviewed: 27 February 2025

## Current pool of tasks

1. ~~Official **Pheix/Ethelia** project registration at ФИПС:~~ [@pheix-io/ethelia#25](https://gitlab.com/pheix-io/ethelia/-/issues/25):
    * script to calculate sourcebase in Kbytes: [sources-to-singlefile.v2.raku](https://gitlab.com/pheix/dcms-raku/-/blob/authenticate-via-metamask/.research/utils/sources-to-singlefile.v2.raku);
    * collect all sources to single file: [@pheix-io/ethelia#25→1448397008](https://gitlab.com/pheix-io/ethelia/-/issues/25#note_1448397008), [PDF](https://gitlab.com/-/project/38820598/uploads/d42649d938aaf8e3d1ad713277356f64/%D0%94%D0%B5%D0%BF%D0%BE%D0%BD%D0%B8%D1%80%D1%83%D0%B5%D0%BC%D1%8B%D0%B5_%D0%BC%D0%B0%D1%82%D0%B5%D1%80%D0%B8%D0%B0%D0%BB%D1%8B_%D0%BB%D0%B8%D1%81%D1%82%D0%B8%D0%BD%D0%B3.v2.pdf);
    * Application is sent on 5 November 2024: [@pheix-io/ethelia#25→2194578265](https://gitlab.com/pheix-io/ethelia/-/issues/25#note_2194578265);
    * **Project is registered on 18 November 2024**: [@pheix-io/ethelia#25→2218888950](https://gitlab.com/pheix-io/ethelia/-/issues/25#note_2218888950).
2. ~~Authenticate user via MetaMask in **Pheix** project:~~ [#186](https://gitlab.com/pheix/dcms-raku/-/issues/186):
    * fix bugs and UI inconsistency for current implementation:
        - unset loading flag on exception [@pheix/dcms-resources#6a04b37d](https://gitlab.com/pheix/dcms-resources/-/commit/6a04b37d271c01ea7ef90141ed4c58842f3722b3);
        - tweak UI on MetaMask exceptions/errors [@pheix/dcms-resources#8a3e9413](https://gitlab.com/pheix/dcms-resources/-/commit/8a3e94132405f4ef03ed955fc8bce819ea19c5c6).
    * public key interchange for opened session:
        - research draft: [@pheix/dcms-raku#186→2185159720](https://gitlab.com/pheix/dcms-raku/-/issues/186#note_2185159720);
        - implementation:
            * **Pheix** core project [5ec52882](https://gitlab.com/pheix/dcms-raku/-/commit/5ec528829aadfca3f6c1f4ea87a2c3884e26b733);
            * **Pheix** resources  [d2eadb5f](https://gitlab.com/pheix/dcms-resources/-/commit/d2eadb5f8dfc46ee1ace53da11598d4264f4784e);
            * **Pheix** configuration [8a686b2f](https://gitlab.com/pheix/dcms-configuration/-/commit/8a686b2fadab044682087cafc6f05e499e98e54d).
    * CI/CD pipelines: [@pheix/dcms-raku#186→2216696042](https://gitlab.com/pheix/dcms-raku/-/issues/186#note_2216696042).
3. MetaMask feature post-merge:
    * write an announcement post on feature merged to:
        - ~~[Feature video intro](https://gitlab.com/pheix/dcms-raku/-/issues/186#note_2216437495), [short](https://www.youtube.com/shorts/z4fEEPNwUwk) on youtube~~;
        - [narkhov.pro](https://narkhov.pro);
        - [pheix.org](https://narkhov.pro);
        - ~~Telegram~~ and Matrix.
    * sanitize auth algorithms:
        - Extended balance-dependent auth [@pheix-research/talks#19](https://gitlab.com/pheix-research/talks/-/issues/19);
        - <span style="background:rgb(255,255,0,.2)">&nbsp;Extended secret-dependent auth&nbsp;</span>
4. **Pheix** project updates/improvements/enhancements:
    * Migrate from flatfiles (filechains) to PostreSQL in **Pheix** project: [#121](https://gitlab.com/pheix/dcms-raku/-/issues/121);
        - ~~Implement `create`, `exists`, `drop`, `insert` and `get_all` methods~~ [@pheix/dcms-raku#121→2373221659](https://gitlab.com/pheix/dcms-raku/-/issues/121#note_2373221659);
        - Outstanding methods: `row_set`, `row_get` and `row_remove`;
    * ~~Support [EIP-4844](https://www.eip4844.com/) transactions in **Pheix** project [#198](https://gitlab.com/pheix/dcms-raku/-/issues/198)~~ → [!24](https://gitlab.com/pheix/dcms-raku/-/merge_requests/24):
        - Implement [C-KZG-4844](https://github.com/ethereum/c-kzg-4844) Raku binding [@pheix/raku-node-ethereum-kzg](https://gitlab.com/pheix/raku-node-ethereum-kzg);
        - Add support to **Pheix** project [28f2dff1](https://gitlab.com/pheix/dcms-raku/-/commit/28f2dff1d6413c965e2f72742a2f8093b794ea17);
        - Add `receive()` or/and `fallback()` functions to `PheixDatabase` [@pheix-research/smart-contracts#48](https://gitlab.com/pheix-research/smart-contracts/-/issues/48);
        - Run **PM25/Ethelia** project on Ethereum blobs;
        - Cover `set_blob_data` and `get_blob_data` methods by unit tests: [@pheix/dcms-raku#198→2339213933](https://gitlab.com/pheix/dcms-raku/-/issues/198#note_2339213933).
    * ~~Log all requests and responses in **Pheix** project:~~ [#197](https://gitlab.com/pheix/dcms-raku/-/issues/197);
    * ~~Database on blockchain [optimization/improvement](#pheix-database-on-blockchain-optimizationimprovement)~~;
    * ~~Push to `fez` ecosystem~~: [https://raku.land/zef:knarkhov/Pheix](https://raku.land/zef:knarkhov/Pheix);
    * Add `/metrics` route for Prometheus, consider [RED](https://grafana.com/files/grafanacon_eu_2018/Tom_Wilkie_GrafanaCon_EU_2018.pdf), [USE](https://www.brendangregg.com/usemethod.html) and LETS [metrics](https://grafana.com/blog/2018/08/02/the-red-method-how-to-instrument-your-services/); check [Prometheus client](https://github.com/zostay/raku-Prometheus-Client/tree/master) in Raku:
        - Basic runtime metrics via `Telemetry` [module](https://docs.raku.org/type/Telemetry);
        - `$*KERNEL` [class](https://docs.raku.org/type/Kernel);
        - FastCGI environment;
        - Basic [CMS metrics](https://docs.magnolia-cms.com/instrumentation/instrumentation-metric-guide/);
        - Data from `Pheix::Controller::Stats` per week: `hosts` and `visits`;
        - Data from log: entries, logons, etc...
    * Add a new slide to index page slider with the details about Matrix workspace and **PM25/Ethelia** project Telegram channel; good case — A. Lebedev's «Контент везде» slide with links to the accounts in the most popular social networks and QR to the landing page with all available links as well. Search *Add to Ethelia Open Points* in Google Calendar for more details;
    * Add valid favicon;
    * Reload captcha buttom/icon — it should sync session token on host page and reload actual captcha challenge;
    * Skip mandatory local signer node check if the valid `keystore` file is setup up;
    * ~~Report details on addon crashed at initialization stage:~~ [@pheix/dcms-raku#200](https://gitlab.com/pheix/dcms-raku/-/issues/200).
5. ~~New `LIVE` self-located servers for **PM25/Ethelia** project~~ [@pheix-io/ethelia#42](https://gitlab.com/pheix-io/ethelia/-/issues/42):
    * ~~launch self-located GitLab runner~~ — pipeline/jobs samples:
        - `Net::Ethereum`: [https://gitlab.com/pheix/raku-net-ethereum/-/jobs/8766648136](https://gitlab.com/pheix/raku-net-ethereum/-/jobs/8766648136)
        - `Ethelia`: [https://gitlab.com/pheix/raku-ethelia/-/jobs/8751915797](https://gitlab.com/pheix/raku-ethelia/-/jobs/8751915797)
        - Smart contracts: [https://gitlab.com/pheix-research/smart-contracts/-/jobs/8754315201](https://gitlab.com/pheix-research/smart-contracts/-/jobs/8754315201)
    * ~~launch self-located new **PM25/Ethelia** project server~~ [@pheix-io/ethelia#42→2295066124](https://gitlab.com/pheix-io/ethelia/-/issues/42#note_2295066124);
    * ~~perform hot-reserve test on self-location, quick test aproach~~ [@pheix-io/ethelia#44](https://gitlab.com/pheix-io/ethelia/-/issues/44):
        - set up and run OpenVPN on self-located server;
        - sync with stations via local node;
        - change local storage `local/holesky/ExtensionEthelia` configuration at `$HOME/nvme/home/apache_root/pheix.org/www/conf/addons/ExtensionEthelia/config.json` to hot-reserve node on `192.168.10.30`;
        - sync with stations via hot-reserve node.
6. ERC20-token funding/charging for **PM25/Ethelia** project, details: [@pheix-io/ethelia#37](https://gitlab.com/pheix-io/ethelia/-/issues/37);
7. Make different maps/locations available in **PM25/Ethelia** project;
8. Upgrade Ethereum Local Network:
    * [Cancun](https://github.com/ethereum/execution-specs/blob/master/network-upgrades/mainnet-upgrades/cancun.md) infrastructure: separate validator(s) and 2x Geth execution clients with built-in beacon clients;
    * Run `clef` instance for one node (1st or 2nd, no matter);
    * Upgrade/refactor tests for `smart-contacts` project according the new network upgrades: [@smart-contracts#33](https://gitlab.com/pheix-research/smart-contracts/-/issues/33);
    * Introduce `clef` authentication module in **Pheix** project: [#174](https://gitlab.com/pheix/dcms-raku/-/issues/174);
    * Remove personal namespace from start up script: [@pheix-research/ethereum-local-network#9](https://gitlab.com/pheix-research/ethereum-local-network/-/issues/9)
9. Infrastructure monitoring and log collecting system via [Prometheus](https://prometheus.io/docs/introduction/overview/)→[PushGateway](https://prometheus.io/docs/introduction/overview/)→[AlertManager](https://prometheus.io/docs/introduction/overview/): [@raku-dossier-prototype#9](https://gitlab.com/pheix-research/raku-dossier-prototype/-/issues/9):
    * use [AlertManager Matrix bot](https://github.com/jaywink/matrix-alertmanager) as a webhook receiver;
    * use **Docker Compose** for deployment;
10. **PM25/Ethelia** project and `Dossier` telegram bot:
    * Extract PM2.5 sensor values from photos (OCR): [@raku-dossier-prototype#6](https://gitlab.com/pheix-research/raku-dossier-prototype/-/issues/6);
    * Smart telegram bot monitoring and possible restart: [@pheix-research/raku-dossier-prototype#9](https://gitlab.com/pheix-research/raku-dossier-prototype/-/issues/9);
    * Embed micro web-server with `/metrics` route to deliver generic bot metrics to Prometheus;
    * Asynchronous dumper for keystores, dossiers, logs, configs and other important data stored inside the container: safe remove/prune/delete feature:
        - Add async dump feature to Telegram bot — create `tgz` dump file on bot start, save `tgz` dump file to shared folder;
        - Add `crontab` job to copy `tgz` dump file from share folder to archive location;
    * Add `/faucet 0xdf...3c` feature to bot: a simple way to get some **AlaAuETH** to get started with authentication feature;
    * ~~Maps (single and animated) in dark theme;~~
    * Allow to store PM2.5 sensor data for different locations: [@raku-dossier-prototype#12](https://gitlab.com/pheix-research/raku-dossier-prototype/-/issues/12).
11. **PM25/Ethelia** project launch: [@pheix-io/ethelia#40](https://gitlab.com/pheix-io/ethelia/-/issues/40);
    * ~~Sync only updated local records in a threshold period:~~ [@raku-ethelia#12](https://gitlab.com/pheix/raku-ethelia/-/issues/12);
    * ~~Sync local and remote databases via different endpoints: consider sync every hour, so let's sync via Alchemy on even hours and local Geth node on odd hours~~ [@raku-ethelia→6a8c2dff](https://gitlab.com/pheix/raku-ethelia/-/commit/6a8c2dffe4bc2af823ec39c57b3db14a26db5648);
    * ~~Describe and test the approaches to make a blockchain sync reliable and endpoint independent. Currently we can actually switch between *Alchemy* and *Local* nodes, in theory (it should be tested) we can open VPN tunnel to `AC78` and use it temporarily (also we can open VPN tunnel right at the sync start from the helper script) — anyway finally we have to get some kind of documentation with all these details~~ [@pheix-io/ethelia#44](https://gitlab.com/pheix-io/ethelia/-/issues/44);
    * `trx-advancer`: review latest implementation and add (???) functionality to get transactions from a *mempool*, in general we have to introduce the tools for quick identification and fixing the next situation:
        - current nonce is `X`;
        - we commit signed transaction with nince `X+100`: transaction stuck and will never be mined;
        - `trx-advancer` should be able to find that transaction and somehow restore the nonce sequence: get stuck transaction's input and create a new transaction with `X+1` nonce;
    * Update **Ethereum authenticator** and **Telegram bot** containers to use shared folders for external data: we have to be able to delete and create containers without data loss;
    * Sync different tables/locations: [@raku-ethelia#13](https://gitlab.com/pheix/raku-ethelia/-/issues/13).
12. Migrate to `HTTP::Tiny` as a generic HTTP transport at:
    * `Net::Ethereum`: [@raku-net-ethereum#32](https://gitlab.com/pheix/raku-net-ethereum/-/issues/32)
    * `WRSLL`: [@what-retirement-should-look-like#3](https://gitlab.com/pheix-research/what-retirement-should-look-like/-/issues/3)
    * `Dossier::Transport::Request`: [b049bfd7](https://gitlab.com/pheix-research/raku-dossier-prototype/-/commit/b049bfd7c5e7bd57fd6733a625a27a1f8db708be)
13. Official [Ethelia Landing Page](https://ethelia.pheix.org/) improvements:
    * Add authentication form and all related features;
    * Update content (slide with details):
        - [Cube](https://gitlab.com/pheix-io/ethelia/-/issues/37#note_1998190304) and [Sphere](https://gitlab.com/pheix-io/ethelia/-/issues/37#note_1892814145) smart sensors;
        - [pm25.online](https://pm25.online/) service;
        - ~~API documentation link to main navigation block~~;
        - Getting started page: ~~add link to main navigation block~~ and translate to English.
14. Update Cube sensor new prototype:
    * new STL model (CubeV2) with:
        - ready to plug-in PM2.5 sensor: [@pheix-io/ethelia#43](https://gitlab.com/pheix-io/ethelia/-/issues/43);
        - no holes;
        - non-removable front panel (reference: the plastic prototype with putty);
        - sharp edges: [cantellation](https://en.wikipedia.org/wiki/Cantellation_(geometry)).
    * Print CubeV2 in plastic;
    * CubeV2 in aluminum.
15. ~~Set up and run VPN on `ala-archa`~~: [screenshot](/assets/images/openvpn-monitor/sample-screenshot.png);
16. Perform primary to secondary server switch on outages or issues:
    * **Pheix** project headless and tailless configurations: [3e46ac4f0](https://gitlab.com/pheix/dcms-raku/-/commit/3e46ac4f0f7dfa52da85d70bf0b669dd1d047c04);
    * `Nginx` on **Trappist-1** server with a [load-balancing configuration](nginx.org/en/docs/http/load_balancing.html) both to **Ala-Archa** and **Southern-Deadend-5**.

*<span style="background:rgb(255,255,0,.2)">&nbsp;Work in progress: new items to be defined and announced.&nbsp;</span>*

## Pinky paper

1. Ethelia technical concept: Telegram, Geth, Pheix and Ethelia as an add-on [module](https://gitlab.com/pheix/raku-ethelia);
2. Technical part from marketing perspective: finally we have to do full rewrite of tech part in human/investor readable way, collect details from:
    * [Startup World Cup Ulan-Bataar 2024](https://gitlab.com/pheix-io/ethelia/-/issues/39);
    * Ethelia investment presentation: [PDF](https://gitlab.com/pheix-io/ethelia/-/blob/main/docs/assets/pdf/ethelia-startup-overview_09_2023_24.pdf), [Video](https://www.youtube.com/watch?v=gRFSxqZitEI);
    * [Letter of motivation](/pp/motivation/) ([#32](https://gitlab.com/pheix-io/ethelia/-/issues/32));
    * [Glovo KG Startup Competition](https://gitlab.com/pheix-io/ethelia/-/issues/33);
    * [Running Ethereum node grant](https://gitlab.com/pheix-io/ethelia/-/issues/28);
    * [Sui x KuCoin Labs Summer Hackathon](https://gitlab.com/pheix-io/ethelia/-/issues/27);
    * [IT-Park UZ registration at acceleration program](https://gitlab.com/pheix-io/ethelia/-/issues/27);
    * [Ethelia traction metrics Q1 2023](https://gitlab.com/pheix-io/ethelia/-/issues/14);
    * [Late startup application form at YC](https://gitlab.com/pheix-io/ethelia/-/issues/9);
    * [UNICEF Call for Frontier Technology Startups](https://gitlab.com/pheix-io/ethelia/-/issues/11);
    * [Techstars startup strategy](https://gitlab.com/pheix-io/ethelia/-/issues/12).
3. Monetization and product payback:
    * [PM2.5 project](https://pm25.online/) funding model: [#37](https://gitlab.com/pheix-io/ethelia/-/issues/37);
    * since [fixed limit](/rawhide/ethelia-features/) for event search is introduced, different paid subscriptions could increase/decrease this setting;
    * event [subscription](https://gitlab.com/pheix-io/service/-/blob/requirements/docs/subscription.md) — **«events-on-air»** feature on remote endpoint or in administrative panel, also consider filters and proxy/relay to external services like Telegram, Matrix, Slack, Discord or traditional email as a future improvements;
    * plan **«pay per device»** — we will define the limit for free-of-charge event sources, any additional source should be paid;
    * plans **«pay per request»** or **«pay per auth»** — request cost is 20x lower for example, but auth is much more flexible for enterprise clients;
    * generally Ethelia is a gateway to public, private (scale the higher number of nodes), virtual (cloud hosted) and local (single CPU) networks — there could be any pricing plans, like **«pay per node»** — increase number of nodes and make it much more decentralized, **«pay per CPU»** — scale your local network, **«pay per resource»** — manage your cloud;
    * plan **«pay per transaction»** — [/pheix-io/unilode/-/issues/1#subscription-details](https://gitlab.com/pheix-io/unilode/-/issues/1#subscription-details);
    * since the registration model via [Telegram bot](/rawhide/registration-model/) is used, we can introduce different paid plans in [TON](https://ton.org/) coins, the most reasonable one **«pay per function»** — customer pays for advanced bot functions (mostly taken from administrative panel);
    * admin panel integration to 3rd-party websites requires cross domain cookies to be allowed, so [SameSite](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie#samesitesamesite-value) should be set to `None` instead of `Strict` — it's configurable and customer might be charged to allow that feature;
    * investment models for **PM25/Ethelia** project based on ERC20 token listings and long-term historical data usage/sharing perspectives: [PDF](https://gitlab.com/pheix-io/ethelia/-/blob/main/docs/assets/presentation-ulanbaator/assets/pdf/StartUp_World_Cup2024.20240705.pdf?ref_type=heads);
4. Future improvements, prospects and road map (3 years vision).

---

## Extended technical details

### Pheix database on blockchain optimization/improvement

It was moved to [Deprecated](/rawhide/deprecated/) section: [/rawhide/deprecated/op/event-driven-database](/rawhide/deprecated/op/event-driven-database).

This optimization/improvement has no sense in general cause of use of EIP-4844 blob transactions. The `PheixDatabase` smart contract was improved at [@pheix-research/smart-contracts#48](https://gitlab.com/pheix-research/smart-contracts/-/issues/48).

---

## Authorization on event source

> Now we have to pass auth on Ethereum node and get the access token. This token might be changed in some time interval, so:
>
> * deliver new token somehow to event source;
> * just add authorization step to event source software/firmware.

> Both solutions are very expensive and we need something lightweight and reliable. I suggest to consider [subscription](https://gitlab.com/pheix-io/service/-/blob/requirements/docs/subscription.md) feature available in Geth.

**Static tokens were introduced at [2a720ea3](https://gitlab.com/pheix/raku-ethelia/-/commit/2a720ea3eb5683240cd2930ae1e893933489bd4f) and this is the possible compromise for now.**

# Pitch

## Brief description of the project

**Ethelia** is a secure, authoritative and reliable Ethereum blockchain storage provider for various kinds of lightweight data.

Сonsider a network with functionally different nodes: IoT, programmable logic controllers, smart home systems, micro-services, standalone multi-threading or mobile applications. Every node runs own duty cycle with state changes and events.
In general we may skip non-critical state changes or usual events, but once the node detects some anomaly or exceptional behavior, importance of logging increases exponentially. Those logs are stored on blockchain by **Ethelia** and the fact of storing guarantees the data integrity and consistency.

Data stored by **Ethelia** is immutable, decentralized and independent from cloud providers and corporate regulations: none can restrict access or cancel any permissions due to the tense geopolitical situation, legal regulations or sanctions. Once the data is stored – it's ready for validation, audit or historical analysis. If the data is personal or sensitive, **Ethelia** will mirror just a hash references on the blockchain instead of actual payloads in a one click on setting toggle.

### Markets

**Ethelia** is between two global markets: Web3.0 and IoT. Our target customers are owners of access controls, intelligent sensors, CCTV, guard alarms, digital trackers, medicine or safety devices. Actually any smart systems, where critical events, traces, notifications or tags are emitting and had to be logged.

The ecology monitoring is the main innovative application area for now. We focus on factory supervision (air pollution profiling, electricity consumption monitoring) and smart city management – control alternative energy sources, systems to balance traffic, street lighting, central heating and air conditioning.

### Competitive advantages

Our direct competitors have proprietary solutions. None gives high level online access to storage via API or web panels. There is no ability to audit their blockchains via 3rd-party block explorers, no tools to verify storage consistency, physical status and overall access reliability. **Ethelia** has all these features out of the box.

### Prototype

Current [minimal viable product](https://pm25.online/) can store data on blockchain and retrieve it back. Our API endpoint and administrative management layer are operational and live. We have own server cluster with high bandwidth – competitive SLA is guaranteed.

### Here we go!

The technical innovations and digital freedom related features, combined in **Ethelia**, make it the next generation storage platform – open, transparent and secure 😇

<div class="video-wrapper">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/gRFSxqZitEI" title="Hey, let's store the data?! Sure! On Ethereum 😎" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

---

## Founder

Hi, I'm [Konstantin](https://narkhov.pro) 👋

My general interests are in real time programming: multi-threaded systems and low level development – like drivers and real time operating systems components. I'm working on critical software verification, test automation and modeling.

On other hand, I'm Raku and Ethereum enthusiast with a long Perl background. Not a crypto holder, but a blockchain developer and researcher. Over 7 years on this way with experience in decentralized projects, DeFi applications and Ethereum related features like smart contracts, nodes and network setup, deployment and audit.

I'm maintaining and contributing a dozen of [Raku modules](https://raku.land/zef:knarkhov). Previously I have taken part at Perl, Raku and FOSS related events and conferences.

### R&D

* [Software implementation of special IEC61131-3 data types at monitor library](https://narkhov.pro/download/papers/IEC61131-3-data-types-2018.pdf)
* [Handling exceptions using the monitor librarty](https://narkhov.pro/download/papers/handling-exceptions-2017.pdf)
* [Monitor Library for multi-threaded programs](https://narkhov.pro/download/papers/monitoring-library-2017.pdf)
* [Implementation of controlled execution principle for real time applications](https://narkhov.pro/download/papers/monitoring-library-2015.pdf)
* [Vulnerabilities reduction techniques in special real time software](http://www.swsys.ru/index.php?page=article&id=3221)
* [Source code generator for real-time systems](http://www.swsys.ru/index.php?page=article&id=2605)

### Talks

* FOSDEM2022 – [Decentralized authentication](https://pheix.org/embedded/fosdem22-raku-auth)
* Raku Conference 2021 – [Multi-network Ethereum dApp in Raku](https://pheix.org/embedded/the-raku-conference-2021)
* Perl & Raku German Workshop 2021 – [Pheix β-release](https://narkhov.pro/german-perl-raku-workshop-2021.html)
* FOSDEM2021 - [Programming Digital Audio Server backend with Raku](https://narkhov.pro/programming-digital-audio-server-backend-with-raku.html)
* Perl Conference in a Cloud 2020 – [Creating secure decentralized content management systems on Ethereum blockchain with Raku](https://narkhov.pro/creating-secure-decentralized-content-management-systems-on-ethereum-blockchain-with-raku.html)
* Perl & Raku German Workshop 2020 – [Querying the Ethereum blockchain nodes with Raku](https://narkhov.pro/querying-the-ethereum-blockchain-nodes-with-raku-gpw2020.html)
* Perl & Raku Swiss Workshop 2019 – [Pheix: Perl6-based CMS with data storing on blockchain](https://narkhov.pro/swiss-perl-workshop-2019.html)

---

## References

* [3 minutes LIVE pitch](/pp/pitch/)
* [Presentation in PDF](https://gitlab.com/pheix-io/ethelia/-/raw/main/docs/assets/pdf/ethelia-startup-overview_09_2023_24.pdf)
* [Pitch summary & workspace](https://gitlab.com/pheix-io/ethelia/-/issues/10)
* [Identification of the entrepreneurial Project](https://gitlab.com/pheix-io/ethelia/-/issues/32)

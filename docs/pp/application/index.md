# Application

## Idea

Сonsider a network with functionally different nodes: IoT, programmable logic controllers (PLC), smart home systems, micro-services, standalone multi-threading or mobile applications. Every node runs own duty cycle with state changes and events emission.

In general non-critical state changes or usual events should not be logged, but once the node detects some anomaly or exceptional behavior importance of logging increases exponentially.

Okay! We have logged critical state change or sequence of events into the regular database. Everybody's happy till the database crashed or modified by hacker with unauthorized access. No problem! We have daily backups, let's restore the latest one.

Okay... Latest backup was done at 3:00am and now it's about 7:00pm. Friday. Let's try to reach our system administrator 🤬

Enough!

Just store data on blockchain instead of regular database. Blockchain gives decentralization, security and resistance against data corruption or falsifying. Unfortunately, blockchain node does not manage direct data access — we need high level application or service to do that.

Meet **Ethelia**!

---

## Prototype

<div class="video-wrapper">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/jFJSZfpu0fM" title="Ethelia — MVP on air" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

---

## Personal data leakage auditor

!!! example "Talk proposal"

    L2-solution for trusted personal data logging, management and auditing on Ethereum — [pheix-research/talks#20](https://gitlab.com/pheix-research/talks/-/issues/20)

[Hardware and software system](https://gitlab.com/pheix-io/ethelia/-/issues/25) for logging, monitoring and auditing the personal data collection by a websites. In general the system might be used as personal data leakage auditor.

The software part includes the implementation of an extension for popular web browsers. That extension automatically sends information about the fact of personal data collection to a decentralized ledger. The complex also includes a micro service for ledger exploring with an advanced search feature, indexer and history browser.
Details about the fact of personal data collection are stored on Ethereum blockchain and might be used by regulators or supervisory authorities (for data leak investigation or data unfair use):

* often users do not remember the fact of personal data collection, and also do not know about current leaks and their extent — been having aggregated data collection history in the ledger and understanding the number of registered users on a compromised site, we can assess the prospects for collective or individual lawsuits.
* ledger with personal data collection history makes possible to audit the web sites: especially ones that are not included in the list of personal data operators;
* details from spam emails/calls allow to uniquely identify the web sites from which personal data was stolen or sold. The fact of identifying a compromised web site might be used for the official demand for compensation for losses.

At the same time the ledger, which reflects information about personal data collection, will allow:

* assess the extent of personal data collection;
* identify resources collecting data without regulatory approval;
* identify resources that have leaked personal data;
* rank resources according to the quality of processing personal data - such a rating currently does not exist at all;
* provide information for the end user about the web sites, which were collected his personal data.

---

## Miscellaneous

1. Tarqvara enterprise integration offer: [/rawhide/enterpise/tarqvara](/rawhide/enterpise/tarqvara)
2. Miscellaneous application use cases: [/pp/application/subjects](/pp/application/subjects)

---

## Side effect idea — automation for music production

Music production uses a lot of devices or standalone applications (like audio workstations or plugins) that could be controlled by [MIDI](https://en.wikipedia.org/wiki/MIDI). A MIDI message (i.e. MIDI protocol packet) controls some aspect of the receiving device. It consists of a few bytes and could manage target device or application instance in some different ways: change channel data or mode, apply system common or real-time update or send system exclusive request.

At music studio we have a lot of hardware and software components wired into common MIDI network. During the recording or mixing we might to change huge number of settings on independent devices and this change could be a result of some new sound style or expression.

Ethelia can serve the automation flow for sound engineers or producers — MIDI messages can be filtered, stored or eventually fetched with guarantee and consistency of message order in final MIDI sequence.

Another case of application — use Ethelia as audio track annotation service. Since we can translate any event or notification code to some description, we can annotate audio time line. Considering the case of audio or video editing, we can automate the whole process with annotations. The annotations stored on the blockchain can be used as the basis for a director's or public cut.

Annotations are very important for music industry where copyright related questions are the prior ones. Simple example: the band is recording the track and during one of the record sessions someone from the members proposes new riff or music phrase – we can store this event on blockchain and in future this could be used in any copyright disputes.

# Мотивация

!!! example "Трудности перевода"

    **Motivation** – [document in english](/pp/motivation/en/)

## Инновационность проекта

1. Публичный сервис для сбора и хранения легких данных:
    * данные хранятся децентрализованно в blockchain – подделка, манипуляции или удаление невозможны;
    * доступ на чтение или запись может регулироваться на основании баланса (ERC-20 token) участника – можно ограничивать доступ типу, предоставлять платный доступ или блокировать доступ вовсе;
    * публичный доступ к сервису может быть предоставлен чарез web control panels, 3rd-party block explorers or messengers;
    * точки доступа к сервису работают на аппаратной платформе собственной сборки (сервер), которая оптимизирована для стабильной работы backend части сервиса – в настоящий момент сеть Ethelia насчитывает 3 физических узла (Россия, Германия, Кыргызстан); при запуске проекта в Португалии предполагается развертывание национальной сети с узлами в крупных городах.
2. О данных с каких устройств вообще идет речь:
    * access controls, intelligent sensors, CCTV, guard alarms, digital trackers, medicine or safety devices – actually any smart home/city/factory systems, where critical events, traces, notifications or tags are emitting and have to be logged;
    * ценность такого хранилища в том, что данные не могут быть изменены после записи в blockchain, при этом программное обеспечение Ethereum нижнего уровня позволяет организовать out-of-the-box:
        * верификацию отправителя – все абоненты сети имеют собственные приватные ключи, которыми подписывают отправляемые транзакции (данные) (также для усиления защищенности сети могут быть использованы аппаратные ключи, которые будут генерироваться/выдаваться авторизованными органами или регуляторами);
        * подписку на обновление данных по типу, оправителю или значению;
        * контроль интервалов публикации данных (записи данных);
        * независимость от cloud/service привайдеров и корпоративного регулирования – датацентры или головные офисы поставщиков телекоммуникацимоных услуг зачастую располагаются в сторонних юрисдикциях и доступ к хранилищам может быть заблокирован в связи с особенностими законодательства и надзора в этих юрисдикциях. Изестные примеры: StarLink, MiHome cloud, Hetzner.
3. Области для инновационного применения:
    * any ecology monitoring: в Кыргызстане был запущен аналог iqair – дело в том, что подавляющее большинство метеостанций, которые выполняют мониторинг загрязнения воздуха в Бишкеке, контролируются государством и в ситуации многократного превышения допустимого уровня загрязнения воздуха либо отключались, либо переносились внутрь зданий (где работали очистители) – **reference**. При этом, iqair также не представляет аудит исторических данных (нет никаких средств для хотя бы частичного подтверждения достоверности отправителя), фактически являясь монополистом программно-аппартной платформы. В этой ситуации сервис может легко манипулировать данными и отображать любые подмененные или заказанные извне значения.
    * smart factory/city monitoring: надзор за промышленными предприятиями (в части контроля доступа, вредных выбросов, потребления электроэнергии), а также городами (в части использования солнечных панелей, умных средств регулирования городского трафика, освещения или кондиционирования);
    * any services for public use to be tracked and verified – they should store private data locally and keep it in secrect under the requirements of GDPR, but the fact of storing or fetching might be published as a lightweight reference for audit purposes. Trivial example: post-office handled some shipment on date X and unique itentifier or number was issued – on public blockchain signature of this identifier or num might be stored. That signature could be checked and verified only with issuer private key;
    * anyone who wants trusted logging platform for audit purposes – any weekly, monthly or yearly data integrity checks by independent auditors might be predictably and in general easily passed by referring to the immutable data on blockchain.

## Потенциал к росту

1. Расширение сети: размещение новых узлов под управлением регуляторов (government), компаний (business) и частных лиц (enthusiasts);
2. Поддержка высокпроизводительных blockchain: SUI, EOS, TRON;
3. Double-layer architecture: locally stored sensitive data → private blockchain network with sensitive data chunks (low level pointers) → public blockchain network with signatures taken from private transaction hashes (top level pointers). This improvement requires:
    * at least 2 networks: private and public, actually there could be N private networks and one main public network;
    * middle layer bridge: transparent and flexible cross-chain software layer with option to automatically convert low level pointers to top level pointers — PolkaDot fork or avatar;
4. Custom binary libraries with built-in client for popular smart home protocols (MioT, MQTT), integration to custom-built firmwares;
5. Выпуск публичного ERC-20 токена для Ethereum mainnet для децентрализации экономики проекта:
    * выделение в public blockchain network узлов-оракулов для биллинга в Ethereum mainnet;
    * создание биржи для обмена публичных Ethereum mainnet токенов в access токены public blockchain network;
6. Итеграция со сторонними сервисами: Telegram (admin control panel bot), OpenAI (построение трендов, прогнозирование, интеллектуальная атоматизация на основании данных в private and public blockchain networks, аудит данных — в том числе коллизий и утечек).

### Мнение основателя

Все аспекты, рассмотренные выше, предполагают рост проекта как в технологическом аспекте, так и в плане количества сотрудников, департаментов и, вероятно, офисов (branches). По предварительной оценке запуск половины представленных направлений (R&D) потребует привлечения минимум 5-ти новых разработчиков. В долгосрочной перспективе выход на рынок hardware и сотрудничество с вендорами позволит привлечь новые инвестиции, и, в конечном итоге, выйти на новый уровень окупаемости и совокупной стоимости компании.

## Масштабирование будущего бизнеса на рынке

1. Сервис работает по модели SAAS, которая позволяет быстро масштабироваться при взрывном росте клиентов, однако есть следующие bottle-necks;
    * порог количества транзакций в минуту в Ethereum-like сетях – мы можем столкнуться с проблемой, когда рост пользователей приводит к увеличению среднего времени обработки транзакции;
    * аппаратные ресурсы в private and public blockchain networks – как минимум storage и network traffic, рост количества транзакций ведёт к увеличению размера исторических данных blockchain и загрузки сетевого канал узлов сети (преимущество тут: мы собираем свои сервера и с одной стороны handling the whole process, c другой не зависим от 3rd-parties);
    * балансировка нагрузки на бэкенд – выделение максимально возможного функционала в фронтенд часть (например, использование MetaMask для работы с транзакциями), миграция к асинхронной (неблокирующей) обработке в высоконагруженных подсистемах (например, в модулях отправки транзакций), использование subscriptions и реактивных механизмов доставки сообщений/событий (например, websockets).
2. Flexible аutomation – изначально проект строился на принципах максимальной автоматизации, так как был крайне ограничен в каких-либо ресурсах. Автоматизации процессов разработки и развёртывания повышает резистивность к отказам при взрывном росте, тем не менее методы и подходы к автоматизации могут меняться в зависимости от нагрузки и размера проекта в целом;
3. Decentralization – децентрализованное хранение данных позволяет избежать ряда denial-of-service отказов, характерных для монолитных, централизованных сервисов;
4. Remote distributred team – подход в формирования команды по модели neo-компаний;
5. Replicable/delegation model базируется на внедрения middle layer bridge: идея следующая – поставщики данных работают в собственных private blockchain networks (в мастштабе предприятия, города или региона), все поставщики подключены к middle layer bridge и передают в public blockchain network (собственно сеть проекта) только статистику своих сетей, спецификацию хранимых данных и, возможно, последние snapshots. При необходимости визуализации данных низкого уровня выполняется запрос через middle layer bridge к заданному поставщику услуг;
6. Декомпозиция проекта ⇔ декомпозиция бизнеса: проект ясно разделен на принципиальные подсистемы, соответственно масштабирование проекта в целом сводится к масштабированию этих подсистем. Поддержка подсистем со слабой взаимной интеграцией может быть отдана на аутсорсинг или выведена в отдельный бизнес/компанию в целях эффективного управления и оптимизации издержек;
7. Масштабирование програмной части проекта и разворот в части поставщиков данных и сопутствующих транспортных протоколов на примере бесшовного перехода от IQair к WAQI (AQICN) в середине апреля 2024.

## Перспективы (обоснования в целевой стране) по окончании программы

1. Project evolution perspectives:
    * Close perspective (1 year): single production API endpoint for 100k daily requests; about 200 users on free plan; over 30 users on paid plans; 5+ enterprise clients; self-hosted decentralized test network with at least 3 physical nodes. Annual income is about €80k;
    * Far perspectives (3-5 years): we will run a pool of production API endpoints stress-tested against the 1M+ daily requests; over 100k active users; self-hosted decentralized test network with more than 50 nodes; about a 1k developers in github community; more than 15 employees and about 20 third-party devices with our client software at the firmware level. Annual income is over €500k.
2. First year **negative scenario** – project's annual brutto income €80k, project's netto income €60k (€5k per month), it allows:
    * €1k rent fund: I plan to rent an office at incubator's co-working space;
    * €3k salary fund: I expect to hire 2 employees to work at office for a half-time;
    * €1k monthly deposit: infrastructure, accounting, lawyers.
3. First year **positive scenario**– annual brutto income €150k (project's €80k + €70k investments), netto income €120k (€10k per month), it allows:
    * €1k rent fund;
    * €7k salary fund: I expect to hire 3 employees to work at office for a full-time;
    * €1k infrastructure fund: public blockchain network maintenance and development;
    * €1k monthly deposit: accounting, lawyers, unexpected costs.

Что выглядит неудачно в этом списке – не видно зарплаты founder'а, иными словами, а на что будет жить собственно основатель стартапа? Если есть требование о подтверждении €6k в год на фаундера (€500 в месяц), то возможно нужно включить что-то типа **Founder's interest** в ежемесячную смету.

Также следует указать (отдельным пунктом) о перспективах устройства на работу (в штат) разработчиков по модели vesting-cliff. То есть разработчик работает в течении cliff-срока за возможность получить долю в стартапе. Эта модель привлекательна с точки зрения ежемесячных трат, однако она требует глубокой проработки начального stock'а. Вероятно, для более быстрого старта хорошо бы сделать гибрид из vesting-cliff модели и стандартной модели ежемесячной оплаты труда (хотя я вижу тут потенциальные проблемы в части КПД работников).

## Ссылки

Рабочее пространство: [https://gitlab.com/pheix-io/ethelia/-/issues/32](https://gitlab.com/pheix-io/ethelia/-/issues/32)

# Motivation

!!! example "Crafting and drafting"

    **Мотивация** – [версия документа на русском](/pp/motivation/ru/)

## The innovative nature of the project

**Ethelia** is the public service to collecting and storing the lightweight data. It guarantees data consistency, validity and immutability, because of Ethereum blockchain is used as storage layer. **Ethelia** uses Web3.0 access model, which is based on account balance in very-own ERC20 tokens – access could be granted by data type (different data types maybe associated by different tokens), restricted by balance limits (block on negative balance, read-only access on zero balance, full access on positive balance). Public access to the services is provided via web control panels, 3rd-party block explorers or messengers. **Ethelia**'s endpoints are driven with self-crafted servers, which are optimized hardware platforms to run service back end with ultimate stability and reliability. **Ethelia** test network has 3 physical nodes in Europe/Asia – Russia, Germany and Kyrgyzstan. While setting up the project in Portugal we're going to launch at least 3 nodes more in the biggest cities across the country.

### What is lightweight data and what are the data emitters?

Сonsider a network with functionally different nodes: IoT, programmable logic controllers, smart home systems, micro-services, standalone multi-threading or mobile applications. Every node runs own duty cycle with state changes and events.

In general non-critical state changes or usual events should not be logged, but once the node detects some anomaly or exceptional behavior, importance of logging increases exponentially. Those logs are stored on blockchain by **Ethelia** and the fact of storing guarantees the data integrity and consistency. Basically those *logs* are time line aligned short data chunks – trace spans, run-time messages, events, tags, notifications or whatever else.

When we are speaking about lightweight data sources, we are mostly assuming access controls, intelligent sensors, CCTV, guard alarms, digital trackers, medicine or safety devices – actually any smart home/city/factory systems, where critical data has to be logged.

### Why does the storing on blockchain look innovative?

The storing on blockchain has a «killer» feature: since the moment the data is written – it is immutable. This feature is available out-of-the-box and built-in to Ethereum node client software. The rich set of account management and data trace tools is available in node client as well, the most valuable ones are:

* sender and recipient verification – all actors in Ethereum network have own 256 bits private keys, which are used to sign transactions (and eventually data), hardware private keys could be used as well – so actors with critical mission (authorities, healthcare, etc...) are served with extended security and reliability;
* flexible data and transaction subscriptions by type, sender/recipient or actual values;
* data publication management: the publication interval and data volume are set up by sender/recipient address – it gives the ability to control the node resources, throughput and load balance.

Speaking about the blockchain in general, we have to mention decentralization and independence from cloud providers and corporate regulations – data centers and head quarters are located in different jurisdiction across the world, so the services could be canceled due to the tense geopolitical situation, legal restrictions or sanctions. Well-known examples are: StarLink, MiHome cloud and Hetzner. One other important thing is transparency: blockchain enables all participants to have visibility into the data recorded on the network. In couple with technical ones these freedom-related features are giving the alternative software platform to build next generation storage – open, independent and secure.

### Application area

For now the **ecology monitoring** is the main innovative application area. We are focusing on factory supervision (access control, air pollution profiling, electricity consumption monitoring) and smart city management – use and audit of alternative energy sources, smart systems to balance traffic, street lighting, central heating and air conditioning.

Another application area is data privacy. We offer the storage on blockchain for state services to be tracked and verified – generally authorities should store private data locally in secret under the requirements of GDPR, but the fact of storing or fetching might be published as a lightweight reference for audit purposes on a public blockchain.

> Trivial example: post-office handled some shipment and unique identifier or serial number was issued – signature of this identifier or serial number might be stored on public blockchain. That signature could be checked and verified only with issuer private key.

**Ethelia** might be considered as a trusted platform for audit purposes – any weekly, monthly or yearly data integrity checks by independent auditors might be predictably and in general easily passed by referring to the immutable data on blockchain (lightweight indexes, references or anonymous chunks).

!!! example "From idea to prototype"

    See more application details at: [/pp/application/](/pp/application/)

#### Air pollution monitoring service

We launched the **PM25.online** [service](https://pm25.online) in Kyrgyzstan, focused on air pollution data collecting. For a first glance it looks very similar to [IQair](https://www.iqair.com/kyrgyzstan/bishkek), however it's completely different in details and mainly in functionality under the hood.

In Bishkek city – capital of Kyrgyzstan – the most of the PM2.5 providers for IQair are affiliated with government and authorities. That monopoly leads to data manipulation and smoothing. The latest example from the beginning of March 2024 – during the winter 2023/2024 all government affiliated PM2.5 sensors were offline. Unfortunately in Bishkek the air in winter becomes much more polluted because of smog and authorities hide the data from their sensors: any concerns about air pollution proved by data from private sensors are considering as false and rejecting with «we cannot trust your devices» statement.

On 7th March 2024 state sensors were switched on (about 25 sensors across the Bishkek city at one moment), so average PM2.5 value was decreased by 32.5% in past 24 hours and air pollution score [changed](https://www.airnow.gov/aqi/aqi-basics/) from **Unhealthy** to **Unhealthy for sensitive groups**:

![06/03/2024 and 07/03/2024 sensors comparison](/assets/images/pm25/map-comparison.png)

In table below we collected the evidences related to the issue with the public state-maintained sensors. All data is stored on Ethereum Sepolia test network, we provide the link to each transaction – so you can manually [decode input data](/api/verify/) and validate it by yourself:

<a id='locations_in_bishkek_city_table'></a>

??? "Locations in Bishkek city and related PM2.5 values on 06-07 March 2024, 10:00 PM"

    | Location | PM2.5 value (06/03) | PM2.5 value (07/03) |
    | -------- | ------------------------ | ------------------------ |
    | 139 Toktogul Street | [194](https://sepolia.etherscan.io/tx/0x425582b52fad40eedc5f43798d3e6a1da291368b251fcdce88e90af86a68b409) | [196](https://sepolia.etherscan.io/tx/0xe77798b6a074c88af360cc6c700bcae01e8d87d94b06271e7035f2c2b13114b2) |
    | Adigine street | [191](https://sepolia.etherscan.io/tx/0x4b9583f4cb41fdbd84593d81051d99bc829bb68eb32b5b0e0529d2dbe21cc537) | [157](https://sepolia.etherscan.io/tx/0x4bfbadfd8b5bef1f36831b4d81b2deb229aaf2c1d0ded7fd3628dce6d0a0711a) |
    | B. Beyshenalieva Street | [186](https://sepolia.etherscan.io/tx/0x8ff0d1016423b966eca56b3ab44612f604187e70db482cee84d90b718c631161) | — |
    | Baytik Village<sup>[1](#baytik-village)</sup> | [155](https://sepolia.etherscan.io/tx/0xecebafdb38c48d8afc7767beca6921e6560c049e115013bfbb7de4c25f081ab9) — **08:01 PM** | [155](https://sepolia.etherscan.io/tx/0x28aa80dd9e87a794c434f4e675d06c1b60bc114d679ae1a0604cfb9186f8125a) — **08:07 PM** |
    | Bishkek | [191](https://sepolia.etherscan.io/tx/0x7d432f8a4d573df6b4961da3b6a54621a11e340c76ecd5c4074509ae71ae4e55) | [3](https://sepolia.etherscan.io/tx/0xfa8dfff6814f0f4627ca96420ecf7756a30cd1301bf250663c794a23dc831891) |
    | Bishkek International School | [187](https://sepolia.etherscan.io/tx/0x5b49922f44a290459f53af73e07cdbea689d6db6528cdf54c4353ae1533946d7) | [153](https://sepolia.etherscan.io/tx/0x0ce56df8fb05f2fc2c96d2790faa9c8e43f3b0091c37d44356001ebe307472a7) |
    | Bravo | [197](https://sepolia.etherscan.io/tx/0x7b47ea4ebdd737923075b2aed883ccd05b8c9a3d9ee1a8f4a577e621c2bb29cf) | [164](https://sepolia.etherscan.io/tx/0x7c50193a037202cec941ef51bc78503852af5410d175cbf63d53c4eec98b9759) |
    | Breeeth! IQAir Көк-Жар | [236](https://sepolia.etherscan.io/tx/0xd5eefb5e71e98b2c44f045245386b90a3f03e432746c0fb0454b1c1f3a7ade69) | [177](https://sepolia.etherscan.io/tx/0xdf5265e7450c8888ec1e5cec4de2cd73168f66f2ef08e774a8e7d556739bc5c8) |
    | Exprofil | [198](https://sepolia.etherscan.io/tx/0x7fae24156a29e25ee528a02423ffa70e91dff76ec484193042822108003a7137) | [176](https://sepolia.etherscan.io/tx/0x6f9532415eb0d5c91e413f47aca65bcd843e92a188e64ceadc13e38dd7250172) |
    | Highland | [82](https://sepolia.etherscan.io/tx/0x3efa60294deda845cee2a5fc9af88426372a08b4cdb7dee3d2250f36ea1850d2) | — |
    | Home | [160](https://sepolia.etherscan.io/tx/0x4c113b7e5faf7e1bb491b60862d76f5ecc167f650b15fb182522513fe1fcc474) | [178](https://sepolia.etherscan.io/tx/0x2f41b120118c31d494e4cbbffe5059b25c52b5b4775223900a887a26f368d683) |
    | Madiyeva | [173](https://sepolia.etherscan.io/tx/0xd4e4c3399fb337af4ece8f0daf6da45f5c446c845604fbd6312c59fd2f063e72) | [148](https://sepolia.etherscan.io/tx/0x841d9618141d46b7fb08ec903648861be3b67656995f1a1a686c6a2e304938a3) |
    | Repin Street | [205](https://sepolia.etherscan.io/tx/0x144bf0e4adbe207e4700bbd74a0ed2ae30c2f4cca47f846f16d52545cc007e01) | [171](https://sepolia.etherscan.io/tx/0xa08481578ada6d4b47f015966ddd3265b38ff9f58d4df7121e38933b59dc32e0) |
    | Ryskulov St | [235](https://sepolia.etherscan.io/tx/0xe6ca6712ffd7fda4cb619f2890b4d886ddfc5d5b2e04d7539fbc39cc23a3ebe2) | [206](https://sepolia.etherscan.io/tx/0xad4ccf5aafdd60275055fc6dee570e2496d82bd2ad0793418de14dfef646df61) |
    | Seyil | [152](https://sepolia.etherscan.io/tx/0xc5cf2590fb3b6794ba8748b1e1cdff0233bda23d6d957ae7a7d7e608d26d9a99) | [146](https://sepolia.etherscan.io/tx/0x834251e17900b5bf262d6b9f0f4e5b188ee95232bbaec4d02482a14f572e5604) |
    | Soros Foundation | [177](https://sepolia.etherscan.io/tx/0x821857eb992e9d16c55da5c69982bafc453268c9e30875eb2f7198ed6f057da5) | [166](https://sepolia.etherscan.io/tx/0x8878ec75d670a884a167b4660c5b5340fc2f140215cf53f31755f188090c48d9) |
    | 12-mcr | — | [155](https://sepolia.etherscan.io/tx/0x43c416e315f21b17e167c66dbf3c8891f0ba80a090b7d14733e658fc60a84073) |
    | Arashan | — | [92](https://sepolia.etherscan.io/tx/0x734f4e8478a87aeb3778f79dfe0a80be75e3bae2d456d1bf94d677dff7cfba14) |
    | Bakai-Ata | — | [108](https://sepolia.etherscan.io/tx/0xd4e70a704bf0cb63bddae36a8d942ac67391100354f1a284685f1be237494fc3) |
    | Karboz uulu Keldibeka | — | [98](https://sepolia.etherscan.io/tx/0x6c896b82689a50dd17140f4f5601f3178b6e1cbfb44ba5557c3ee765e422844b) |
    | Kubanych | — | [40](https://sepolia.etherscan.io/tx/0x17164b48f6ddb0d5e617d4b552e341682ef5819b3cdfb2bc1055cfa97a7fbd16) |
    | Kyrgyz State Academy of Physical Culture and Sports | — | [111](https://sepolia.etherscan.io/tx/0x65a23aa8ec283e7c3a5ed1fedfbee8dd3bfd8f387d57fa1caaf2c613a17f483f) |
    | Kyrgyz branch of Home Ecology Center | — | [233](https://sepolia.etherscan.io/tx/0x8cb8529ae40b1798744dfbf08b5388903c18c2f388df03a3b33b3c55f330f53a) |
    | MHS KR st. Toktonalieva | — | [161](https://sepolia.etherscan.io/tx/0xe8c6490fe38ef722af2944af9a13bfa307ac1e1182fbe42c9d3b73eba4689b5a) |
    | PNZ No. 4 street, Zibek-Zolu | — | [49](https://sepolia.etherscan.io/tx/0xd86089f83266aba7dfed935c77c7a84e3b5b1aebffa22df4b3debd23ee40ca29) |
    | PNZ No1 Manas Ave | — | [71](https://sepolia.etherscan.io/tx/0x4f3e520a155c2ccdb184eb40fff19828b6a3ff0ffe1a66a99175c37977575cfa) |
    | Park Seil | — | [79](https://sepolia.etherscan.io/tx/0xe1c427a272fd42f1d2100f21a24bd2bb29aa95e214a1f4b2ecf733dc4ee18a51) |
    | Park im. Kemal Ataturk | — | [108](https://sepolia.etherscan.io/tx/0xdff1b689fb0b275e98a5790169ace672607d7e0e7f165af26d8c99ba76da4aad) |
    | Stone - Moynok | — | [58](https://sepolia.etherscan.io/tx/0x5d46b733dc679c2c7da34972ff90ba7bdbca9d3b71daad51b80a02d2e8f72cc0) |
    | Ulitsa Pskovskaya | — | [177](https://sepolia.etherscan.io/tx/0x38b67f27d84bf0f60e93f2376de8163ae5ff911b1429c098f92794be5990bf85) |
    | Youth quarter 263 (near Pobedy park) | — | [91](https://sepolia.etherscan.io/tx/0x474381f4281308572e6191f7571b8ba7caa4db2d91778ae7b82fffb270912593) |
    | c/m Kok-Jar | — | [118](https://sepolia.etherscan.io/tx/0x002aa7ad45e9f0a948056fc6ebbadd81ad80753551ed2b0c66921239639abe07) |
    | mkr.7 12/1' | — | [96](https://sepolia.etherscan.io/tx/0xcc38b58afd9ef5780ad416a074f9dbc25b99ba2562820817255986f6b8e74896) |
    | s. 186 Orok ul. Semetei | — | [105](https://sepolia.etherscan.io/tx/0x4a42cc9eb1c21bdd0d87a39fd280004f10d7d52f7f333ebfdf2a9b88d303c709) |
    | s. Baitik | — | [112](https://sepolia.etherscan.io/tx/0x68f375f6631d2a9e4798407a478948db96c80a952780a609396a7aec495ab72c) |
    | s. Besh Kungai | — | [25](https://sepolia.etherscan.io/tx/0xb1b9dacc9112cf5d25b4eb3be044fb839538e16e9551eba8a347e1f813840f01) |
    | s. Gorny Veterok | — | [71](https://sepolia.etherscan.io/tx/0xa8611d4b106e0ceae88e9290f66f6f799ce39e02dc1123a2211bb227c1e081de) |
    | s. Manas, ul Matisakova | — | [84](https://sepolia.etherscan.io/tx/0x8b6dc141a7aeb6e14e4f1980339d693c39b47aabad9ded59cc58da84f9c9e9eb) |
    | s. Suimonkula Chokmorova, ul. Chokmorova 14 | — | [160](https://sepolia.etherscan.io/tx/0x1c8d691f177f172218e9572fa411210b25490c7d4ed25da51de4cd1391735e27) |
    | s. Zarechnoe, ul. Ala-Archa per. Anniversary | — | [187](https://sepolia.etherscan.io/tx/0x5f80287bde26b394bc35fd987cc3786e84c8805300481630766fcb077d6fa23d) |
    | y/m Altyn-Ordo | — | [159](https://sepolia.etherscan.io/tx/0x3ca97a14f2f8777b119623f05b0d342338d1d06f94a707541fa664977815ba41) |
    | y/m Archa-Beshik, ul. Baba-Ata | — | [148](https://sepolia.etherscan.io/tx/0xee1b4b27f68cf70e542d925b53b39d898bb07eca9fbe19ee0e055a7910b917f2) |

    1. <a id="baytik-village"></a>PM2.5 values for **Baytik Village** location are shown for 08:00 PM and are outdated against all others (10:00 PM) for 2 hours. Currently the value is [considered](https://gitlab.com/pheix-pool/core-perl6/-/commit/712152819e885e03fd84842226ca1a867b31b93d) as valid for 2 hours and 30 minutes. Сonsequently provider could be actually inactive (no data sent on scheduled sync), but PM2.5 value is still shown as valid (aprx. 30 minutes lag). That is a case for **Baytik Village**: provider was online at 08:00 PM, but went offline before 10:00 PM. It's still shown as active at 10:00 PM with outdated PM2.5 value from 08:00 PM.

---

## The potential for growth

At initial step we are looking for **Ethelia** network expansion: launching new nodes to be managed by private companies and individuals. In general, it will make the project much more decentralized and positively impact overall reliability and fault tolerance.

On next milestone own software suite for node management will be introduced: it will allow the communication between different blockchains on single physical node. The concept is very close to blockchain bridges and swap protocols – running different chains in parallel will provide the flexible approach to improve transaction speed, volume and cost. We are very excited with [SUI](https://docs.sui.io/guides/developer/getting-started/local-network), [TRON](https://github.com/TRON-US/docker-tron-quickstart) and [TON](https://docs.ton.org/participate/run-nodes/local-ton) blockchains.

Moving forward we would like to support *double-layer architecture* out-of-the-box: locally stored sensitive data → sensitive data chunks stored on private network (*base layer* pointers) → signatures taken from private transaction hashes stored on public network (*top layer* pointers).

That improvement requires at least two blockchain networks: private and public (actually there could be `N` private networks and one main public network) and middle layer bridge – transparent cross-chain software with the option to convert pointers from *base layer*  to *top layer* and back. Honestly that middle layer has been already proposed above as an own suite for node management.

Next we're planning the collaborations with [hardware manufacturers](https://aqicn.org/gaia/) to integrate **Ethelia** support to custom-built firmware. Also sources files (and binary libraries for the most popular distributions) with built-in client for smart home protocols (MIoT, MQTT) will be published – those libraries could be used by enthusiasts or small vendors in DIY devices and applications for environment monitoring systems.

During the first year after the launch the official ERC20 token **PM25** will be issued. We expect that as obvious step to funding model distribution and decentralization. Side effects: listing at a few well-known CEXs and **Ethelia** wallet development for our Telegram bot (direct **TON**/**PM25** in-app exchange). As a «plan B» we are considering at least four oracle nodes (both for Ethereum main and test networks) as a billing sub-system in the initial period after launch.

On a background we are constantly working on the most promising integrations with the 3rd-party services: featuring full-functional admin control panel bot in Telegram and OpenAI module for smart trend forecasting, AI-driven blockchain data analysis – particularly to spot data falsification, collisions and leaks.

### Founder's vision

All aspects above imply the growth of the **Ethelia** both in the technological and human resource terms (employees, departments and, probably, branches). According to preliminary estimates, at least 5 new developers should be hired to launch the work in those directions (R&D). In the long perspective, entering the hardware market and collaborating with vendors will attract new investments and eventually reach a new level of profitability and total company value.

---

## The scalability of the business in the market

### Business side

We consider technical decomposition in conjunction with business decomposition: **Ethelia** is explicitly separated to principal subsystems. So, to scale the project in general we have to scale its single subsystems. Since subsystems are loosely coupled – they could be developed in parallel by outsource and eventually incorporated in separate business/company for efficient management and overall cost reduce.

### Technincal side

**Ethelia** uses [SaaS](https://en.wikipedia.org/wiki/Software_as_a_service) model: it allows the flexible scaling on the explosive growth. However, the following bottlenecks have to be sorted out:

   * transaction per minute threshold: obviously the growth will increase the average processing time – huge number of transactions will slow Ethereum virtual machine and overall throughput on blockchain;
   * hardware (resources of authority nodes in private and public blockchain networks) – at least storage capacity and network traffic. Huge number of transactions increases historical blockchain volume exponentially and overloads the network with transaction and eventually consensus related traffic (**Ethelia's** advantages: we do not depend on 3rd-party providers, because we craft own servers, optimize them and finally improve the performance);
   * load balancing on back end software – the possible improvements: move as much functions as possible to front end (use MetaMask as signing engine), migrate to async (non-blocking) processing (at least at `Pheix::Model::Database::Blockchain` [module](https://gitlab.com/pheix-pool/core-perl6/-/blob/develop/lib/Pheix/Model/Database/Blockchain.rakumod)), use subscriptions and reactive transport for message delivery (websockets).

Initially **Ethelia** was extremely limited by the hardware resources, It was launched on the cheapest Virtual Private Server – €3,10 per month. So we had to optimize and automate everything and even a bit more. It turned out to many advantages – the back end has a margin of safety, reliability and performance. Flexible аutomation model gives the options to tweak or adjust the **Ethelia** on overload and denial of service. The basic package for project alignment, scaling and future evolution:

* *decentralization* – decentralized and distributed storage is reliable enough against denial-of-service failures, which are specific for monolith (centralized) services;
* *remote team* – we are following contemporary model and guidelines to build challenger neocompany: we have no a headquarter, our team is distributed around the world, some teammates are anonymous;
* *replicable delegation model* – **Ethelia** has middle layer cross-chain bridge prototype: data providers are operating with own private sidechains networks (factory, city or region scale), those private ones are connected to **Ethelia's** public network via the bridge and pushing own statistics, anonymous data chunks or latest network snapshots. The bridge may work in duplex mode: requests from public networks might be sent to sidechains (e.g. to verify data or extended metadata retrieval). In theory **Ethelia** can serve huge number of sidechains, because the major resource and performance costs are actually on sidechains and **Ethelia** works just as a proxy.

#### Seamless data source migration

To be honest – almost seamless. The story is: as it was mentioned above **Ethelia** uses global PM2.5 data providers alongside private individuals. IQAir is the most popular service. Unfortunately, they do not share PM2.5 data via API on free plan. However, all that data is available on their website at *contributors* section (check it out for [Bishkek](https://www.iqair.com/kyrgyzstan/bishkek#contributors) city). **Ethelia** has built-in IQAir parser to retrieve and track reference values. Those ones are used as a reliable set for private data audit and approval.

*Contributors* section at IQAir website loads after the general content (lazy loading). On 17th April 2024 between 6:00AM and 8:00AM IQAir hid *contributors* section and obviously it affected **Ethelia**. We stuck with the PM2.5 data verification from our individual providers. There is alternative to IQAir – [WAQI](https://waqi.info) (or AQICN), it has a bit less providers in Bishkek, but it allows free API access for registered users, so it's crucial advantage.

Migration from IQAir to WAQI was done in a few hours and at 4:00PM on 17th April 2024 **Ethelia** was fully operational again, but with totally different global PM2.5 reference data provider (GIF map generation [notes](https://gitlab.com/pheix-io/ethelia/-/wikis/Notes)):

<center><a id="migration_from_iqair_to_waqi"></a>![17/04/2024 Migration from IQAir to WAQI](/assets/images/pm25/20240417-animatedmap.gif)</center>

!!! example "Historical data"

    You can fetch the animated map for any date via `/livemap DATE` command at official [Ethelia bot](https://t.me/+0YOeSWFoceAyYjBi) in Telegram.<br>`DATE` value should be in `YYYYMMDD` format, e.g. `20240417`.

---

## The prospects (of the setting up in target country) at the end of the program

In the short term (1 year) it's planned to run single production API endpoint for 100k daily requests, engage 200+ users on free plan, 30+ users on paid plans and 5+ enterprise clients, launch self-hosted decentralized test network with at least 5 physical nodes. We expect annual income is about €80k for this stage.

In the long term (3-5 years) a pool of production API endpoints stress-tested against the 1M+ daily request will be launched. We expect 100k+ active users, up and running self-hosted decentralized test network with 50+ nodes, more than 15 employees and 20+ third-party devices with our client software embedded to the firmware. Annual income is over €500k is expected.

### First year positive scenario

Annual brutto income €150k (project's €80k + €70k investments), netto income €120k (€10k per month). It allows:

* €1k office rent fund;
* €7k salary fund: I expect to hire 3 employees to work at office for a full-time;
* €1k infrastructure fund: public blockchain network maintenance and development;
* €1k monthly deposit: accounting, lawyers, founder's interest, unexpected costs.

### First year negative scenario

Project's annual brutto income €80k, project's netto income €60k (€5k per month). It allows:

* €1k office rent fund: I plan to rent an office at incubator's co-working space;
* €3k salary fund: I expect to hire 2 employees to work at office for a half-time;
* €1k monthly deposit: infrastructure, accounting, lawyers, founder's interest.

### First year failure

Project's annual brutto income €0 – **Ethelia** is funded only by founder's savings for the first year: €5k startup visa deposit and €20k founder's savings: total €25 (€2k per month). We expect:

* €0 office rent fund: remote work only;
* €1k salary fund: I expect to hire 1 employee to work remotely for a half-time;
* €500 monthly deposit: infrastructure, accounting, lawyers, founder's interest.

### Hiring model

Four years **vesting-cliff** model is considered as the attractive one to hire new employees. It will allow to use salary fund any company's goals or purposes for a *cliff period*. The only difference with the classic model is: we will vest not a company stock, but some agreed percentage from an anniversary income.

However, it's still a case for **failure** scenario – how much shall we vest and what funds will be used for vesting? So, the vesting schedule should be a bit more complicated: no issues at **positive** and **negative** scenarios – we have an income and we can vest employees. Indeed, if employees are paid only after first is passed, we have to guarantee enough funds on company's account on vesting date.

On a **failure** scenario no income is expected, so we have to define this scenario as exceptional. In this case the next options could be defined:

* ERC20 tokens are planned to be issued and listed at a few public CEXs – we shall agree on the vesting these tokens in amount equal to **negative** scenario salary fund;
* on independent assessment for a startup after a first year we shall agree on stake for early developers – no income does not mean project closing, so we can move forward to second year and continue the work.

### Founder's interest

Above we defined founder's interest/salary at the *monthly deposit* fund – obviously during the temporary residence founder have to rent an apartment and spent some money on daily needs. For considered **positive** and **negative** scenarios *monthly deposit* fund could be increased for €5k more (startup visa deposit). On **failure** scenario *monthly deposit* and pre-issued **Ethelia's** ERC20 tokens are considered as the primary funding tool.

---

## References

* [Letter of Motivation PDF](https://gitlab.com/pheix-io/ethelia/-/raw/main/docs/assets/pdf/Letter-of-Motivation_ELS.ML.20240508.1.0.1.pdf)
* Workspace: [https://gitlab.com/pheix-io/ethelia/-/issues/32](https://gitlab.com/pheix-io/ethelia/-/issues/32)

# 3 minutes LIVE pitch

Presentation: [https://docs.ethelia.pheix.org/assets/presentation-pitch-deck/index.html](https://docs.ethelia.pheix.org/assets/presentation-pitch-deck/index.html)

## Title

Ethelia is a secure, authoritative and reliable Ethereum blockchain storage provider for various kinds of lightweight data.

## Cloud VS Blockchain

Сonsider a network with functionally different nodes: IoT, programmable logic controllers, smart home systems, micro-services, standalone multi-threading or mobile applications. Every node runs own duty cycle with state changes and events.

In general non-critical state changes or usual events should not be logged, but once the node detects some anomaly or exceptional behavior, importance of logging increases exponentially. Those logs are stored on blockchain by Ethelia and the fact of storing will guarantee the data integrity and consistency.

## Markets

Ethelia is between two global markets: Web3.0 and IoT. The second is bigger one and we are focusing on it.

Our target consumers are owners of access controls, intelligent sensors, CCTV, guard alarms, digital trackers, medicine or safety devices. Actually any smart systems, where critical events, traces, notifications or tags are emitting and have to be logged.

*<span style="background:rgb(255,255,0,.2)">As I said, those logs will be stored on public or private blockchains, so it might be used as a trusted platform for audit purposes – any monthly or yearly data integrity checks by 3rd party auditors might be predictably and in general easily passed by referring to the immutable data on blockchain.</span>*

## Competitors

The most of our competitors have proprietary solutions. None gives high level online access to storage via API or web panels. Also there is no ability to audit their blockchains via 3rd-party block explorers, no tools to verify storage consistency, physical status and overall access reliability. Ethelia has all these features out of the box.

As a public gateway, we offer a wide range of paid plans – you can make payments inside Ethelia ecosystem via our own ERC-20 token. It keeps things simple and gives independence from traditional finance regulators.

## We are crafting own servers

Enterprise clients are the priority ones. We offer full integration solutions: hardware, software, deployment, configuration and maintenance. One of our self crafted servers is on the slide, it was settled in local DC about a month ago.

## MVP and initial prototyping results

Current minimal viable product prototype has everything to store data on blockchain and retrieve it back. Our API endpoint and administrative management layer are operational and live. We have own server cluster with high bandwidth, so we can guarantee competitive SLA.

In the end of August I launched documentation portal. There is quick start guide for early developers, which would like to integrate their products to Ethelia's ecosystem.

## Next year expectations

Next year I expect about 200 users on free plan; over 30 users on paid plans; 5+ enterprise clients. This will give 82k bucks in total for next twelve months.

## Technology risks

The most relevant technology risk for Ethelia — Ethereum price and marketcap volatility. Every market crash leads to negative for whole ecosystem: decentralized applications, protocols and projects. *<span style="background:rgb(255,255,0,.2)">So at the drawdown peaks we expect regression, members leaving and pessimistic hype.</span>*

We consider spotting the vulnerabilities, issues and bugs after the project launch. *<span style="background:rgb(255,255,0,.2)">Those ones are the critical and might cause the loss of funds locked in our public smart contracts. We work with 3rd-party bug bounty hunters and software auditors to prevent that situation.</span>*

Finally we have to keep an eye on regulations and local legal restrictions in countries, where we will do our business — generally it's about the legal use of payments within our ecosystem.

## Investment forecast

Now we are using our own funds. Since the start it's approximately 15k dollars. I'm expecting about 60k income from investors next year. With our 20k more, it will give double yearly revenue, so we can make plans and road map for 2025.

## Team

We are true distributed team. We are working remotely, but not only working — multiplayer gaming is our passion. The core developers are the core gamers as well.

## Epilogue

I’m looking for acceleration or mentorship. I believe to find like-minded folks, earlyvangelists and maybe co-founders. Thanks for your attention and feel free to join our official Telegram group.

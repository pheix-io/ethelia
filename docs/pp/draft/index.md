# Pinky paper draft

> reviewed: 08 May 2024

## Executive Summary

!!! example "Рекомендации по написанию:"

    - [Spark Centre business plan template](https://gitlab.com/pheix-io/ethelia/-/raw/main/docs/assets/pdf/business-plan-sparkcentre_feb2021.pdf)
    - [Writing your Business Plan with MSBDC](https://www.middletownct.gov/DocumentCenter/View/12929/Instructions-for-Business-Plan-Template-PDF)
    - [Businessnewsdaily: business plan template](https://www.businessnewsdaily.com/images/business-plan-template.pdf)
    - [Executive Summary for fake company «La Vida Lola»](https://openstax.org/books/entrepreneurship/pages/11-4-the-business-plan#fs-idm366618080)
    - [Characterization and presentation of the entrepreneurial project](https://gitlab.com/pheix-io/ethelia/-/issues/32#note_1896610642)

### Table of contents

!!! warning "Important"

    At the first step we have to write/summarize the details for **bold items** from TOC.

1. Company Objectives
    * [Innovative nature of the project](#innovative-nature-of-the-project)
    * Opportunity you want to capture
    * What are your business goals in the short term, long term?
    * What are the timelines and revenue targets
2. Business Description
    * What problem are you solving or what solution are you providing for your customers?
    * Do you have a minimum viable product (MVP)?
    * What are the current major initiatives of your business?
    * What is your current status of your business? Have you sold any product?
    * [Justification of the internationalization business potential or potential for the export of goods and services](#justification-of-the-internationalization-business-potential-or-potential-for-the-export-of-goods-and-services)
3. Products And Services
    * What products or services do you plan to sell or are already selling?
    * What portion of the revenue does each product or service represent?
    * [Identification of the technology/knowledge that supports the innovation of the product/service and demonstration of its associated potential](#identification-of-the-technologyknowledge-that-supports-the-innovation-of-the-productservice-and-demonstration-of-its-associated-potential)
4. Intellectual Property
    * Does your business have intellectual property (IP) - trademarks, patents, copyrights?
    * What is the current status of your IP?
    * What is your plan for IP to be part of your Canadian company?    
5. Financial Discussion
    * Describe how you will monetize your product or service? What is your sales model?
    * When do you expect to breakeven?
    * When do you expect to be profitable?
    * [Finacial forecast](#finacial-forecast)
7. Key People
    * Who are the main executives / advisors in your business?
    * What experience do they bring to their role?
    * What are their current responsibilities within the company?
    * Who is moving to Canada? Are any remaining in their home country? What is the timeline?
8. Job creation
    * [Job creation potential](#job-creation-potential)
    * [Justification of job creation potential](#justification-of-job-creation-potential)
9. Risk Assessment And Contingency Plan
    * What are your company’s weaknesses (internal), threats (external) that could be a problem for your business plans?
    * Is there any risk in ability to hire the correct people?
    * Is there any risk if your competitors get to market first? Is there risk of missing the opportunity if your business plans are delayed?
    * Are there any changes to laws or regulations that may affect your business?
    * How will you monitor, measure and respond to these risks
10. Business Overview: provide a concise description of what you do and who you serve. Use this section to highlight your competitive position, include size of market, competitors and market & technological trends;
11. Any relevant documents that [validate or verify](https://plan.io/blog/product-idea-validation/) the context of your idea, e.g. market research, patent confirmation, etc.
12. [Planning of the steps to carry out until the creation of the company during the period of 12 months](#planning-of-the-steps-to-carry-out-until-the-creation-of-the-company-during-the-period-of-12-months)

---

### Company Objectives

#### Innovative nature of the project

Consider a network with functionally different nodes: IoT, programmable logic controllers, smart home systems, micro-services, standalone multi-threading or mobile applications. Every node runs own duty cycle with state changes and events. In general we may skip non-critical state changes or usual events, but once any anomaly or exceptional behavior is detected, importance of logging increases exponentially.

Blockchain gives decentralization, security and resistance against data corruption or falsifying. However, blockchain node software does not manage direct data access via any standard application interfaces — we need separate high level provider to do that.

And here comes **Ethelia** – public service to collect and store the lightweight data. It guarantees data consistency, validity and immutability, because of Ethereum blockchain as storage layer. **Ethelia** uses Web3.0 access model, which is based on account balance in very-own ERC20 tokens: access could be granted by data type (different data types maybe associated by different tokens) or restricted by balance limits (block on negative balance, read-only access on zero balance, full access on positive balance).

**Ethelia**'s endpoints are driven with self-crafted servers, which are optimized hardware platforms to run service back end with ultimate stability and reliability.

Our direct competitors have proprietary solutions, in opposite – **Ethelia** open source and community-driven project. It the only public service, which gives high level online access to blockchain storage via API, web control panels and messengers. The features like integration with 3rd-party block explorers, tools to verify storage consistency, physical status and overall access reliability are in **Ethelia** out of the box.

As a public gateway, **Ethelia** offers a wide range of paid plans – you can make payments inside the ecosystem via very-own ERC20 token. It keeps things simple and gives more freedom to end-users.

**Ecology monitoring** is the main innovative application area. We are focusing on factory supervision (access control, air pollution profiling, electricity consumption monitoring) and smart city management – alternative energy sources auditing, systems to balance the traffic, street lighting, central heating and air conditioning. In the beginning of 2024 we started the research for standalone atmospheric particulate matter smart sensor development with the built-in **Ethelia** support.

### Business Description

#### Justification of the internationalization business potential or potential for the export of goods and services

[PM25.online](https://pm25.online) service is the actual use case of business internationalization. This service is launched in Kyrgyzstan, but it's ready to use anywhere across the world with minor changes and adjustments. Basically, it's built to monitor, track, and audit the local environment, so it's ultimately focused on local citizens, regardless of the location.

Another direction is to develop and expand the areas of application of Ethelia, with soft competition with Spain in mind. Consider agriculture: farms or any agricultural ventures may be equipped with smart sensors and connected to the public blockchain via the Ethelia gateway. Since we can gather competitive data such as air quality index, humidity, or CO<sub>2</sub> volume, the farm could use it as a market advantage in promotion or investment. Concerning competition with Spain, the described case is relevant for border regions. The potential for export: any providers, consumers, or clients could be registered on the Ethelia gateway under the paid plans, making it open for anyone, including companies from Spain or any neighboring country.

An important aspect of internationalization and export is targeting enterprise clients and international organizations such as the UN, OSCE, and others. They are the priority customers. We offer full integration solutions including hardware, software, deployment, configuration, and maintenance. We have our own servers that are targeted and optimized to run Ethelia with ultimate performance. The idea behind cooperation with these clients is to implement a working and effective solution in Portugal and then share it across the international business network.

As previously mentioned, Ethelia is a crucial data logging tool in industries and sectors with a high level of danger and control. Prototypes of critical systems embedded with Ethelia software are expected to be presented to green hydrogen manufacturers and mining companies. The successful implementation of Ethelia and its subsequent economic impact will attract international partners to explore and potentially acquire our technology.

### Products And Services

#### Identification of the technology/knowledge that supports the innovation of the product/service and demonstration of its associated potential

The **LAER** (Linux-Apache-Ethereum-Raku) technology stack plays a major role in driving innovation and potential of a product. **Linux**, as the operating system, provides a stable and secure environment for hosting applications, while **Apache** works as the web server software for serving web content. **Ethereum**, a blockchain technology, is utilized for implementing smart contracts and decentralized applications, enabling secure and transparent transactions. **Raku language**, known for its versatility and expressive syntax, is employed for backend development, ensuring efficient performance and flexibility in coding.

On the frontend, **RIOT.js**, a lightweight and powerful framework, is used for creating dynamic and interactive user interfaces, enhancing the
overall user experience.

**GitLab**, an integrated platform for software development, provides a comprehensive set of tools for version control, issue tracking, CI/CD, and project management, streamlining the development process among team members. **Docker**, a containerization platform, simplifies the backend deployment process by encapsulating the application and its dependencies into containers, promoting consistency and portability across different environments. **Redis**, an in-memory data structure store, is utilized for caching and improving response times, resulting in faster and more efficient data access.

Communication tools like **Telegram** and **Matrix** enable real-time messaging among team members, facilitating seamless workflow and collaboration. **MkDocs**, a static site generator, is used for creating documentation that is easy to maintain and navigate, ensuring that users have access to up-to-date information about the product and its features.

Furthermore, the integration of AI technologies like **OpenAI** and **Midjourney** enhances the product's capabilities in social media marketing, content creation, and customer engagement, enabling personalized interactions with users and generating valuable insights.

In conclusion, the **LAER** technology stack effectively leverages a combination of robust and innovative technologies. By harnessing the potential of these tools, we create a product that are not only technologically advanced but also scalable, user-friendly, and adaptable to changing market demands. The strategic use of these technologies enables **Ethelia** to stay ahead of the competition and deliver compelling solutions that meet the evolving market needs.

### Financial Discussion

#### Finacial forecast

##### Summary estimate for the fifth year

| Description                              | Amount **min**, € | Amount **max**, € |
|------------------------------------------|-------------------|-------------------|
| Estimated turnover in 5th year expressed | 160,908.56        | 301,703.55        |
| Estimated assets in 5th year expressed   | 125,957.88        | ∞                 |

##### Turnover

We forecast two scenarios for the income in the first year: positive and negative. In the positive scenario, we will obtain a gross income of €150,000, while in the negative scenario, we will achieve €80,000. We assume a 15% growth rate for each subsequent year. To estimate the turnover in the 5th year, we will use the compound annual growth rate **CAGR** formula. The formula for *future value* `FV` after a certain number of years with a constant growth rate is:

    FV = PV * (1 + g)^n

> `FV` – the future value.<br>
> `PV` – the present value (initial income).<br>
> `g` – the growth rate.<br>
> `n` – the number of years.<br>

Given:

- `g = 15% = 0.15`
- `n = 5`

Positive Scenario with `PV = €150,000`:

    FV_{positive} = 150,000 * (1 + 0.15)^5

Where:

    (1 + 0.15)^5 = 2.011357

So,

    FV_{positive} = 150,000 * 2.011357 = 301,703.55

Negative Scenario with `PV = €80,000`:

    FV_{negative} = 80,000 * (1 + 0.15)^5

Using the same factor calculated before:

    FV_{negative} = 80,000 * 2.011357 = 160,908.56

Summarizing the results: the total for the positive scenario is **€301,703.55**, and the total for the negative scenario  – **€160,908.56**. While the 15% growth rate assumption provides a clear forecast, it's crucial to remain adaptable and continuously monitor and respond to the dynamic business environment to mitigate market, operational and financial risks.

##### Assets

The venture has the following assets at the beginning: founder's capital of €15k, equipment consisting of 4 servers valued at €1k and 2 workstations valued at €1k.

We expect to hire 3 new employees every year. In relation to this growth – each new employee will require 1 workstation. Additionally, we will apply an amortization rate of 20% per year for all equipment. Our expansion plans include colocating a total of 12 servers. Furthermore, the founder's capital is projected to increase by 50% annually.

The year-by-year breakdown is demonstrated by the first year calculation:

1. Founder's capital: `€15,000 * 1.5 = €22,500`
2. Equipment:
    * Equipment value at start of year: **€6,000**
    * Annual amortization (20% of €6,000): **€1,200**
    * Book value of equipment at end of year: `€6,000 - €1,200 = €4,800`
    * New employees: **3**
    * New workstations: `3 * €1,000 = €3,000`
    * Total equipment value: `€4,800 + €3,000 = €7,800`
3. **Total Assets at end of the first year:** `€22,500 + €7,800 = €30,300`

While applying this calculating approach to the subsequent four years, we will finally forecast the asset's cost at the end of the fifth year, which is estimated to be **€125,957.88**.

### Job creation

#### Job creation potential

On positive venture evolution scenario we are planning to hire for the positions listed below. In total during the 2025/2025 we will engage 6 employees (entrepeneurs/founders are not included):

| Qualification degree                      | 2025       | 2026       | Position                          |
|-------------------------------------------|------------|------------|-----------------------------------|
| [ISCED 6](https://uis.unesco.org/en/glossary-term/isced-6-bachelors-or-equivalent-level) - Bachelor’s or equivalent level  | 1          | 1          | System operator                   |
| [ISCED 7](https://uis.unesco.org/en/glossary-term/isced-7-masters-or-equivalent-level) - Master’s or equivalent level    | 2          | 2          | Backend & blockchain developers |

#### Justification of job creation potential

Job creation is a vital aspect of any venture, particularly for startups that aim to drive economic growth and create opportunities for skilled individuals. In the case of our venture, we anticipate three scenarios for job creation in the first year: positive, negative, and failure.

In the positive scenario, we plan to rent an office and allocate a salary fund of €7k to hire 3 employees to work full-time. These employees will include 2 developers - a middle backend developer and a middle blockchain developer, as well as a SysOp. By hiring these employees, we not only create job opportunities but also bring in specialized skills that are crucial for the development of our startup. Additionally, the decision to invest in office space indicates a commitment to creating a conducive work environment that fosters collaboration and team building.

In the negative scenario, we still plan to rent an office but allocate a smaller salary fund of €3k to hire 2 employees (2 developers: middle backend developer and middle blockchain developer) to work at the office for a half-time. By hiring part-time employees, the venture can benefit from the flexibility and cost-effectiveness of this arrangement, while still tapping into the skills and knowledge of qualified professionals. While this scenario may not result in as many job opportunities as the positive scenario, it still contributes to job creation and provides individuals with the opportunity to work for a startup and gain valuable experience in the field.

In the case of failure, where the venture is unable to rent an office, we plan to allocate a salary fund of €1k to hire 1 employee (1 developer: middle backend developer) to work remotely for a half-time. Despite the challenges faced in this scenario, job creation is still a priority, and by hiring at least one employee, we aim to provide opportunities for skilled individuals even in the face of setbacks.

Overall, job creation is a key component of our venture's growth strategy, and we are committed to creating opportunities for talented individuals in the tech industry. Whether in positive, negative, or failure scenarios, our goal remains the same - to drive economic growth, foster creativity, and provide meaningful employment opportunities for skilled professionals.

We will interpolate the scenarios of the first year to subsequent ones via straight-forward approach: increase of salary fund and office rent fund if needed.

### Planning of the steps to carry out until the creation of the company during the period of 12 months

Here is a possible plan for the next 12 months to take the venture from inception to company incorporation:

1. **Month 1-2**: Legal and Financial Preparation
    - Research and select a business name
    - Choose a legal structure for the company (e.g. EIRL, LDA)
    - Set up a business banking account
    - Draft a founders' agreement outlining ownership and decision-making rights
    - Define roles and responsibilities for the founding team
    - Create a budget and financial projections for the next 12 months

2. **Month 3-4**: Product Development and Testing
    - Refine the Ethereum blockchain storage provider prototype (MVP), prepare public beta version
    - Conduct user testing and gather feedback
    - Make any necessary improvements based on feedback
    - Set up a system for tracking and prioritizing feature requests

3. **Month 5-6**: Marketing and Branding
    - Develop a brand identity, including a logo and website
    - Create a marketing strategy to generate buzz around the launch of the company
    - Begin building an online presence through social media and other digital channels
    - Reach out to potential partners and influencers in the blockchain space

4. **Month 7-8**: Legal Compliance and Intellectual Property Protection
    - Register for any necessary licenses or permits to operate the business
    - Ensure compliance with data security and privacy regulations
    - File for any patents or trademarks related to your technology
    - Draft and finalize all legal agreements, including customer contracts and terms of service

5. **Month 9-10**: Funding and Investor Relations
    - Create a pitch deck and financial model for potential investors
    - Research and reach out to potential investors, including angel investors and venture capital firms
    - Attend networking events and pitch competitions to raise awareness of your company
    - Secure funding to support the growth of the company and the development of the product

6. **Month 11-12**: Company Incorporation
    - Work with a lawyer to file the necessary paperwork to incorporate the company
    - Set up payroll and employee benefits, if applicable
    - Define company policies and procedures for governance and compliance
    - Celebrate the official launch of the company and begin full-scale operations

By following this plan, we should be able to successfully take **Ethelia** from inception to company incorporation within 12 months. Here we go!

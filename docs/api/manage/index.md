# Management

> reviewed: 29 March 2024

## Nota bene

All requests should be sent via `POST` method to `https://pheix.org/api` endpoint.

---

## Store data

### Request

```javascript
{
  "credentials": {
    "token": "0x95c27722267bc8f668a84688374dc8feb53af09e564b43598df3390881c89f3d"
  },
  "method": "GET",
  "route": "/api/ethelia/event/store",
  "payload": {
    "code": "20240329",
    "topic": "sample-data",
    "payload": {
      "value": 15,
      "title": "Sample data title"
    }
  }
}
```

**Arguments:**

1. `token` — current access token;
2. `method` — API transport method/direction, only `GET` value is supported;
3. `route` — API data storing route: `/api/ethelia/event/store`;
4. `payload` — lightweight data structure, free to use any key-value pairs here.

### Response

```bash
TOKEN="0x95c27722267bc8f668a84688374dc8feb53af09e564b43598df3390881c89f3d" && \
VALUE=`date +%s` && \
curl -X POST https://pheix.org/api \
     -H 'Content-Type: application/json' \
     -d '{"credentials":{"token":"'$TOKEN'"},"method":"GET","route":"/api/ethelia/event/store","payload":{"code":"20240329","topic":"sample-data","payload":{"value":'$VALUE',"title":"Sample data title"}}}'
```

```javascript
{
  "content": {
    "chainid": 62,
    "event": "sample-data",
    "notification": false,
    "result": "success"
  },
  "msg": "/api/ethelia/event/store fetch is successful",
  "render": "0.403889579",
  "status": 1
}
```

!!! warning "Important"

    Record identifier in database on blockchain is shown as `content.chainid` value.

---

## Search data

### Request

```javascript
{
  "credentials": {
    "token": "0x95c27722267bc8f668a84688374dc8feb53af09e564b43598df3390881c89f3d"
  },
  "method": "GET",
  "route": "/api/ethelia/event/search",
  "payload": {
    "search": {
      "target": "1",
      "value": "*"
    }
  }
}
```

1. `token` — current access token;
2. `method` — API transport method/direction, only `GET` value is supported;
3. `route` — API data search route: `/api/ethelia/event/search`;
4. `payload.search.target` — search target specifier:
    * `1` — search by `payload.code`;
    * `2` — search by `payload.topic`.
4. `payload.search.value` — search key phrase, meta character `*` might be used to search for any value.

### Response

```bash
TOKEN="0x95c27722267bc8f668a84688374dc8feb53af09e564b43598df3390881c89f3d" && \
SEARCHTARGET="1" && \
SEARCHKEYPHRASE="20240329" && \
curl -X POST https://pheix.org/api \
     -H 'Content-Type: application/json' \
     -d '{"credentials":{"token":"'$TOKEN'"},"method":"GET","route":"/api/ethelia/event/search","payload":{"search":{"target":"'$SEARCHTARGET'","value":"'$SEARCHKEYPHRASE'"}}}'
```

```javascript
{
  "content": {
    "component_render": "0.347872805",
    "tparams": {
      "events": [
        [
          "0xe1b4cb4187233b153c559e86ea9661ce7af11a9a",
          "20240329",
          "Fri, 29 Mar 2024 06:36:06 GMT",
          "eyJ2YWx1ZSI6MTcxMTY5NDE2NiwidGl0bGUiOiJTYW1wbGUgZGF0YSB0aXRsZSJ9",
          "sample-data"
        ]
      ]
    }
  },
  "msg": "/api/ethelia/event/search fetch is successful",
  "render": "0.348812532",
  "status": 1
}
```

**Explanation of `content.tparams.events` values:**

Lightweight data (events), found by the search request, is available as array at `content.tparams.events`. Each element is represented by array as well:

1. Element at `0` — sender address on Ethereum;
2. Element at `1` — event code;
3. Element at `2` — event creation date;
4. Element at `3` — event payload in base64;
5. Element at `4` — event topic.

Event payload element at index `3` could be decoded in command line:

```bash
PAYLOAD="eyJ2YWx1ZSI6MTcxMTY5NDE2NiwidGl0bGUiOiJTYW1wbGUgZGF0YSB0aXRsZSJ9" && \
echo $PAYLOAD | base64 --decode | jq .
```
```javascript
{
  "value": 1711694166,
  "title": "Sample data title"
}
```

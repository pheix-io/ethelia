# Authentication

> reviewed: 27 March 2024

## Nota bene

All requests should be sent via `POST` method to `https://pheix.org/api` endpoint.

---

## Get access token

### Request

```javascript
{
  "credentials": {
    "login": "0x8485c5cb3426ec52792ca112c17457182dab52ec",
    "password": "passwd"
  },
  "method": "GET",
  "route": "/api/admin/auth"
}
```

**Arguments:**

1. `login` — address on Ethereum;
2. `password` — secure password;
3. `method` — API transport method/direction, only `GET` value is supported;
4. `route` — API authentication route: `/api/admin/auth`.

### Response

```bash
ADDR="0x8485c5cb3426ec52792ca112c17457182dab52ec" && \
PASS="passwd" && \
curl -X POST https://pheix.org/api \
     -H 'Content-Type: application/json' \
     -d '{"credentials":{"login":"'$ADDR'","password":"'$PASS'"},"method":"GET","route":"/api/admin/auth"}'
```

```javascript
{
  "content": {
    "component": "PHBoZWl4L...50ZW50Pgo=",
    "component_render": "0.329945149",
    "tparams": {
      "addr": "0x8485c5cb3426ec52792ca112c17457182dab52ec",
      "extensions": [...],
      "pkey": "0x622e43b7a774f9821218d91db52f062a749b6f0763e99f2cf4f37e84534f4b6f",
      "pheixauth": "",
      "pheixlogs": [...],
      "scope": "full",
      "session": 60,
      "stats": {...},
      "status": true,
      "table": "embeddedadmin/area",
      "tmpl_sesstoken": "0x2ed43647c41ffae65413e187a4d8a47df74712d9caa86af615b1464eb239587f1757a55b7d2a74f94bc982144de121",
      "tx": "0xa49167f37c044b1aabd98fb7f527d3a636f7e7da7ea483e936d42bc9d18d80c2"
    }
  },
  "msg": "/api/admin/auth fetch is successful",
  "render": "12.676372468",
  "status": 1
}
```

Access token is available at `content.tparams.tx`

---

## Validate access token

### Request

```javascript
{
  "credentials": {
    "token": "0x95c27722267bc8f668a84688374dc8feb53af09e564b43598df3390881c89f3d"
  },
  "method": "GET",
  "route": "/api/admin/session/validate"
}
```

**Arguments:**

1. `token` — current access token;
2. `method` — API transport method/direction, only `GET` value is supported;
3. `route` — API token validation route: `/api/admin/session/validate`.

### Response

```bash
TOKEN="0x95c27722267bc8f668a84688374dc8feb53af09e564b43598df3390881c89f3d" && \
curl -X POST https://pheix.org/api \
     -H 'Content-Type: application/json' \
     -d '{"credentials":{"token":"'$TOKEN'"},"method":"GET","route":"/api/admin/session/validate"}' \
     --cookie "pheixauth=$TOKEN"
```

```javascript
{
  "content": {
    "sesstatus": true,
    "tparams": {
      "addr": "0x8485c5cb3426ec52792ca112c17457182dab52ec",
      "pheixauth": "0xe64b88c94ccbdf013acb7e93f6beeb007399edf5709519bc5ff49a8ce326f64c",
      "pkey": "0xbbf1aa6dcb15ea22cf85f04509dfffc2cde38881f94a25172391281c7a104927",
      "scope": "full",
      "session": 27,
      "tx": "0xe64b88c94ccbdf013acb7e93f6beeb007399edf5709519bc5ff49a8ce326f64c"
    },
    "tryextend": false
  },
  "msg": "/api/admin/session/validate fetch is successful",
  "render": "4.570447564",
  "status": 1
}
```

!!! warning "Important"

    On token validation current access token is forced to expire — new access token is available at `content.tparams.tx`, estimated token lifetime in seconds is shown at `content.tparams.session`

---

## Extend access token

### Request

```javascript
{
  "credentials": {
    "token": "0x20c71f62e6766b9cf698aa8dda19d99d4dfb274d258302fd5a85b9e75c842a30"
  },
  "method": "GET",
  "route": "/api/admin/session/extend"
}
```

**Arguments:**

1. `token` — current access token;
2. `method` — API transport method/direction, only `GET` value is supported;
3. `route` — API token extension route: `/api/admin/session/extend`.

### Response

```bash
TOKEN="0x20c71f62e6766b9cf698aa8dda19d99d4dfb274d258302fd5a85b9e75c842a30" && \
curl -X POST https://pheix.org/api \
     -H 'Content-Type: application/json' \
     -d '{"credentials":{"token":"'$TOKEN'"},"method":"GET","route":"/api/admin/session/extend"}' \
     --cookie "pheixauth=$TOKEN"
```

```javascript
{
  "content": {
    "sesstatus": true,
    "tparams": {
      "addr": "0x8485c5cb3426ec52792ca112c17457182dab52ec",
      "pheixauth": "0x20c71f62e6766b9cf698aa8dda19d99d4dfb274d258302fd5a85b9e75c842a30",
      "pkey": "0xe13b1ff5f1446e6452fcc2d743e779f64b880ad3de1130b8ff5f691332762ddf",
      "scope": "full",
      "session": 60,
      "tx": "0x20c71f62e6766b9cf698aa8dda19d99d4dfb274d258302fd5a85b9e75c842a30"
    },
    "tryextend": false
  },
  "msg": "/api/admin/session/extend fetch is successful",
  "render": "10.896421884",
  "status": 1
}
```

!!! warning "Important"

    Extend only validated access tokens with estimated lifetime more than 15 seconds. On token extension current access token is forced to expire — new access token is available at `content.tparams.tx`, estimated token lifetime (60 seconds by default) is shown at `content.tparams.session`

---

## Destroy access token

### Request

```javascript
{
  "credentials": {
    "token": "0xdc254f53137c0e774681ab3ea48bfa5aa67e6d8f676ec9a13e5d528581407ff8"
  },
  "method": "GET",
  "route": "/api/admin/session/close"
}
```

**Arguments:**

1. `token` — current access token;
2. `method` — API transport method/direction, only `GET` value is supported;
3. `route` — API token destruction route: `/api/admin/session/close`.

### Response

```bash
TOKEN="0xdc254f53137c0e774681ab3ea48bfa5aa67e6d8f676ec9a13e5d528581407ff8" && \
curl -X POST https://pheix.org/api \
     -H 'Content-Type: application/json' \
     -d '{"credentials":{"token":"'$TOKEN'"},"method":"GET","route":"/api/admin/session/close"}' \
     --cookie "pheixauth=$TOKEN"
```

```javascript
{
  "content": {
    "component": "PHBoZWl4L...lbnQ+Cg==",
    "component_render": "0.044001805",
    "tparams": {
      "pheixauth": "0xdc254f53137c0e774681ab3ea48bfa5aa67e6d8f676ec9a13e5d528581407ff8",
      "table": "embeddedadmin/login",
      "tmpl_sesstoken": "0x92a218b1924fe2fbcd18ad62e2d698a79a32ceee72b3e27c725546b61778b828fed8acbddc785f4e51a423438f"
    }
  },
  "msg": "/api/admin/session/close fetch is successful",
  "render": "4.251673905",
  "status": 1
}
```

# Store settings on blockchain

> reviewed: 8 April 2023

At initial step make sure that your local Ethereum network is up and running. Also check `PheixDatabase` smart contract to be deployed to local Ethereum network. `PheixDatabase` smart contract address (any actual one: latest deployment or some previous one) should be set up in initial module configuration file.

## Initial module configuration file

```javascript
{
    "module": {
        "configuration": {
            "settings": {
                "storage": {
                    "group": {
                        "ExtensionEthelia": {
                            "type": "1",
                            "path": "conf/system/eth",
                            "strg": "PheixDatabase",
                            "extn": "abi",
                            "prtl": "http://",
                            "host": "127.0.0.1",
                            "port": "8541",
                            "hash": "0x3ad9c91c3984d62768b9f2e18460c0375a0f34e03a800b3d2d336ad49f27999b",
                            "user": "",
                            "pass": "node1",
                            "data": "undefined",
                            "sign": ""
                        }
                    }
                },
                "routing": {
                    "label": "Routing settings",
                    "group": {
                        "routes": {
                            "search": {
                                "label": "Event store",
                                "route": {
                                    "path": "/ethelia/event/store",
                                    "hdlr": {
                                        "/api": "event_store_api"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
```

---

## Deploy configuration file to blockchain

Change directory to your `Pheix` distributive:

```bash
cd $HOME/git/pheix-pool/core-perl6
```

Make sure that `PheixDatabase` smart contract address is setup for `tst_table` storage at `Pheix` configuration file at `conf/config.json`. Run the deployment command:

```bash
PHEIXDEBUGLEVEL=0 PHEIXTESTENGINE=1 raku ./t/06-blockchain-comp.t local deploy $HOME/apache_root/pheix/www/custom-config/storage/ExtensionEthelia/config-decentralized.json Ethelia/ false true 97000
```

**NOTE**: table on blockchain with configuration JSON will has `Ethelia/config-decentralized` name: it's compiled from target base name w/o extension `config-decentralized` and `Ethelia/` prefix from command line.

Successful deployment:

```bash
# Subtest: Tables with content datasets test on local PoA net, tab: tst_table
    1..4
    ok 1 - s3: account unlock (1.901 sec)
    ok 2 - s3: drop 1 tabs (5.904 sec)
    # *** DPL: 0x643e197d06e33d84a33fdb3bf11d272a7a453c5a5d2367e208d3fb205bb056a9
    ok 3 - s3: store datasets (10.257 sec)
    ok 4 - s3: validate datasets (0.934 sec)
```

---

## Create table for events storing

Since we deployed configuration file to blockchain — all `PheixDatabase` tables was dropped. This cause the issue on storing new events via API, so you have to unlock your account first with `personal.unlockAccount()` and create blank table `ExtensionEthelia` via Geth console:

```javascript
storage.newTable.sendTransaction("ExtensionEthelia", "", false, {from: personal.listAccounts[0], gas: 3000000});
```

---

## Basic module configuration file

Alongside the configuration on blackchain we have to create simple configuration file for extension, which will point to blockchain storage.

Extension setup in Pheix `/home/pheix/www/conf/config.json`:

```javascript
{
    "addons": {
        "group": {
            "installed": {
                ...
                "ethelia": {
                    "addon": "Ethelia",
                    "config": "/home/pheix/extensions/ethelia/storage",
                    "extension": 1
                }
            }
        }
    }
}
```

By default `<ModuleName>/config.json` file will be searched in `/home/pheix/extensions/ethelia/storage` directory. For Pheix extensions module name should be hard coded in extension source and available via getter `get_name()`.

In case of `Ethelia` extension [module name](https://gitlab.com/pheix/raku-ethelia/-/blob/main/lib/Ethelia.rakumod#L17) is `ExtensionEthelia`.

Let's put the next content to `/home/pheix/extensions/ethelia/storage/ExtensionEthelia/config.json`:

```javascript
{
    "module": {
        "configuration": {
            "settings": {
                "external-config-storage": {
                    "value": "Ethelia/config-decentralized"
                },
                "storage": {
                    "group": {
                        "Ethelia/config-decentralized": {
                            "type": "1",
                            "path": "conf/system/eth",
                            "strg": "PheixDatabase",
                            "extn": "abi",
                            "prtl": "http://",
                            "host": "127.0.0.1",
                            "port": "8541",
                            "hash": "0x3ad9c91c3984d62768b9f2e18460c0375a0f34e03a800b3d2d336ad49f27999b",
                            "user": "",
                            "pass": "node1",
                            "data": "undefined"
                        }
                    }
                }
            }
        }
    }
}
```

Implementation details: [https://gitlab.com/pheix-pool/core-perl6/-/issues/170](https://gitlab.com/pheix-pool/core-perl6/-/issues/170)

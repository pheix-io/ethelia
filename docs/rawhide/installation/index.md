# Installation

> reviewed: 29 March 2023

`Ethelia` is a [Raku module](https://docs.raku.org/language/modules), that could be installed by `zef` utility from [zef ecosystem](https://raku.land/zef:tony-o/fez). `Ethelia` has a few dependencies on `Pheix` — so you should install `Pheix` before you will try `Ethelia`:

```bash
mkdir ~/pheix && cd ~/pheix
git clone https://gitlab.com/pheix-pool/core-perl6.git .
zef install .
zef install Ethelia
```

On successful installation you will be able to get `Ethelia` details from local module storage:

```bash
zef info Ethelia
# - Info for: Ethelia
# - Identity: Ethelia:ver<0.0.19>:auth<zef:knarkhov>
# - Recommended By: inst#/opt/rakudo/rakudo-star-2022.12/share/perl6/site
# - Installed: Yes
```

## Integration to Pheix

Extensions are integrated to `Pheix` via main configuration file. `Pheix` [will be split](https://gitlab.com/pheix-pool/core-perl6/-/issues/116) to generic module and deployment stuff in [Release Candidate 2](https://gitlab.com/pheix-pool/core-perl6/-/milestones/4#tab-issues), so now we have to update configuration file `~/pheix/conf/config.json` right in cloned repository.

You have to add `ethelia` section to `module/configuration/settings/addons/group/installed` group:

```javascript
"ethelia": {
    "addon": "Ethelia",
    "config": "/home/me/pheix/conf/addons",
    "extension": 1
}
```

You should provide absolute path to your home dir in `config` section: `/home/me/` instead of `~/`.

## Ethelia configuration file

Name convention for `Pheix` extensions is considered in [Settings on blockchain](/rawhide/settings-on-blockchain/#basic-module-configuration-file) section.

`Ethelia` configuration should be located in `~/pheix/conf/addons/ExtensionEthelia/config.json` file. Sample content is:

```javascript
{
    "module": {
        "configuration": {
            "settings": {
                "storage": {
                    "group": {
                        "ExtensionEthelia": {
                            "type": "0",
                            "path": "conf/extensions",
                            "strg": "",
                            "extn": "tnk",
                            "prtl": "",
                            "host": "",
                            "port": "",
                            "hash": "",
                            "user": "",
                            "pass": "",
                            "data": "undefined"
                        }
                    }
                },
                "routing": {
                    "label": "Routing settings",
                    "group": {
                        "routes": {
                            "store": {
                                "label": "Event store",
                                "route": {
                                    "path": "/ethelia/event/store",
                                    "hdlr": {
                                        "/api": "event_store_api"
                                    }
                                }
                            },
                            "search": {
                                "label": "Event search",
                                "route": {
                                    "path": "/ethelia/event/search",
                                    "hdlr": {
                                        "/api": "event_search_api"
                                    }
                                }
                            },
                            "decode": {
                                "label": "Transaction data decode",
                                "route": {
                                    "path": "/ethelia/transaction/decode",
                                    "hdlr": {
                                        "/api": "event_decode_api"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
```

This configuration file defines the file chain data storage in `~/pheix/conf/extensions/ExtensionEthelia.tnk`. `Ethelia` will crash if this file will be unavailable while you will use `Pheix` administration layer. Configuration file initialization:

```bash
mkdir -p ~/pheix/conf/extensions/ && touch ~/pheix/conf/extensions/ExtensionEthelia.tnk
```

## Smoke Pheix test

```bash
cd ~/pheix/www && raku user.raku --test
```

This command should finish silently — no any warnings or errors.

## Run as a web service

Make sure that you passed this checklist:

1. Create your custom [Rakudo Star bundle](https://gitlab.com/pheix/rakudo-star-linux) distribution;
2. Install your custom Rakudo Star and Pheix dependency modules;
3. Install and configure [Apache](https://httpd.apache.org/download.cgi) with [FastCGI](https://httpd.apache.org/mod_fcgid/) module;
4. Deploy, start and run [Ethereum local network](https://gitlab.com/pheix-research/ethereum-local-network) in Docker [container](https://gitlab.com/pheix-pool/docker-ethereum/container_registry/854193?orderBy=NAME&sort=asc&search%5B%5D=stable).

Then just make a symbolic link to `~/pheix/www/` to your [virtual host](https://httpd.apache.org/docs/2.4/vhosts/examples.html) `DocumentRoot`.

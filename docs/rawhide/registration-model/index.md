# Ethelia registration model

> reviewed: 28 May 2023

I like sign up/in idea from [Midjourney](https://midjourney.com/home/?callbackUrl=%2Fapp%2F) — do it via Discord. For Ethelia it's fine to use Telegram for registration and authentication (optional).

Basic idea is based from recent [TG functionality upgrade](https://twitter.com/telegram/status/1458434269613862913). We use Ethelia official Telegram channel subscribe link to sign up. Since consumer requested channel joining, we have to contact him via bot and collect some additional info/details. Actually via the conversation with bot consumer has to pass the Turing Test 🫠

## Sign up model

1. Consumer request Ethelia official Telegram channel subscribe via a special link;
2. Bot is checking pending subscribe requests and fetches new user to contact;
3. Bot sends `Hello!` message to user and asks to provide more details — the best way is to use [inline keyboards](https://core.telegram.org/bots/2-0-intro) to collect the dossier;
4. When dossier is collected bot asks user to confirm account creation on local auth Ethereum node and returns back Ethereum address and generated password;
5. Bot sends quick guide with account lock/unlock details — on free plans we create auto lock accounts with manual daily unlock (via the bot).

Account on auth Ethereum node should be lock automatically in 5 minutes and user has to unlock it via bot. The steps are:

1. User sends `/unlock 0xf36...66000` request to bot directly;
2. Bot does a sequence of checks:
    * user is subscribed to Ethelia TG channel;
    * user is not blacklisted at Ethelia TG channel (not in `removed users`);
    * address `0xf36...66000` is associated with that user;
3. Bot unlocks account for next 5 minutes and responses `Welcome to Ethelia!` back.

## Dossier

~~On sign up bot collects mandatory user details: full name, email, social network acount.~~

In case of Telegram we actually do not need these details. First argument — dossier collection means that user has bee already joined our Telegram channel, so, since we do not want to spam user directly, we will broadcast our announcements not to user directly, but to all Telegram channel subscribers. Second argument — we know private Telegram chat identifier and user nick name, because user was communicated with our bot. So in edge cases we can reach the user by these details.

The problem of *Second argument* — user might delete the chat with bot and automatically block bot on deletion. In this case, we would not reach the user. The solution — use bot for authentication on Ethelia endpoint. It make the chat with bot active and event if the user deleted the chat, he have to open it again for any activity with Ethelia.

On sign up bot suggests to pass an interview (ChatGPT should be there as well). During this interview we will collect user details:

1. What is the goal of your registration (150 chars at least)? [`skip/text`]
2. What is the project you are working on (pitch/cover essay or just a link)? [`skip/text`]
3. Why and how will you use Ethelia? [`skip/text`]
4. Are you going to use both API and web panel endpoints? [`yes/no`]
5. Are you interested in pricing plans or enterprise integration? [`yes/no`]
6. Can you imagine any partnership or some kind of collaboration with you and Ethelia? [`yes/no`]
7. Would you like to contribute to Ethelia? [`yes/no`]
8. Ethelia is 100% free and open source, would you like to help our project somehow? [`yes/no`]
9. Could you drop a few words for Ethelia team? [`skip/text`]

This list is based mostly on product [payback model](/open-points/).

## Collecting diagram

```mermaid
flowchart TD
A[Goal of registration] --> B[Project pitch]
B --> D[How will you use Ethelia?]
D --> F[API and/or web panel]
F --> G[Pricing plans or enterprise integration]
D -- skip --> G
G --> H[Would you like to contribute]
H --> I[Partnership or some kind of collaboration]
I --> J[Help or donate]
J --> K[Drop a few words for Ethelia team]
I -- no --> K
```

## Bot dossier configuration

```yaml
bot: sample configuration
chat:
  - id: 1
    type: start
    question: Goal of registration
    minchars: 10
    toggles:
      - skip
    next: 2
  - id: 2
    question: Project pitch
    minchars: 10
    toggles:
      - skip
    next: 3
  - id: 3
    question: How will you use Ethelia?
    minchars: 10
    toggles:
      - skip:
          - next: 5
    next: 4
  - id: 4
    question: API and/or web panel
    minchars: 2
    toggles:
      - yes
      - no
    next: 5
  - id: 5
    question: Pricing plans or enterprise integration
    minchars: 2
    toggles:
      - yes
      - no
    next: 6
  - id: 6
    question: Would you like to contribute
    minchars: 2
    toggles:
      - yes
      - no
    next: 7
  - id: 7
    question: Partnership or some kind of collaboration
    minchars: 2
    toggles:
      - yes
      - no:
          - next: 9
    next: 8
  - id: 8
    question: Help or donate
    minchars: 2
    toggles:
      - yes
      - no
    next: 9
  - id: 9
    type: finish
    question: Drop a few words for Ethelia team
    minchars: 10
    toggles:
      - skip
```

## Bots

Command line `curl` helper gists: [https://gist.github.com/pheix/acd8b299e18f4b087faba596bc38864a](https://gist.github.com/pheix/acd8b299e18f4b087faba596bc38864a)

### Trivial Perl bot

Source: [/sources/tg-bot/trivial-bot.pl](/sources/tg-bot/trivial-bot.pl)

### Dossier Raku bot

Source: [https://gitlab.com/pheix-research/raku-dossier-prototype/-/blob/main/bin/trivial-bot.raku](https://gitlab.com/pheix-research/raku-dossier-prototype/-/blob/main/bin/trivial-bot.raku)

Dossier Raku module prototype: [https://gitlab.com/pheix-research/raku-dossier-prototype](https://gitlab.com/pheix-research/raku-dossier-prototype)

## GPT4 wait list request

I'm researching authentication/registration on blockchain via 3rd-parties like Telegram or Discord. On registration step I would like to collect some details (dossier), the answers with no strict options like "yes" or "no" should be routed to GPT — just to make a registration process less official, but the idea behind — to engage user in dialog with AI and get a bit more detailed answers for original questions.

# Feedback

> reviewed: 25 September 2023

## Sui x KuCoin Labs Summer Hackathon 2023

### Credits

1. Workspace: [https://gitlab.com/pheix-io/ethelia/-/issues/27](https://gitlab.com/pheix-io/ethelia/-/issues/27)
2. [Judging criteria, ranking approach and details](https://docs.google.com/spreadsheets/d/1vKlbCwWehh37a9l9eXWbbCUWk8dGmJXe8SukIJB69-8/)

### Batch 3 Scores

| Project Name | Concept / Idea | Product  | Technical Implementation | Creative Use of Sui Move Features | Real World Impact | Overall Impression | Total (Max 60) |
|--------------|----------------|----------|--------------------------|-----------------------------------|--------------------|--------------------|----------------|
Ethelia | 4.14 | 3.57 | 3.57 | 3.29 | 3.71 | 3.29 | **21.57**

### More details

> This project has an interesting concept. However, at first glance, I don’t see how it’s different from [indexers](https://docs.near.org/concepts/advanced/indexers) in general, which are also heavily reliant on events and serve some of the same purposes.
>
> I would try to expand more on how this is different from indexers in the project description and demo.
>
> The other potential issue is that all the copy right now says Ethereum. It would have been better to tailor it to Sui.

## Central Asian Tech forum 2023

На форуме [Central Asian Tech forum 2023](https://kssda.kg/en/awards2023) мне удалось пообщаться c участниками и несколько раз представить свой проект. Позитивные наблюдения: люди, которые далеки от технической и бизнес составляющей проекта воспринимают идею с интересом.

### Критика

Мне удалось пообщаться с основателем [https://haiku.dev/](https://haiku.dev/), который срезал идею проекта на корню в связи с тем, что за два года развития/разработки мне не удалось привлечь ни цента с продаж/инвестиций. Иными словами, продукт никому не нужен — идея проекта не востребована на рынке.

Совет был прост — никакие "развороты" или новые "концепции" проекту не нужны, его следует оставить и не тратить силы.

В процессе беседы к нам присоединился коллега (быть может co-founder), который, выслушав основные тезисы идеи проекта, отметил что:

* сбор данных с датчиков и хранение этих данных где-либо в том или ином виде — не новая идея;
* компания не отдаст данные о состоянии своих критически важных объектов (данные с safety сенсоров) третьей стороне.

### Позитивный опыт

Было три других контакта, при которых я рассказал о своем проекте в техническом и отчасти прикладном ключе:

1. Директору [https://m-vector.com/](https://m-vector.com/) — положительная реакция, было приглашение к дальнейшему обсуждению в кулуарах;
2. На открытом микрофоне был питч по похожей системе сбора и отображения данных с датчиков умного дома, после питча мы обсуждали проект с докладчиком, и сложилось впечатление о возможном сотрудничестве в будущем;
3. Ведущий менеджер [ЕБРР](https://www.ebrd.com/kyrgyz-republic.html) — положительная реакция с предложением выпустить сопутствующий токен;
4. Директору [https://irbis-tech.kg](https://irbis-tech.kg) — аналогично п.1 была положительная реакция, но без перспектив сотрудничества.

Я пообщался с менеджером стартап инкубатора в [IT Part Uzbekistan](https://it-park.uz), договорились связаться в течении нескольких дней — они заинтересованы в стартапах и возможно будет предложение по дальнейшему сотрудничеству.

### Развитие идеи проекта

Собеседники резонируют при упоминании `security`, органичным видится разворот к идеи с акцентом на безопасность хренения и доступа к light weight данным.

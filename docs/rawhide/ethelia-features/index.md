# Ethelia features list

> reviewed: 30 March 2024

## Event management via API

1. ~~store events on blockchain~~;
2. fetch events from blockchain:
    * ~~search by `code` and `topic`~~;
    * ⚠️ search by `created`
        - latest event;
        - events by time range with **fixed limit**.
3. manage `ExtensionEthelia` table:
    * CRUD on event record:
        - `C` — ~~create~~;
        - `R` — ~~read/search~~;
        - `U` — ~~update~~ (we update existed event if it's found by `code` and `token`);
        - `D` — ⚠️ delete/remove operation is not available via both API and admin panel.
    * ⚠️ import data from JSON — consider production and development databases on blockchain: now you can export data from production to JSON via API (search with `*` metachar), but have no any ability to import that JSON to development database.

---

## Pheix administrative layer

Suggestion for MVP — do not take all API features to admin, but keep:

1. searching by ~~code, topic~~ ([raku-ethelia #2](https://gitlab.com/pheix/raku-ethelia/-/issues/2)) and ⚠️ time range (consider **all** and **latest**);
2. ⚠️ visualization of event statistics from `ExtensionEthelia` — depends on code search: we should visualize search results on a **timeline** with [bar chart](https://www.chartjs.org/docs/latest/samples/bar/vertical.html);
3. ⚠️ API statistics (ref: [Alchemy dashboard](https://www.alchemy.com/dashboard)) — looks like `BigBro` engine might be used while accepting API requests.


### «Wanted» features

1. account balance details (actual balance in Ξ) and integration with [MetaMask](https://docs.metamask.io/guide/create-dapp.html#project-setup) to transfer funds to another accounts (useful feature to `root` account at least);
2. store new events.

### «Nice-to-have» features

1. minimalistic transaction explorer;
2. CRUD functionality for `ExtensionEthelia`;
3. *configurable `fixed limit` for search*;
4. event subscription via web sockets with filtering and possible proxy/relay.

---

## Settings

⚠️ **IMPORTANT:** this section requires review/rewrite.

> We have mandatory settings for Ethelia module or associated account, they should be stored in cookies and/or module configuration file. MVP settings list are:
>
> 1. search `fixed limit`;
> 2. event proxy/relay setup: tokens, endpoints, chat ids, etc...;
> 3. filter configuration.
>
> Currently Pheix can:
>
> 1. use local `JSON` config files from [different (non-built-in) paths](https://gitlab.com/pheix-pool/core-perl6/-/commit/39dcef1ce2e326fae929aad17edeeac4369684ee);
> 2. use onchain config files via the `JSON` → blockchain [bridge](https://gitlab.com/pheix-pool/core-perl6/-/issues/170).
>
> With onchain config files we can guarantee safe storing for any setting set (no need for cookies, no restrictions due to built-in paths and any dependencies on global configuration file or `JSON` brige) and only owner account access.
>
> Deployment how-to: [rawhide/settings-on-blockchain](/rawhide/settings-on-blockchain/).

---

## Reference

MVP goal: [https://gitlab.com/pheix-io/ethelia/-/issues/3#note_1087533051](https://gitlab.com/pheix-io/ethelia/-/issues/3#note_1087533051)

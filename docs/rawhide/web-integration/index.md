# Integration to possibly any website

> reviewed: 9 April 2024

We are speaking about administrative panel integration while considering integration to possibly any website. The case: you are using Ethelia via [public API](/api/auth/index.md) to store your lightweight data. Obviously you want to get access to that data via the admin panel at your own website.

Since **Pheix CMS** (the functional core of Ethelia) is headless on one hand and has built-in admin panel implementation on other — *in theory* you can use that panel on your website «**as is**».

How does it possible?

Admin panel has a **back end** — implemented in `Pheix::Addons::Embedded::Admin` [class](https://gitlab.com/pheix-pool/core-perl6/-/blob/develop/lib/Pheix/Addons/Embedded/Admin.rakumod) — and a **front end**: browser side JavaScript [modules](https://gitlab.com/pheix-pool/core-perl6/-/tree/develop/www/resources/skins/akin/js), set of **RIOT.js** components for [login](https://gitlab.com/pheix-pool/core-perl6/-/blob/develop/www/conf/_pages/embeddedadmin/login.txt) and [admin](https://gitlab.com/pheix-pool/core-perl6/-/blob/develop/www/conf/_pages/embeddedadmin/area.txt) pages. Communication between the back end and front end is done via API, so we can assume the back end and front end on separate servers and eventually websites.

It means that all you need to embed Ethelia's admin panel to your website — slightly update HTML code and plug in external JavaScript/CSS.

## Sample integration to `www.pm25.online`

### HTML mods/updates

The first step on admin panel integration — modify your website's generic HTML template (the template of the page or pages set) from where you would like to administrate the lightweight data. To minimize the modifications, we are running all admin panel related stuff in [modal window](https://getbootstrap.com/docs/4.5/components/modal/). So you just have to add the code below to your template:

```html
<div id="adminModal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header header-bg-admin">
        <a href="https://ethelia.pheix.org/" class="ethelia-svg-link" target="_blank">
          <svg style="width:115px; height:40px;">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#logo">
              <symbol id="logo" viewBox="0 0 3954 565">
                <title>Ethelia</title>
                <path fill="#fff" d="M421.84 562.04l0 -149.01 -257.39 0 0 -61.81 251.46 0 0 -149.01 -251.46 0 0 -55.88 257.39 0 0 -149.02 -428.42 0 0 564.73 428.42 0zm454.66 0l0 -415.71 150.71 0 0 -149.02 -473.29 0 0 149.02 151.55 0 0 415.71 171.03 0zm842.44 0l0 -564.73 -171.03 0 0 199.82 -202.35 0 0 -199.82 -171.03 0 0 564.73 171.03 0 0 -215.9 202.35 0 0 215.9 171.03 0zm606.22 0l0 -149.01 -257.39 0 0 -61.81 251.46 0 0 -149.01 -251.46 0 0 -55.88 257.39 0 0 -149.02 -428.42 0 0 564.73 428.42 0zm547.8 0l0 -149.01 -214.21 0 0 -415.72 -171.03 0 0 564.73 385.24 0zm325.97 0l0 -564.73 -171.03 0 0 564.73 171.03 0zm748.46 0l-208.29 -564.73 -215.05 0 -208.28 564.73 193.89 0 20.32 -66.04 203.2 0 20.32 66.04 193.89 0zm-259.93 -215.05l-111.76 0 55.88 -182.04 55.88 182.04z" />
              </symbol>
            </use>
          </svg>
        </a>
        <button type="button" class="close tweak-close-button" data-dismiss="modal" aria-label="Close" onclick="javascript:setTimeout(() => componentUnmount(), 200);">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pheix-space pb-5">
        <div id="pheix-basic-content">
          <div class="pheix-spinner-page" style="margin: 30px 0px; opacity: .4; text-align: center;">
            <i class="fa fa-cog fa-spin fa-2x fa-fw"></i>
            <span class="sr-only">Loading...</span>
          </div>
          <pheix-content></pheix-content>
          <script type="module">
            import {
              loadAPI_v2
            } from './assets/js/pheix/api.js';

            jQuery("#signin").bind("click", function() {
              jQuery('#img-sandwich').removeClass('is-active');
              jQuery('#clicker').trigger('click');
              jQuery('#header-container').removeClass('header-bg');
              jQuery('#header-container').addClass('header-bg-loading');
              jQuery('#adminModal').modal('show');

              loadAPI_v2('page', {
                "token": "0x0"
              }, 'GET', '/api/admin', '200', '', null, false, null);
            });
          </script>
        </div>
        <div id="login-essentials" class="text-center m-4">
          <p>
            <strike>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strike>&nbsp;OR&nbsp; <strike>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strike>
          </p>
          <a title="Use Ethelia telegram bot" class="login-telegram" href="https://t.me/+0YOeSWFoceAyYjBi" target="_blank">
            <i class="fab fa-telegram"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
```

### CSS

The HTML code above uses styles from **Pheix**, **FontAwesome** [v5.10.0](https://cdnjs.com/libraries/font-awesome/5.10.0), **Bootstrap** [v4.5.2](https://getbootstrap.com/docs/4.5/getting-started/introduction/) and **Google Fonts** — you have to import them in your HTML template's `<head></head>`:

```html
<link href="https://pheix.org/resources/skins/akin/css/pheix.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" integrity="sha512-PgQMlq+nqFLV4ylk1gwUOgm6CtIIXkKwaIHp/PAIWHzig/lKZSEGKEysh0TCVbHJXCLN7WetD8TFecIky75ZfQ==" crossorigin="anonymous" referrerpolicy="no-referrer" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet" >
```

!!! warning "Important"

    It's crucial to import exactly the mentioned versions due to the breaking changes in **FontAwesome** and **Bootstrap** releases. Since your site uses built-in admin panel from **Pheix**, you have to use the same versions as **Pheix** does.

### JavaScript

**FontAwesome** and **Bootstrap** also require own Javascript modules to be imported in your HTML template's `<head></head>`:

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/js/all.min.js" integrity="sha512-I2eVORN4ZTPhAiP2pz5d6wQ3ScD/h7ORW4WoCsl3/neI8RSrRnBl2kW3myjTUN+J0Ml4fRT31ZLz08gQb/eo1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
```

Pheix admin panel also requires **RIOT.js** [v7.0.0](https://www.jsdelivr.com/package/npm/riot?version=7.0.0), **js-cookie** [v3.0.5](https://www.jsdelivr.com/package/npm/js-cookie?version=3.0.5) and **Chart.js** [v3.6.2](https://www.jsdelivr.com/package/npm/chart.js?version=3.6.2):

```html
<script src="https://cdn.jsdelivr.net/npm/riot@7/riot+compiler.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.2/chart.min.js" integrity="sha512-tMabqarPtykgDtdtSqCL3uLVM0gS1ZkUAVhRFu1vSEFgvB73niFQWJuvviDyBGBH22Lcau4rHB5p2K2T0Xvr6Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
```

Generic **Pheix** JavaScript modules should be copied and imported from your local file system:

* `api.js`: [https://pheix.org/resources/skins/akin/js/api.js](https://pheix.org/resources/skins/akin/js/api.js)
* `admin/auth.js`: [https://pheix.org/resources/skins/akin/js/admin/auth.js](https://pheix.org/resources/skins/akin/js/admin/auth.js)

You have to manually tweak `endpoint` constant at `api.js` [module](https://gitlab.com/pheix-pool/core-perl6/-/blob/sign-transactions-on-module-level/www/resources/skins/akin/js/api.js?ref_type=heads#L3) — initially it's blank (the same server is used as an API endpoint, so just the absolute path is good enough) and you have to explicitly set it to `https://pheix.org`:

```javascript
import * as PheixAuth from './admin/auth.js';

// const endpoint = '';
const endpoint    = 'https://pheix.org';
window.PheixAPI   = { "isLoading" : false };
window.PheixAuth  = PheixAuth;
```

Consider your `api.js` mod and `admin/auth.js` in `/assets/js/pheix` folder, import code is (see HTML code [above](#html-modsupdates)):

```javascript
import {
  loadAPI_v2
} from '/assets/js/pheix/api.js';
```
### Problem with cookies

When all updates considered above are applied, you will be able to login to admin panel. But you will be unable to manage the authentication session — it's based on access token in cookies and those cookies have `SameSite=Strict` setting enabled. So your session will be successfully opened, but it will be automatically lost due to missed access token (endpoint does not send cookies in responses for cross-domain requests).

For now it's solved by the **whitelist** with the manually approved websites. Also it could be the funding model feature: client pays for **whitelist** membership, if he/she wants the admin panel to be integrated to his/her website. Otherwise panel is available only at [https://pheix.org/admin](https://pheix.org/admin).

### HTML code examples

* [example #1](https://gitlab.com/pheix-io/ethelia/-/blob/main/docs/sources/bishkek-smog/v2/index.html) — **development version** with separate CSS and JS;
* [example #2](https://gitlab.com/pheix-pool/core-perl6/-/blob/sign-transactions-on-module-level/www/conf/_pages/embeddeduser/1696983823.txt) — all-in-one **production version** with embedded CSS and JS.

### Quick demo

![Admin panel integration demo](/assets/images/gif/admin-integration-demo.gif)

## References

* [https://www.pm25.online](https://www.pm25.online)
* Initial idea and details: [#29](https://gitlab.com/pheix-io/ethelia/-/issues/29)
* Updates to `Ethelia` extension: [pheix/raku-ethelia#5](https://gitlab.com/pheix/raku-ethelia/-/issues/5)

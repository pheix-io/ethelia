# Documentation basics

> reviewed: 28 March 2024

⚠️ **THIS IS DEPRECATED CONTENT**

## Concept of this documentation

It's handy to have a guide like **How to read this doc**, where we will cover doc model:

* skeleton with `Pinky paper`, `Rawhide`, `Technical details` (**KO**) and `Open points` sections;
* TOC of each section is presented by pool of subsections;
* Subsection (except ones in `Rawhide`) contains wip/unpublished docs in `Rawhide` section: *идея в том, что если какая-либо subsection имеет wip/unpublished документы, то их следует публиковать в `Rawhide`, исключениями являются собственно subsection из `Rawhide`*;
* Every doc in `Rawhide` should has **reviewed: DD Month YYYY** block;
* All docs in `Rawhide` are mostly based on **issues** of [Ethelia repo](https://gitlab.com/pheix-io/ethelia), so these issues should have specific label (doc name or just **rawhide**) and clear doc evolution structure.

Since this guide is not ready, let's follow the guidelines above to create consistent and clear documentation.

# Event driven database

Add new `bytes32` **data** member to `PheixAccess` event at `PheixDatabase` [smart contract](https://gitlab.com/pheix-research/smart-contracts/-/blob/master/sol/PheixDatabase.sol?ref_type=heads#L6).

The idea is to use that new *event topic* to store data directly on blockchain via built-in Ethereum event mechanism. Consider `insert` or `update`: these functions store data via mappings and other solidity-driven features. The data is actually a `|`-separated string like:

```
0xd67495f282530196a6db3441ce39eaac06faf22f|aqicn-32|Mon, 22 Jul 2024 12:36:12 GMT|ewogICJlbnJpY2htZW50IjogewogICAgInRpdGxlIjogInBhcmsgSW0uIEEuQm90YWxpZXZhIiwKICAgICJwcm9maWxlIjogImh0dHBzOi8vYXFpY24ub3JnL3N0YXRpb24vQDM5ODA3Ny8iLAogICAgInBvc2l0aW9uIjogewogICAgICAibG5nIjogNzQuNTg1NjkyOCwKICAgICAgImxhdCI6IDQyLjkwNjk0NzIKICAgIH0KICB9LAogICJzZW5zb3JkZXZpY2UiOiAiRXRoZWxpYTo6U3RhdGlvblBhcnNlcjo6QVFJQ04iLAogICJzZW5zb3J2YWx1ZSI6ICI0MyIKfQ==|parkImABotalieva
```

That string is stored in mapping with the specific metadata: `id` and `compression` flag, which we can also store in event:

```javascript
event PheixAccess (
    uint8   indexed mcode,
    bytes32 indexed table,
    uint256 indexed rowid,
    bytes32         data,
    uint8           compression,
);
```

## Open points / To be discussed

> How to perform `delete`/`remove` functions on event

Just add indexed `zombie` member of `uint8` type — if it's set to `1`: it means the deleted record and we should not include it in the full table representation.

> How to collect full table from event list

By `table` and `zombie` members — `table` should contain searched table name and `zombie` should not set to `0`.

# Concept

> reviewed: 25 September 2023

⚠️ **THIS IS DEPRECATED CONTENT**

> **Ethelia** is Ethereum blockchain storage provider for different kinds of events. Event — some specific data with `code`, `topic` and `payload`. You just need to be authenticated on endpoint to access a pool of events.
>
> Ethelia uses [Pheix](https://pheix.org) as a generic REST API endpoint software with decentralized authentication and multi-network event accessing agent.
>
> Events could be distinguished by codes. Different events could be accessed on different blockchains, e.g. public or private networks.
>
> Events are stored in **«events-based-database»** — Ethereum built-in logging system. Pheix provides flexible interface to it via `Pheix::Model::Database::Blockchain::Events` and `Pheix::Addons::Embedded::Admin::Events` modules.
>
> Map of all known events is also stored on blockchain in relational database, served by [PheixDatabase](https://gitlab.com/pheix-research/smart-contracts) CRUD smart contract. Simple table with `row_id` column for event code and `data` column for event related meta data is used.

---

## Inspired by

[https://gitlab.com/pheix-pool/core-perl6/-/issues/126](https://gitlab.com/pheix-pool/core-perl6/-/issues/126)

# Deprecated sections

> reviewed: 12 December 2024

A list of deprecated sections:

1. **[Event driven database](/rawhide/deprecated/op/event-driven-database)** — removed from [Open Points](/open-points/) on 12 December 2024;
2. **[Documentation basics](/rawhide/deprecated/rh/doc-basics/)** — removed from [Rawhide](/rawhide/installation/) on 28 March 2024;
3. **[Concept](/rawhide/deprecated/pp/concept/)** — removed from [Pinky Paper](/) on 25 September 2023;

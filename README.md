# Ethelia

[Ethelia](https://ethelia.pheix.org) is Ethereum blockchain storage provider for different kinds of lightweight data.

The idea is to store tamper-proof and sensitive data on blockchain instead of regular database. Blockchain gives decentralization, security and resistance against data corruption or falsifying. Since blockchain node does not manage direct data access — we have to build high level service to do that. This is Ethelia.

Ethelia would be considered as secure, authoritative and reliable service to access and analyze the accumulated data.

## Official documentation

[https://docs.ethelia.pheix.org/](https://docs.ethelia.pheix.org/)

## Pre-launch Telegram channel

![Pre-launch Telegram channel invite](https://gitlab.com/pheix-io/ethelia/-/raw/main/docs/assets/images/prelaunch-telegram-preview.png "Pre-launch Telegram channel invite")

Join via your Telegram application: https://t.me/+0YOeSWFoceAyYjBi

## Getting started

1. Pinky paper
    * [Pitch](docs/index.md)
    * Motivation
        - [en](docs/pp/motivation/en/index.md)
        - [ru](docs/pp/motivation/ru/index.md)
    * [Application](docs/pp/application/index.md)
    * [Draft](docs/pp/draft/index.md)
2. API
    * [Authentication](docs/api/auth/index.md)
    * [Management](docs/api/manage/index.md)
    * [Verification](docs/api/verify/index.md)
3. Rawhide
    * [Installation](docs/rawhide/installation/index.md)
    * [Getting started](docs/rawhide/gettingstarted/index.md)
    * [Registration model](docs/rawhide/registration-model/index.md)
    * [Ethelia features list](docs/rawhide/ethelia-features/index.md)
    * [Settings on blockchain](docs/rawhide/settings-on-blockchain/index.md)
    * [Website integration](docs/rawhide/web-integration/index.md)
    * [Feedback](docs/rawhide/feedback/index.md)
    * [Deprecated](docs/rawhide/deprecated/index.md)
    * Enterprise
        * [Tarqvara](docs/rawhide/enterpise/tarqvara/index.md)
4. [Open points](docs/open-points/index.md)
5. [Repository](https://gitlab.com/groups/ethelia/-/shared?sort=name_asc)

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
